# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def load_fixture(apps, schema_editor):
    from django.core.management import call_command
    call_command('loaddata', 'menuitems_20150909.json')


def empty_migration(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('treenav', '0003_load_menuitems'),
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=empty_migration),
    ]
