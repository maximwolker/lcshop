# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(help_text='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043d\u0430 \u0441\u0430\u0439\u0442\u0435', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slug', models.SlugField(help_text='\u0423\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440', unique=True, max_length=255, verbose_name='slug')),
                ('order', models.IntegerField(verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a', choices=[(0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12), (13, 13), (14, 14), (15, 15), (16, 16), (17, 17), (18, 18), (19, 19), (20, 20), (21, 21), (22, 22), (23, 23), (24, 24), (25, 25), (26, 26), (27, 27), (28, 28), (29, 29), (30, 30), (31, 31), (32, 32), (33, 33), (34, 34), (35, 35), (36, 36), (37, 37), (38, 38), (39, 39), (40, 40), (41, 41), (42, 42), (43, 43), (44, 44), (45, 45), (46, 46), (47, 47), (48, 48), (49, 49), (50, 50)])),
                ('is_enabled', models.BooleanField(default=True, verbose_name='\u0412\u043a\u043b\u044e\u0447\u0435\u043d')),
                ('link', models.CharField(help_text='URL-\u0430\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b', max_length=255, verbose_name='\u041f\u043e\u043b\u043d\u044b\u0439 \u0430\u0434\u0440\u0435\u0441', blank=True)),
                ('object_id', models.PositiveIntegerField(null=True, blank=True)),
                ('href', models.CharField(verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', max_length=255, editable=False)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('content_type', models.ForeignKey(blank=True, to='contenttypes.ContentType', null=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='treenav.MenuItem', help_text='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u0441\u043a\u0438\u0439 \u043f\u0443\u043d\u043a\u0442 \u043c\u0435\u043d\u044e', null=True, verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c')),
            ],
            options={
                'ordering': ('lft', 'tree_id'),
                'verbose_name': '\u043f\u0443\u043d\u043a\u0442 \u043c\u0435\u043d\u044e',
                'verbose_name_plural': '\u043f\u0443\u043d\u043a\u0442\u044b \u043c\u0435\u043d\u044e',
            },
        ),
    ]
