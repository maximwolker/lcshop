# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def load_fixture(apps, schema_editor):
    pass


def empty_migration(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('treenav', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=empty_migration),
    ]
