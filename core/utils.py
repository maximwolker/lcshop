# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import string

from datetime import datetime
from urlparse import urlparse, parse_qs


def get_formatted_dt():
    """
    Получаем текущую дату в текстовом представлении для queryset-фильтров.

    Пример использования:
    def published(self):
        from core.utils import get_formatted_dt
        return self.filter(published_dt__lte=get_formatted_dt(), show=True)
    """
    dt = datetime.now()
    formatted_dt = '{0}-{1}-{2} {3}:{4}:{5}'.format(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second)
    return formatted_dt


def get_youtube_video_id(link):
    """
    Получаем ID ютуб-видео из ссылки
    (http://stackoverflow.com/a/7936523)

    Примеры ссылок:
    - http://youtu.be/SA2iWivDJiE
    - http://www.youtube.com/watch?v=_oPAwA_Udwc&feature=feedu
    - http://www.youtube.com/embed/SA2iWivDJiE
    - http://www.youtube.com/v/SA2iWivDJiE?version=3&amp;hl=en_US
    """
    query = urlparse(link)
    if query.hostname == 'youtu.be':
        return query.path[1:]
    if query.hostname in ('www.youtube.com', 'youtube.com'):
        if query.path == '/watch':
            p = parse_qs(query.query)
            return p['v'][0]
        if query.path[:7] == '/embed/':
            return query.path.split('/')[2]
        if query.path[:3] == '/v/':
            return query.path.split('/')[2]
    # fail?
    return None


def get_youtube_embed_video(link):
    """
    Получаем ссылку для вставки видео на сайт
    """
    video_id = get_youtube_video_id(link)
    return 'https://www.youtube.com/embed/{0}'.format(video_id)


def validate_password(password):
    if len(password) < 6:
        return 'Минимальное количество символов - 6.'

    upper=[]; digits=[]
    for s in password:
        if s in string.ascii_uppercase:
            upper.append(s)
            if len(digits):
                break
        elif s in string.digits:
            digits.append(s)
            if len(upper):
                break
    if not (len(upper) and len(digits)):
        return 'Пароль должен содержать как минимум одну заглавную букву и одну цифру.'

    return 'ok'
