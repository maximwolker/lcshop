# -*- coding: utf-8 -*-
from __future__ import unicode_literals


def core(request):
    compared_products = request.session.get('compared_products', [])

    return {
        'compared_products': compared_products,
    }
