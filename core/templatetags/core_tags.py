# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template


register = template.Library()


@register.filter
def to_str(something):
    """
    Переводим что-либо в строку. Используется:
    - для Decimal, которые должны быть представлены в виде "123.00", а не "123,00"
    - для проверки, лайкал ли человек пост в блоге ({% if post.id|to_str in liked_posts %}), 
    ибо при записи в сессию числа сериализуются в строки
    """
    return str(something)
