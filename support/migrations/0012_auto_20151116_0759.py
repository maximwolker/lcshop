# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0011_faqvideo'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='faqvideo',
            options={'ordering': ['order'], 'verbose_name': '\u0432\u0438\u0434\u0435\u043e', 'verbose_name_plural': 'FAQ: \u0432\u0438\u0434\u0435\u043e-\u0438\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u0438'},
        ),
    ]
