# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0003_auto_20150817_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='supportquestion',
            name='subject',
            field=models.TextField(default='---', max_length=255, verbose_name='\u0422\u0435\u043c\u0430'),
            preserve_default=False,
        ),
    ]
