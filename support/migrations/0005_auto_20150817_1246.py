# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0004_supportquestion_subject'),
    ]

    operations = [
        migrations.AlterField(
            model_name='supportquestion',
            name='subject',
            field=models.CharField(max_length=255, verbose_name='\u0422\u0435\u043c\u0430'),
        ),
    ]
