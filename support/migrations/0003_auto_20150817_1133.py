# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0002_auto_20150817_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faqitem',
            name='at_recommended',
            field=models.BooleanField(default=False, verbose_name='\u0412\u044b\u043d\u0435\u0441\u0442\u0438 \u0432 \u0440\u0430\u0437\u0434\u0435\u043b "\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0443\u0435\u043c\u044b\u0435 \u0442\u0435\u043c\u044b"'),
        ),
        migrations.AlterField(
            model_name='faqsection',
            name='at_header',
            field=models.BooleanField(default=False, help_text='\u041d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435 \u043f\u043e\u043c\u043e\u0449\u0438', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u0448\u0430\u043f\u043a\u0435'),
        ),
    ]
