# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0007_auto_20150817_1318'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='faqsection',
            options={'ordering': ['location', 'order', 'title'], 'verbose_name': '\u0440\u0430\u0437\u0434\u0435\u043b', 'verbose_name_plural': 'FAQ: \u0440\u0430\u0437\u0434\u0435\u043b\u044b'},
        ),
        migrations.RemoveField(
            model_name='faqsection',
            name='at_header',
        ),
        migrations.RemoveField(
            model_name='faqsection',
            name='slug',
        ),
        migrations.AddField(
            model_name='faqsection',
            name='location',
            field=models.CharField(default='bottom', max_length=15, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[('top', '\u0412 \u0432\u0435\u0440\u0445\u043d\u0435\u0439 \u0447\u0430\u0441\u0442\u0438'), ('bottom', '\u0412 \u043d\u0438\u0436\u043d\u0435\u0439 \u0447\u0430\u0441\u0442\u0438')]),
        ),
    ]
