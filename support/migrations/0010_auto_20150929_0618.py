# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0009_load_sections'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='faqitem',
            options={'ordering': ['section', 'order', 'id'], 'verbose_name': '\u0432\u043e\u043f\u0440\u043e\u0441', 'verbose_name_plural': 'FAQ: \u0432\u043e\u043f\u0440\u043e\u0441\u044b \u0438 \u043e\u0442\u0432\u0435\u0442\u044b'},
        ),
        migrations.AlterModelOptions(
            name='faqsection',
            options={'ordering': ['-location', 'order', 'title'], 'verbose_name': '\u0440\u0430\u0437\u0434\u0435\u043b', 'verbose_name_plural': 'FAQ: \u0440\u0430\u0437\u0434\u0435\u043b\u044b'},
        ),
    ]
