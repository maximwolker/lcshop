# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='faqitem',
            name='answer',
            field=models.TextField(verbose_name='\u041e\u0442\u0432\u0435\u0442'),
        ),
    ]
