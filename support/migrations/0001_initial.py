# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FAQItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=255, verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441')),
                ('answer', ckeditor.fields.RichTextField(verbose_name='\u041e\u0442\u0432\u0435\u0442')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('at_recommended', models.BooleanField(default=True, verbose_name='\u0412\u044b\u043d\u0435\u0441\u0442\u0438 \u0432 \u0440\u0430\u0437\u0434\u0435\u043b "\u0420\u0435\u043a\u043e\u043c\u0435\u043d\u0434\u0443\u0435\u043c\u044b\u0435 \u0442\u0435\u043c\u044b"')),
            ],
            options={
                'ordering': ['section', 'order', 'id'],
                'verbose_name': '\u0432\u043e\u043f\u0440\u043e\u0441',
                'verbose_name_plural': 'FAQ: \u0432\u043e\u043f\u0440\u043e\u0441\u044b',
            },
        ),
        migrations.CreateModel(
            name='FAQSection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meta_title', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043b\u0435 "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a"', max_length=255, verbose_name='Meta title (\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_description', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_desc', max_length=255, verbose_name='Meta description (\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_keywords', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_keyw', max_length=255, verbose_name='Meta keywords (\u043a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e)', blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slug', models.SlugField(unique=True, max_length=255, verbose_name='\u0412 url')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('at_header', models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u0448\u0430\u043f\u043a\u0435')),
            ],
            options={
                'ordering': ['order', 'title'],
                'verbose_name': '\u0440\u0430\u0437\u0434\u0435\u043b',
                'verbose_name_plural': 'FAQ: \u0440\u0430\u0437\u0434\u0435\u043b\u044b',
            },
        ),
        migrations.CreateModel(
            name='SupportQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('email', models.EmailField(max_length=255, verbose_name='Email')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('question', models.TextField(verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u0432\u043e\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0432\u043e\u043f\u0440\u043e\u0441\u044b \u0438\u0437 \u0444\u043e\u0440\u043c\u044b',
            },
        ),
        migrations.AddField(
            model_name='faqitem',
            name='section',
            field=models.ForeignKey(verbose_name='\u0420\u0430\u0437\u0434\u0435\u043b', to='support.FAQSection'),
        ),
    ]
