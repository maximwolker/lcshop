# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0006_load_sections'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='faqsection',
            options={'ordering': ['-at_header', 'order', 'title'], 'verbose_name': '\u0440\u0430\u0437\u0434\u0435\u043b', 'verbose_name_plural': 'FAQ: \u0440\u0430\u0437\u0434\u0435\u043b\u044b'},
        ),
    ]
