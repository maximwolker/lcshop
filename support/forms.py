# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from .models import SupportQuestion


class QuestionForm(forms.ModelForm):

    class Meta:
        model = SupportQuestion
        fields = ('profile', 'name', 'email', 'subject', 'question',)
