# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models

# from ckeditor.fields import RichTextField

from core.utils import get_youtube_video_id, get_youtube_embed_video
from site_settings.models import MetatagModel
from lk.models import Profile


class FAQSection(MetatagModel):
    LOCATIONS = (
        ('top', 'В верхней части'),
        ('bottom', 'В нижней части'),
    )
    title = models.CharField('Заголовок', max_length=255)
    location = models.CharField('Расположение', max_length=15, choices=LOCATIONS, default='bottom')

    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField('Показывать на сайте', default=True)

    class Meta:
        ordering = ['-location', 'order', 'title', ]
        verbose_name = 'раздел'
        verbose_name_plural = 'FAQ: разделы'

    def __unicode__(self):
        return self.title

    @property
    def items(self):
        return self.faqitem_set.filter(show=True)


class FAQItem(models.Model):
    section = models.ForeignKey(FAQSection, verbose_name='Раздел')
    question = models.CharField('Вопрос', max_length=255)
    # answer = RichTextField('Ответ')
    answer = models.TextField('Ответ')

    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField('Показывать на сайте', default=True)
    at_recommended = models.BooleanField('Вынести в раздел "Рекомендуемые темы"', default=False)

    class Meta:
        ordering = ['section', 'order', 'id', ]
        verbose_name = 'вопрос'
        verbose_name_plural = 'FAQ: вопросы и ответы'

    def __unicode__(self):
        return self.question

    # def get_absolute_url(self):
    #     return reverse('support:section', kwargs={'slug': self.section.slug})


class FAQVideo(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    link = models.CharField('Ссылка на youtube.com', max_length=255)
    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField('Показывать на сайте', default=True)

    class Meta:
        ordering = ['order', ]
        verbose_name = 'видео'
        verbose_name_plural = 'FAQ: видео-инструкции'

    def __unicode__(self):
        return self.title

    @property
    def has_video(self):
        return get_youtube_video_id(self.link)

    def get_iframe_video_link(self):
        return get_youtube_embed_video(self.link)


class SupportQuestion(models.Model):
    profile = models.ForeignKey(Profile, verbose_name='Пользователь', null=True, blank=True)
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)

    name = models.CharField('Имя', max_length=255)
    email = models.EmailField('Email', max_length=255)
    subject = models.CharField('Тема', max_length=255)
    question = models.TextField('Вопрос')

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'вопрос'
        verbose_name_plural = 'вопросы из формы'

    def __unicode__(self):
        return '{0} ({1})'.format(self.email, self.datetime)
