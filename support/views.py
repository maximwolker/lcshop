# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView, CreateView

from lk.email import send_question_email
from .forms import QuestionForm
from .models import FAQSection, FAQItem, FAQVideo


class SupportView(TemplateView):
    template_name = "support/support.html"

    def get_context_data(self, **kwargs):
        context = {
            'top_sections': FAQSection.objects.filter(show=True, location='top'),
            'bottom_sections': FAQSection.objects.filter(show=True, location='bottom'),
            'recommended_items': FAQItem.objects.filter(show=True, at_recommended=True),
            'videos': FAQVideo.objects.filter(show=True),
        }
        context.update(super(SupportView, self).get_context_data(**kwargs))
        return context


class SupportSearchView(TemplateView):
    template_name = "support/include/search_results.html"

    def get(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseRedirect(reverse('support'))
        return super(SupportSearchView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        q = self.request.GET.get('q')
        context = {
            'items': FAQItem.objects.filter(Q(question__icontains=q) | Q(answer__icontains=q), show=True),
            'video_items': [v for v in FAQVideo.objects.filter(show=True, title__icontains=q) if v.has_video],
        }
        context.update(super(SupportSearchView, self).get_context_data(**kwargs))
        return context


class QuestionFormView(CreateView):
    form_class = QuestionForm

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        super(QuestionFormView, self).form_valid(form)
        send_question_email(self.request, form.instance)
        data = {
            'errors': [],
            'result': 'ok',
        }
        return JsonResponse(data)
