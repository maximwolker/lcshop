# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from site_settings.admin import MetatagModelAdmin
from .models import FAQSection, FAQItem, FAQVideo, SupportQuestion


class FAQItemInline(admin.TabularInline):
    model = FAQItem
    fields = ('question', 'answer', 'order', 'show', 'at_recommended',)
    # extra = 0
    extra = 3


@MetatagModelAdmin
@admin.register(FAQSection)
class FAQSectionAdmin(admin.ModelAdmin):
    list_display = ('title', 'location', 'order', 'show',)
    list_filter = ('location', 'show',)
    list_editable = ('order',)
    fieldsets = (
        (None, {
            'fields': ('title', ('location', 'order',), 'show',)
        }),
    )
    search_fields = ['title', ]
    inlines = [FAQItemInline, ]


@admin.register(FAQItem)
class FAQItemAdmin(admin.ModelAdmin):
    list_display = ('question', 'section', 'order', 'show', 'at_recommended')
    list_filter = ('section', 'show', 'at_recommended',)
    list_editable = ('order',)
    search_fields = ['question', 'answer', ]


@admin.register(FAQVideo)
class FAQVideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'link', 'order', 'show',)
    list_filter = ('show',)
    list_editable = ('order',)


@admin.register(SupportQuestion)
class SupportQuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'name', 'email', 'subject',)
    list_display_links = ('id', 'datetime',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), 'profile',)
        }),
        ('Данные из формы', {
            'fields': ('name', 'email', 'subject', 'question',)
        }),
    )
    search_fields = ['email', 'name', 'subject', 'question', ]
    readonly_fields = ('id', 'datetime', 'profile', 'name', 'email', 'subject', 'question',)
