# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from registration.forms import RegistrationFormUniqueEmail
from registration.users import UserModel, UsernameField

from core.utils import validate_password
from lk.models import Profile, InvoiceRequest, DocumentsRequest

User = UserModel()


class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        if not self._errors:
            email = cleaned_data.get('email')
            password = cleaned_data.get('password')
            try:
                user = Profile.objects.get(email=email)
                if not user.check_password(password):
                    raise forms.ValidationError('Неверное сочетание Email / Пароль.')
                elif user.is_blocked:
                    raise forms.ValidationError('Пользователь с данным email заблокирован.')
            except Profile.DoesNotExist:
                raise forms.ValidationError('Неверное сочетание Email / Пароль.')
        return cleaned_data


class RegistrationForm(RegistrationFormUniqueEmail):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    phone = forms.CharField(required=False)
    password1 = forms.CharField(required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(required=True, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = (UsernameField(), 'first_name', 'last_name', 'phone')

    def save(self, commit=True):
        instance = super(RegistrationForm, self).save(commit=False)
        instance.profile_type = 'human'
        if commit:
            instance.save()
        return instance

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        res = validate_password(password1)
        if res != 'ok':
            raise forms.ValidationError(res)
        return password1


class SellerRegistrationForm(RegistrationFormUniqueEmail):

    class Meta:
        model = User
        fields = (UsernameField(), 'first_name', 'last_name', 'phone', 'postal_code',
                  'company_name', 'phone2', 'address1', 'address2',
                  'city', 'image', 'region', 'country')

    def __init__(self, *args, **kwargs):
        super(SellerRegistrationForm, self).__init__(*args, **kwargs)

        for _, field in self.fields.iteritems():
            if field.label != 'Телефон №2' and field.label != 'Изображение' and field.label != 'Почтовый код' and field.label != 'Адрес №2':
                field.required = True
            field.widget.attrs.update({'class': 'form-control input-lg'})

        self.fields['country'].widget.attrs.update({'class': 'form-control input-lg'})
        self.fields['region'].widget.attrs.update({'class': 'form-control input-lg'})

    def save(self, commit=True):
        instance = super(SellerRegistrationForm, self).save(commit=False)
        instance.profile_type = 'company'
        if commit:
            instance.save()
        return instance

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        res = validate_password(password1)
        if res != 'ok':
            raise forms.ValidationError(res)
        return password1


class HumanUpdateForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ('email', 'first_name', 'last_name', 'phone',)

    def __init__(self, *args, **kwargs):
        super(HumanUpdateForm, self).__init__(*args, **kwargs)

        for _, field in self.fields.iteritems():
            if field.label != 'Телефон':
                field.required = True


class FirstCompanyUpdateForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ('email', 'image', 'first_name', 'last_name', 'phone',
                  'company_name', 'phone2',)

    def __init__(self, *args, **kwargs):
        super(FirstCompanyUpdateForm, self).__init__(*args, **kwargs)

        for _, field in self.fields.iteritems():
            # import ipdb; ipdb.set_trace()
            # if field.label != 'Телефон №2' and not (field.label == 'Изображение' and self.instance.image):
            #    field.required = True
				
            if field.label != 'Телефон №2' and field.label != 'Изображение' and field.label != 'Адрес №2':
                field.required = True


class SecondCompanyUpdateForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ('address1', 'address2', 'city', 'region', 'postal_code', 'country',)

    def __init__(self, *args, **kwargs):
        super(SecondCompanyUpdateForm, self).__init__(*args, **kwargs)

        for _, field in self.fields.iteritems():
            if field.label != 'Телефон №2' and field.label != 'Почтовый код' and field.label != 'Адрес №2':
                field.required = True
            field.widget.attrs.update({'class': 'form-control input-lg'})

        self.fields['country'].widget.attrs.update({'class': 'form-control input-lg'})
        self.fields['region'].widget.attrs.update({'class': 'form-control input-lg'})


class ChangePasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput)
    new_password_repeat = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password(self):
        new_password = self.cleaned_data.get('new_password')
        res = validate_password(new_password)
        if res != 'ok':
            raise forms.ValidationError(res)
        return new_password

    def clean(self):
        cleaned_data = super(ChangePasswordForm, self).clean()

        if not self._errors:
            new_password = cleaned_data.get('new_password')
            new_password_repeat = cleaned_data.get('new_password_repeat')

            if not new_password == new_password_repeat:
                raise forms.ValidationError('Пароли не совпадают')
            else:
                return cleaned_data

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password'])
        if commit:
            self.user.save()
        return self.user


class InvoiceRequestForm(forms.ModelForm):

    class Meta:
        model = InvoiceRequest
        fields = ('company_name', 'phone', 'email', 'address',)


class DocumentsRequestForm(forms.ModelForm):

    class Meta:
        model = DocumentsRequest
        fields = ('company_name', 'phone', 'email', 'address',)
