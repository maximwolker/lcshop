# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from django.views.generic import FormView

from registration.backends.default.views import RegistrationView
from cart.cart import Cart
from .forms import SellerRegistrationForm, LoginForm


class SellerRegistrationView(RegistrationView):
    form_class = SellerRegistrationForm
    template_name = 'registration/registration_seller_form.html'


class AjaxLoginView(FormView):
    form_class = LoginForm

    def form_invalid(self, form):
        errors = []

        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})

        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        user = authenticate(**form.cleaned_data)
        if user:
            login(self.request, user)

        cart = Cart(self.request).cart
        cart.customer = user
        cart.save()
        return JsonResponse({'result': 'ok'})
