# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404, JsonResponse
from django.views.generic import View

from catalog.models import Product


class AddToFavorites(View):

    def post(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(id=(request.POST['product_id']))
            profile = request.user

            if profile.is_anonymous() or not profile.is_active:
                raise Http404
            profile.favorites.add(product)
            return JsonResponse({'result': 'ok'})

        except (ValueError, Product.DoesNotExist) as e:
            return JsonResponse({'result': 'error', 'error': 'Ошибка: такого товара не существует.'})


class RemoveFromFavorites(View):

    def post(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(id=(request.POST['product_id']))
            profile = request.user

            if profile.is_anonymous() or not profile.is_active:
                raise Http404
            profile.favorites.remove(product)
            return JsonResponse({'result': 'ok'})

        except (ValueError, Product.DoesNotExist) as e:
            return JsonResponse({'result': 'error', 'error': 'Ошибка: такого товара не существует.'})


class AddToCompare(View):

    def post(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(id=(request.POST['product_id']))
            category_id = str(product.category_id)
            compared_products = request.session.get('compared_products', {})
            count = sum([len(arr) for arr in compared_products.values()])
            category_arr = compared_products.get(category_id, [])

            if product.id in category_arr:
                return JsonResponse({'result': 'ok', 'count': count})

            elif len(category_arr) < 4:
                category_arr.append(product.id)
                compared_products[category_id] = category_arr
                request.session['compared_products'] = compared_products
                count = sum([len(arr) for arr in compared_products.values()])
                return JsonResponse({'result': 'ok', 'count': count})

            else:
                return JsonResponse({
                    'result': 'error',
                    'error': 'Нельзя добавить больше 3 товаров для сравнения в одной категории.'
                })

        except (ValueError, Product.DoesNotExist) as e:
            return JsonResponse({'result': 'error', 'error': 'Ошибка: такого товара не существует.'})


class RemoveFromCompare(View):

    def post(self, request, *args, **kwargs):
        try:
            product = Product.objects.get(id=(request.POST['product_id']))
            category_id = str(product.category_id)
            compared_products = request.session.get('compared_products', {})
            category_arr = compared_products.get(category_id, [])

            category_arr.pop(category_arr.index(product.id))
            if len(category_arr) == 0:
                del(compared_products[category_id])

            count = sum([len(arr) for arr in compared_products.values()])
            request.session['compared_products'] = compared_products
            return JsonResponse({'result': 'ok', 'count': count})

        except (ValueError, KeyError, Product.DoesNotExist) as e:
            return JsonResponse({'result': 'error', 'error': 'Ошибка'})
