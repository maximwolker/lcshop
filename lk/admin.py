# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin

from .models import Profile, InvoiceRequest, DocumentsRequest, NewsletterSubscription


class ProfileCreationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password_repeat = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Profile
        fields = ('email', 'is_staff', 'is_superuser',)

    def clean_password_repeat(self):
        password = self.cleaned_data.get('password')
        password_repeat = self.cleaned_data.get('password_repeat')

        if password and password_repeat and password != password_repeat:
            raise forms.ValidationError('Пароли не совпадают')

        return password_repeat

    def save(self, commit=True):
        user = super(ProfileCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])

        if commit:
            user.save()

        return user


class ProfileChangeForm(forms.ModelForm):
    new_password = forms.CharField(widget=forms.PasswordInput, required=False)
    new_password_repeat = forms.CharField(widget=forms.PasswordInput, required=False)

    class Meta:
        model = Profile
        fields = '__all__'

    def clean_new_password_repeat(self):
        new_password = self.cleaned_data.get('new_password')
        new_password_repeat = self.cleaned_data.get('new_password_repeat')

        if new_password and new_password_repeat and new_password != new_password_repeat:
            raise forms.ValidationError('Пароли не совпадают')

        return new_password_repeat

    def save(self, commit=True):
        user = super(ProfileChangeForm, self).save(commit=False)

        if self.cleaned_data['new_password']:
            user.set_password(self.cleaned_data['new_password'])
            if commit:
                user.save()

        return user


@admin.register(Profile)
class ProfileAdmin(UserAdmin):
    form = ProfileChangeForm
    add_form = ProfileCreationForm

    list_display = ('id', 'email', 'show_first_name', 'show_last_name', 'show_profile_type', 'show_company_name',)
    list_display_links = ('id', 'email',)
    list_filter = ('profile_type',)

    fieldsets = (
        ('Общие данные', {
            'fields': (('email', 'date_joined',), ('first_name', 'last_name',), 'phone', 'profile_type',)
        }),
        ('Данные продавца', {
            'fields': ('company_name', 'image', 'phone2', 'postal_code', 'country', 'region', 'city', 'address1', 'address2',)
        }),
        ('Управление доступами', {
            # 'fields': ('is_superuser', 'is_staff', 'is_active', 'active_sig', 'is_blocked',)
            'fields': ('is_superuser', 'is_staff', 'is_active', 'is_blocked',)
        }),
        ('Пароль', {
            'classes': ('wide',),
            'fields': ('new_password', 'new_password_repeat')
        }),
        (None, {
            'fields': ('favorites',)
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', ('is_staff', 'is_superuser',), 'password', 'password_repeat')}),
    )
    filter_horizontal = ['favorites', ]
    search_fields = ['email', 'name', ]
    readonly_fields = ('date_joined', 'active_sig',)
    ordering = ('-id',)


@admin.register(InvoiceRequest)
class InvoiceRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'company_name', 'profile',)
    list_display_links = ('id', 'datetime',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), 'profile',)
        }),
        ('Данные из формы', {
            'fields': ('company_name', 'phone', 'email', 'address',)
        }),
    )
    search_fields = ['profile__email', 'profile__company_name', 'profile__first_name', 'profile__last_name', ]
    search_fields += ['company_name', 'phone', 'email', 'address', ]
    readonly_fields = ('id', 'datetime', 'profile', 'company_name', 'phone', 'email', 'address')
    # readonly_fields = ('id', 'datetime', 'profile',)
    # readonly_fields = ('id', 'datetime',)


@admin.register(DocumentsRequest)
class DocumentsRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'company_name', 'profile',)
    list_display_links = ('id', 'datetime',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), 'profile',)
        }),
        ('Данные из формы', {
            'fields': ('company_name', 'phone', 'email', 'address',)
        }),
    )
    search_fields = ['profile__email', 'profile__company_name', 'profile__first_name', 'profile__last_name', ]
    search_fields += ['company_name', 'phone', 'email', 'address', ]
    readonly_fields = ('id', 'datetime', 'profile', 'company_name', 'phone', 'email', 'address')
    # readonly_fields = ('id', 'datetime', 'profile',)
    # readonly_fields = ('id', 'datetime',)


@admin.register(NewsletterSubscription)
class NewsletterSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('email', 'show_profile', 'datetime',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), 'profile', 'email')
        }),
    )
    search_fields = ['email', ]
    readonly_fields = ('id', 'datetime', 'profile',)
    # readonly_fields = ('id', 'datetime',)


admin.site.unregister(Group)
