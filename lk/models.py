# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin

from smart_selects.db_fields import ChainedForeignKey

import catalog
from geo.models import Country, Region


class UserManager(BaseUserManager):

    def create_user(self, email, password):

        if not email:
            raise ValueError('E-mail: обязательное поле')

        if not password:
            raise ValueError('Пароль: обязательное поле')

        user = self.model(
            email=UserManager.normalize_email(email),
        )
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password,
        )

        user.is_staff = True
        user.is_superuser = True
        user.save()

        return user


class Profile(AbstractBaseUser, PermissionsMixin):
    PROFILE_CHOICES = (
        ('human', 'Покупатель'),
        ('company', 'Продавец'),
    )

    # Общие данные
    email = models.EmailField('E-mail', max_length=255, unique=True)
    date_joined = models.DateTimeField('Дата регистрации', auto_now_add=True)
    first_name = models.CharField('Имя', max_length=255, null=True, blank=True)
    last_name = models.CharField('Фамилия', max_length=255, null=True, blank=True)
    phone = models.CharField('Телефон', max_length=255, null=True, blank=True)
    profile_type = models.CharField('Тип профиля', choices=PROFILE_CHOICES, max_length=10, default='human')

    # Данные продавца
    company_name = models.CharField('Название компании', max_length=255, null=True, blank=True)
    image = models.ImageField('Изображение', null=True, blank=True, upload_to='sellers/')
    phone2 = models.CharField('Телефон №2', max_length=255, null=True, blank=True)
    postal_code = models.CharField('Почтовый код', max_length=255, null=True, blank=True)
    country = models.ForeignKey(Country, verbose_name='Страна', null=True, blank=True)
    region = ChainedForeignKey(
        Region, verbose_name='Регион', null=True, blank=True,
        chained_field='country', chained_model_field='country',
        show_all=False, auto_choose=True
    )
    city = models.CharField('Город', max_length=255, null=True, blank=True)
    address1 = models.CharField('Адрес №1', max_length=255, null=True, blank=True)
    address2 = models.CharField('Адрес №2', max_length=255, null=True, blank=True)

    # Управление доступами
    is_staff = models.BooleanField('Имеет доступ к административной панели сайта', default=False)
    is_active = models.BooleanField('Активен', default=True)
    active_sig = models.CharField(max_length=50, default='', blank=True)
    is_blocked = models.BooleanField('Заблокирован', default=False,
                                     help_text='Поставьте галочку, чтобы заблокировать пользователя')

    favorites = models.ManyToManyField('catalog.Product', verbose_name='Список желаний',
                                       blank=True, related_name='in_favorites')
    objects = UserManager()

    USERNAME_FIELD = 'email'

    def get_address(self):
        return '{0} {1}, {2}, город {3}, {4}'.format(self.postal_code, self.country.title, self.region.title, self.city, self.address1)

    class Meta:
        ordering = ['-id', ]
        verbose_name = 'профиль'
        verbose_name_plural = 'профили'

    def get_full_name(self):
        return self.__unicode__()

    def get_short_name(self):
        return (self.company_name if self.profile_type == 'company' and self.company_name
                else self.first_name)

    def has_full_name(self):
        return self.first_name and self.last_name

    def __unicode__(self):
        return (
            '{0} ({1})'.format(self.company_name, self.email) if self.profile_type == 'company' and self.company_name
            else '{0} {1} ({2})'.format(self.first_name, self.last_name, self.email) if self.has_full_name()
            else self.email
        )

    def show_profile_type(self):
        return ('Покупатель' if self.profile_type == 'human'
                else 'Продавец' if self.profile_type == 'company'
                else '')
    show_profile_type.allow_tags = True
    show_profile_type.short_description = 'Тип профиля'

    def show_first_name(self):
        return self.first_name or ''
    show_first_name.allow_tags = True
    show_first_name.short_description = 'Имя'

    def show_last_name(self):
        return self.last_name or ''
    show_last_name.allow_tags = True
    show_last_name.short_description = 'Фамилия'

    def show_company_name(self):
        return self.company_name or ''
    show_company_name.allow_tags = True
    show_company_name.short_description = 'Название компании'

    def get_signature(self):
        self.active_sig = Profile.objects.make_random_password(length=40)
        self.save()
        return self.active_sig

    def activate_user(self):
        self.active_sig = ''
        self.is_active = True
        self.save()

    def reset_pass(self):
        password = Profile.objects.make_random_password(length=10)
        self.active_sig = ''
        self.set_password(password)
        # send_reset_password_email(self, password)
        self.save()

    @property
    def is_seller(self):
        return self.profile_type == 'company'

    def get_carts(self):
        return self.cart_set.filter(checked_out=True)

    # @property
    # def products(self):
    #     return self.product_set.filter(show=True)

    @property
    def reviews(self):
        return catalog.ProductReview.objects.filter(product__seller_id=self.id, show=True)

    @property
    def reviews_average(self):
        reviews = self.reviews.values_list('rating', flat=True)
        return int(round(sum(reviews)/float(len(reviews)) * 20)) if len(reviews) else 0

    @property
    def shown_products(self):
        return self.product_set.filter(show=True)

    def get_favorites(self):
        return self.favorites.filter(show=True)

    def get_seller_url(self):
        return reverse('shop:seller', kwargs={'seller_id': self.id})



class InvoiceRequest(models.Model):
    profile = models.ForeignKey(Profile, verbose_name='Профиль', limit_choices_to={'profile_type': 'company'})
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)

    company_name = models.CharField('Название компании', max_length=255)
    phone = models.CharField('Телефон', max_length=255)
    email = models.EmailField('Email')
    address = models.TextField('Адрес')

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы накладных и счет-фактур'

    def __unicode__(self):
        return '{0} ({1})'.format(self.company_name, self.datetime)


class DocumentsRequest(models.Model):
    profile = models.ForeignKey(Profile, verbose_name='Профиль', limit_choices_to={'profile_type': 'company'})
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)

    company_name = models.CharField('Название компании', max_length=255)
    phone = models.CharField('Телефон', max_length=255)
    email = models.EmailField('Email')
    address = models.TextField('Адрес')

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы документов для юр.лиц'

    def __unicode__(self):
        return '{0} ({1})'.format(self.profile.company_name, self.datetime)


class NewsletterSubscription(models.Model):
    datetime = models.DateTimeField('Дата и время подписки', auto_now_add=True)
    email = models.EmailField('Email')
    profile = models.ForeignKey(Profile, verbose_name='Профиль', null=True, blank=True)

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'подписка'
        verbose_name_plural = 'подписки на рассылку'

    def __unicode__(self):
        return self.email

    def show_profile(self):
        return self.profile or ''
    show_profile.allow_tags = True
    show_profile.short_description = 'Профиль'
