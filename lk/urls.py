# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import RedirectView

from .views import (ProfileHomeView, ProfileTemplateView, OrderView, EditProfileView, RemoveProfileView,
                    SellerTemplateView, SellerOrdersView, InvoiceRequestView, DocumentsRequestView,
                    ProductsView, ProductView,
                    ProductEditView, ProductEditImagesView, ProductEditColorsView, ProductEditRelatedView)
from .api_views import AddToFavorites, RemoveFromFavorites, AddToCompare, RemoveFromCompare
from .update_views import HumanUpdateView, FirstCompanyUpdateView, SecondCompanyUpdateView, PasswordUpdateView


urlpatterns = [
    url(r'^$', ProfileHomeView.as_view(), name='home'),
    url(r'^remove/$', RemoveProfileView.as_view(), name='remove'),

    url(r'^orders/$', ProfileTemplateView.as_view(template_name='profile/orders.html'), name='orders'),
    url(r'^orders/(?P<pk>\d+)/$', OrderView.as_view(), name='order'),

    # url(r'^reviews/$', ProfileTemplateView.as_view(template_name='profile/reviews.html'), name='reviews'),
    url(r'^reviews/$', RedirectView.as_view(pattern_name='profile:home', permanent=False), name='reviews'),

    url(r'^wishlist/$', ProfileTemplateView.as_view(template_name='profile/wishlist.html'), name='wishlist'),
    url(r'^favorites/add/$', AddToFavorites.as_view(), name='add-to-favorites'),
    url(r'^favorites/remove/$', RemoveFromFavorites.as_view(), name='remove-from-favorites'),

    url(r'^compare/add/$', AddToCompare.as_view(), name='add-to-compare'),
    url(r'^compare/remove/$', RemoveFromCompare.as_view(), name='remove-from-compare'),

    url(r'^seller_orders/$', SellerOrdersView.as_view(), name='seller-orders'),

    url(r'^products/$', ProductsView.as_view(), name='products'),
    url(r'^products/(?P<pk>\d+)/$', ProductView.as_view(), name='product'),
    url(r'^products/(?P<pk>\d+)/edit/$', ProductEditView.as_view(), name='product-edit'),
    url(r'^products/(?P<pk>\d+)/edit/images/$', ProductEditImagesView.as_view(), name='product-edit-images'),
    url(r'^products/(?P<pk>\d+)/edit/colors/$', ProductEditColorsView.as_view(), name='product-edit-colors'),
    url(r'^products/(?P<pk>\d+)/edit/related/$', ProductEditRelatedView.as_view(), name='product-edit-related'),

    url(r'^invoice/$', InvoiceRequestView.as_view(), name='invoice'),
    url(r'^documents/$', DocumentsRequestView.as_view(), name='documents'),

    url(r'^edit/$', EditProfileView.as_view(), name='edit'),
    url(r'^edit/human/$', HumanUpdateView.as_view(), name='update-human'),
    url(r'^edit/company/1/$', FirstCompanyUpdateView.as_view(), name='update-company-1'),
    url(r'^edit/company/2/$', SecondCompanyUpdateView.as_view(), name='update-company-2'),
    url(r'^edit/password/$', PasswordUpdateView.as_view(), name='update-password'),
]
