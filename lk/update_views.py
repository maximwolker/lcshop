# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import update_session_auth_hash
from django.core.urlresolvers import reverse
from django.http import Http404, JsonResponse, HttpResponseRedirect
from django.views.generic import UpdateView, FormView

from .forms import HumanUpdateForm, FirstCompanyUpdateForm, SecondCompanyUpdateForm, ChangePasswordForm


class BaseProfileUpdateView(UpdateView):
    form_class = None

    def get(self, request, *args, **kwargs):
        if self.request.user.is_anonymous():
            if self.request.is_ajax():
                raise Http404
            else:
                return HttpResponseRedirect(reverse('index'))
        return super(BaseProfileUpdateView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        return self.request.path

    def get_object(self, queryset=None):
        return self.request.user

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        super(BaseProfileUpdateView, self).form_valid(form)
        return JsonResponse({'result': 'ok'})


class HumanUpdateView(BaseProfileUpdateView):
    form_class = HumanUpdateForm


class FirstCompanyUpdateView(BaseProfileUpdateView):
    form_class = FirstCompanyUpdateForm


class SecondCompanyUpdateView(BaseProfileUpdateView):
    form_class = SecondCompanyUpdateForm


class PasswordUpdateView(FormView):
    form_class = ChangePasswordForm

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.request.user, **self.get_form_kwargs())

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []

        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})

        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        return JsonResponse({'result': 'ok'})
