# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import logout
from django.core.urlresolvers import reverse
from django.http import Http404, JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import RedirectView, DetailView, ListView, TemplateView, View, UpdateView, CreateView

from cart.models import Cart, CartItem
from catalog.forms import EditProductForm
from catalog.models import Category, Product, ProductImage, ProductColor
from .email import (send_new_product_email, send_updated_product_email,
                    send_invoice_request_email, send_documents_request_email)
from .forms import SecondCompanyUpdateForm, InvoiceRequestForm, DocumentsRequestForm


class ProfileHomeView(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        profile = self.request.user
        return ('/auth/login/' if profile.is_anonymous() or profile.is_blocked
                else reverse('profile:orders') if not profile.profile_type == 'company'
                else reverse('profile:seller-orders'))


class ProfileMixin(object):

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous() or request.user.is_blocked:
            return HttpResponseRedirect('/auth/login/')
        return super(ProfileMixin, self).get(request, *args, **kwargs)


class ProfileTemplateView(ProfileMixin, TemplateView):
    pass


class OrderView(ProfileMixin, DetailView):
    template_name = "profile/order.html"
    model = Cart
    context_object_name = 'order'

    def get_object(self, queryset=None):
        order = super(OrderView, self).get_object(queryset)
        if order.customer_id != self.request.user.id or order.checked_out is False:
            raise Http404
        return order


class EditProfileView(View):

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous() or request.user.is_blocked:
            return HttpResponseRedirect('/auth/login/')
        elif request.user.profile_type == 'human':
            return EditHumanView.as_view()(request, *args, **kwargs)
        else:
            return EditCompanyView.as_view()(request, *args, **kwargs)


class EditHumanView(TemplateView):
    template_name = "profile/edit.html"


class EditCompanyView(UpdateView):
    template_name = "profile/edit.html"
    form_class = SecondCompanyUpdateForm

    def get_object(self, queryset=None):
        return self.request.user


class RemoveProfileView(ProfileTemplateView):
    template_name = "profile/remove.html"

    def post(self, request, *args, **kwargs):
        profile = request.user
        logout(request)
        profile.delete()
        return HttpResponseRedirect(redirect_to=reverse('index'))


class SellerMixin(object):

    def get(self, request, *args, **kwargs):
        if request.user.is_anonymous() or request.user.is_blocked:
            return HttpResponseRedirect(reverse('index'))
        elif not request.user.profile_type == 'company':
            return HttpResponseRedirect(reverse('profile:home'))
        return super(SellerMixin, self).get(request, *args, **kwargs)


class SellerTemplateView(SellerMixin, TemplateView):
    pass


class SellerOrdersView(SellerMixin, ListView):
    template_name = 'profile/seller_orders.html'
    model = Cart
    context_object_name = 'cart_items'

    def get_queryset(self):
        profile = self.request.user
        products = Product.all_objects.filter(seller_id=profile.id).values_list('id', flat=True)
        # cart_items = CartItem.objects.filter(product_id__in=products, cart__checked_out=True)
        # import ipdb
        # ipdb.set_trace()

        return CartItem.objects.filter(product_id__in=products, cart__checked_out=True)
        # return cart_items


class SellerCreateView(SellerMixin, CreateView):
    template_name = ''
    form_class = None
    email_function = None

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        form.instance.profile = self.request.user
        super(SellerCreateView, self).form_valid(form)
        self.email_function(self.request, form.instance)
        data = {
            'errors': [],
            'result': 'ok',
        }
        return JsonResponse(data)

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)


class InvoiceRequestView(SellerCreateView):
    template_name = 'profile/invoice.html'
    form_class = InvoiceRequestForm
    email_function = send_invoice_request_email


class DocumentsRequestView(SellerCreateView):
    template_name = 'profile/documents.html'
    form_class = DocumentsRequestForm
    email_function = send_documents_request_email


class ProductsView(SellerTemplateView):
    template_name = 'profile/products.html'

    def get_context_data(self, **kwargs):
        context = super(ProductsView, self).get_context_data(**kwargs)
        removed_id = int(self.request.GET.get('r', 0))
        if removed_id:
            removed_product = Product.all_objects.filter(id=removed_id).first()
            if removed_product and removed_product.seller_id == self.request.user.id and removed_product.removed is True:
                context['removed_product'] = removed_product
        return context


class ProductView(SellerMixin, DetailView):
    template_name = 'profile/product.html'
    model = Product
    context_object_name = 'product'

    def get_object(self, queryset=None):
        product = super(ProductView, self).get_object(queryset)
        if product.seller_id != self.request.user.id:
            raise Http404
        return product

    def post(self, request, *args, **kwargs):
        product = self.get_object()
        if product.seller_id != request.user.id:
            raise Http404
        product.removed = True
        product.save()
        return HttpResponseRedirect(redirect_to='{0}?r={1}'.format(reverse('profile:products'), product.id))


class ProductEditView(SellerMixin, UpdateView):
    model = Product
    form_class = EditProductForm
    template_name = 'profile/product_edit.html'

    def get_object(self, queryset=None):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_form(self, form_class=None):
        """
        https://jacobian.org/writing/dynamic-form-generation/
        """
        product = self.get_object()
        shown = product.show
        category = product.category
        attrs = product.attrs

        category_id = self.request.POST.get('category') or product.category_id
        extra_fields = Category.objects.get(id=category_id).get_new_attrs() if category_id else []
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.request.user, extra_fields, shown, category, attrs, **self.get_form_kwargs())

    def form_valid(self, form):
        self.object, category_changed, was_product_updated_before, changed_fields = form.save()
        if self.object.show and len(changed_fields):  # and not was_product_updated_before:
            send_updated_product_email(self.request, self.object, changed_fields)

        message = ('info' if len(changed_fields) == 0
                   else 'info1' if category_changed
                   else 'success1' if self.object.show
                   else 'success2' if self.object.admin_emailed_at_new
                   else 'success3')
        return HttpResponseRedirect('{0}?r={1}'.format(self.request.path, message))


class ProductEditImagesView(SellerTemplateView):
    template_name = 'profile/product_edit_images.html'
    error = ''
    success = ''

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        product = self.get_product()
        images_count = product.images.count()
        context = {
            'product': product,
            'images_range': xrange(images_count+1, 8) if images_count < 7 else [],
            'error': self.error,
            'success': self.success,
        }
        context.update(super(ProductEditImagesView, self).get_context_data(**kwargs))
        return context

    def post(self, request, *args, **kwargs):
        product = self.get_product()
        removed_images = request.POST.getlist('removed_image')
        new_images = request.FILES.getlist('image')

        if product.images.count() == len(removed_images) and len(new_images) == 0:
            self.error = 'Нужно загрузить хотя бы одну картинку.'

        else:
            for i, image in enumerate(new_images):
                if image.content_type not in ['image/png', 'image/jpeg', 'image/gif']:
                    error = 'Изображение #{0}: неверный формат файла.'.format(i+product.images.count()+1)
                    self.error = '{0}\r\n{1}'.format(self.error, error) if self.error else error

        if not self.error:
            for removed_image_id in removed_images:
                try:
                    image = ProductImage.objects.get(id=int(removed_image_id))
                    image.delete()
                except (ProductImage.DoesNotExist, ValueError) as e:
                    pass

            for image in new_images:
                ProductImage.objects.create(product=product, image=image)
            self.success = 'Изображения успешно обновлены.'
            if product.admin_emailed_at_new == False:
                send_new_product_email(request, product)
                product.admin_emailed_at_new = True
                product.save()
                self.success = '{0}\r\n{1}'.format(self.success, 'Товар будет опубликован после проверки модератором.')

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class ProductEditColorsView(SellerTemplateView):
    template_name = 'profile/product_edit_colors.html'
    success = ''
    error = ''

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        context = {
            'product': self.get_product(),
            'success': self.success,
            'error': self.error,
        }
        context.update(super(ProductEditColorsView, self).get_context_data(**kwargs))
        return context

    def post(self, request, *args, **kwargs):
        product = self.get_product()
        colors = []

        form_data = dict(request.POST)
        i = 0
        while 'color_name[%s]' % i in form_data and 'color[%s]' % i in form_data:
            if not form_data['color_name[%s]' % i][0]:
                continue
            if not form_data['color[%s]' % i][0]:
                continue
            colors.append({'name': form_data['color_name[%s]' % i][0], 'hex': form_data['color[%s]' % i][0]})
            del form_data['color_name[%s]' % i]
            del form_data['color[%s]' % i]
            i += 1

        for color in product.colors.all():
            color.delete()
        for color in colors:
            ProductColor.objects.create(product=product, title=color['name'], hex_value=color['hex'])
        self.success = 'Цвета успешно обновлены.'

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class ProductEditRelatedView(SellerTemplateView):
    template_name = 'profile/product_edit_related.html'
    success = ''
    error = ''

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        context = {
            'product': self.get_product(),
            'success': self.success,
            'error': self.error,
        }
        context.update(super(ProductEditRelatedView, self).get_context_data(**kwargs))
        return context

    def post(self, request, *args, **kwargs):
        seller_id = request.user.id
        product = self.get_product()
        product.clear_related_from_seller()

        related = request.POST.getlist('related')
        for p_id in related:
            p = Product.objects.filter(id=p_id, seller_id=seller_id).first()
            if p:
                product.related.add(p)
        self.success = 'Блок "С этим товаром также покупают" обновлен.'

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
