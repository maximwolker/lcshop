# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lk', '0005_auto_20150908_1407'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsletterSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0434\u043f\u0438\u0441\u043a\u0438')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u043f\u043e\u0434\u043f\u0438\u0441\u043a\u0430',
                'verbose_name_plural': '\u043f\u043e\u0434\u043f\u0438\u0441\u043a\u0438 \u043d\u0430 \u0440\u0430\u0441\u0441\u044b\u043b\u043a\u0443',
            },
        ),
    ]
