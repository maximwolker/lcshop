# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_auto_20150907_1332'),
        ('lk', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='favorites',
            field=models.ManyToManyField(related_name='in_favorites', verbose_name='\u0418\u0437\u0431\u0440\u0430\u043d\u043d\u043e\u0435', to='catalog.Product', blank=True),
        ),
    ]
