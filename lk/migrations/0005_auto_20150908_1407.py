# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('lk', '0004_auto_20150908_1346'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='region',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field='country', chained_field='country', verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True, auto_choose=True, to='geo.Region', null=True),
        ),
    ]
