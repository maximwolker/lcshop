# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('lk', '0007_auto_20151002_1825'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentsRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('company_name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438')),
                ('phone', models.CharField(max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('email', models.EmailField(max_length=254, verbose_name='Email')),
                ('address', models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u043e\u0432 \u0434\u043b\u044f \u044e\u0440.\u043b\u0438\u0446',
            },
        ),
        migrations.AlterModelOptions(
            name='invoicerequest',
            options={'ordering': ['-datetime'], 'verbose_name': '\u0437\u0430\u043a\u0430\u0437', 'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b \u043d\u0430\u043a\u043b\u0430\u0434\u043d\u044b\u0445 \u0438 \u0441\u0447\u0435\u0442-\u0444\u0430\u043a\u0442\u0443\u0440'},
        ),
        migrations.AddField(
            model_name='invoicerequest',
            name='address',
            field=models.TextField(default='-', verbose_name='\u0410\u0434\u0440\u0435\u0441'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoicerequest',
            name='company_name',
            field=models.CharField(default='-', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoicerequest',
            name='email',
            field=models.EmailField(default='qwe@qwe.qwe', max_length=254, verbose_name='Email'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='invoicerequest',
            name='phone',
            field=models.CharField(default='-', max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
            preserve_default=False,
        ),
    ]
