# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name='E-mail')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u0438')),
                ('first_name', models.CharField(max_length=255, null=True, verbose_name='\u0418\u043c\u044f', blank=True)),
                ('last_name', models.CharField(max_length=255, null=True, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f', blank=True)),
                ('phone', models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('profile_type', models.CharField(default='human', max_length=10, verbose_name='\u0422\u0438\u043f \u043f\u0440\u043e\u0444\u0438\u043b\u044f', choices=[('human', '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c'), ('company', '\u041f\u0440\u043e\u0434\u0430\u0432\u0435\u0446')])),
                ('company_name', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', blank=True)),
                ('address1', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u21161', blank=True)),
                ('address2', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u21162', blank=True)),
                ('city', models.CharField(max_length=255, null=True, verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True)),
                ('region', models.CharField(max_length=255, null=True, verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True)),
                ('postal_code', models.CharField(max_length=255, null=True, verbose_name='\u041f\u043e\u0447\u0442\u043e\u0432\u044b\u0439 \u043a\u043e\u0434', blank=True)),
                ('country', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True)),
                ('phone2', models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d \u21162', blank=True)),
                ('is_staff', models.BooleanField(default=False, verbose_name='\u0418\u043c\u0435\u0435\u0442 \u0434\u043e\u0441\u0442\u0443\u043f \u043a \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u043f\u0430\u043d\u0435\u043b\u0438 \u0441\u0430\u0439\u0442\u0430')),
                ('is_active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u0435\u043d')),
                ('active_sig', models.CharField(default='', max_length=50, blank=True)),
                ('is_blocked', models.BooleanField(default=False, help_text='\u041f\u043e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u0433\u0430\u043b\u043e\u0447\u043a\u0443, \u0447\u0442\u043e\u0431\u044b \u0437\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f', verbose_name='\u0417\u0430\u0431\u043b\u043e\u043a\u0438\u0440\u043e\u0432\u0430\u043d')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'ordering': ['-id'],
                'verbose_name': '\u043f\u0440\u043e\u0444\u0438\u043b\u044c',
                'verbose_name_plural': '\u043f\u0440\u043e\u0444\u0438\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='InvoiceRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b \u043d\u0430\u043a\u043b\u0430\u0434\u043d\u044b\u0445',
            },
        ),
        migrations.CreateModel(
            name='ProductSearchRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('email', models.EmailField(max_length=255, verbose_name='Email')),
                ('phone', models.CharField(max_length=255, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('request', models.CharField(max_length=255, verbose_name='\u0417\u0430\u043f\u0440\u043e\u0441')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b \u043f\u043e\u0438\u0441\u043a\u0430 \u0442\u043e\u0432\u0430\u0440\u0430',
            },
        ),
    ]
