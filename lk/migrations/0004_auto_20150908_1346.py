# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lk', '0003_auto_20150908_1346'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='country_temp',
            new_name='country',
        ),
        migrations.RenameField(
            model_name='profile',
            old_name='region_temp',
            new_name='region',
        ),
    ]
