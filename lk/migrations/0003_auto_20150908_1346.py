# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0001_initial'),
        ('lk', '0002_profile_favorites'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='country',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='region',
        ),
        migrations.AddField(
            model_name='profile',
            name='country_temp',
            field=models.ForeignKey(verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True, to='geo.Country', null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='region_temp',
            field=models.ForeignKey(verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True, to='geo.Region', null=True),
        ),
    ]
