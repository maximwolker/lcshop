# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lk', '0006_newslettersubscription'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productsearchrequest',
            name='profile',
        ),
        migrations.DeleteModel(
            name='ProductSearchRequest',
        ),
    ]
