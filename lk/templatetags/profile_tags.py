# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template


register = template.Library()


@register.assignment_tag(takes_context=True)
def get_profile_slug(context):
    """
    Получаем slug текущей страницы
    (для меню пользователя)
    """
    path = context['request'].path
    slug = path.split('/')[2]
    show_link = slug != path.split('/')[-1]
    return [(slug, show_link)]
