# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template

from crequest.middleware import CrequestMiddleware

from site_settings.models import Setting


def email_admin(request, subject, email_key, obj, settings_key='feedback_email', **kwargs):
    from_email = settings.DEFAULT_FROM_EMAIL
    try:
        to = [s.strip() for s in Setting.objects.get(key=settings_key).value.split('\r\n')]
    except Setting.DoesNotExist:
        to = [settings.ADMINS[0][1]]
    try:
        site = request.get_host()
    except AttributeError:
        site = 'alazone.kz'

    template_text = get_template('email/to_admin/{0}.txt'.format(email_key))
    template_html = get_template('email/to_admin/{0}.html'.format(email_key))

    context = Context({'object': obj, 'subject': subject, 'site': site})
    context.update(kwargs)
    text_content = template_text.render(context)
    html_content = template_html.render(context)

    message = EmailMultiAlternatives(subject, text_content, from_email, to)
    message.attach_alternative(html_content, "text/html")
    message.send()


def send_feedback_email(request, obj):
    subject = 'Alazone: Новое сообщение из формы обратной связи'
    email_key = 'feedback'
    email_admin(request, subject, email_key, obj)


def send_question_email(request, obj):
    subject = 'Alazone: Новый вопрос со страницы службы поддержки'
    email_key = 'question'
    email_admin(request, subject, email_key, obj)


def send_request_email(request, obj):
    subject = 'Alazone: Новый запрос на поиск товара'
    email_key = 'request'
    email_admin(request, subject, email_key, obj)


def send_new_product_email(request, product):
    subject = 'Alazone: Новый товар в каталоге'
    email_key = 'new_product'
    email_admin(request, subject, email_key, product)


def send_updated_product_email(request, product, changed_fields):
    subject = 'Alazone: Данные о товаре были изменены (и нуждаются в проверке)'
    email_key = 'updated_product'
    product_kwargs = {'changed_fields': changed_fields}
    email_admin(request, subject, email_key, product, **product_kwargs)


def send_admin_order_email(request, order):
    subject = 'Alazone: Новый заказ'
    email_key = 'order'
    email_admin(request, subject, email_key, order)


def send_invoice_request_email(_, request, order):
    subject = 'Alazone: Новый заказ накладных и счет-фактур'
    email_key = 'invoice_request'
    email_admin(request, subject, email_key, order)


def send_documents_request_email(_, request, order):
    subject = 'Alazone: Новый заказ документов на юр.лицо'
    email_key = 'documents_request'
    email_admin(request, subject, email_key, order)


def email_user(subject, email_key, profile=None, email=None, request=None, **kwargs):
    if not (email or profile):
        return

    request = request or CrequestMiddleware.get_request()

    from_email = settings.DEFAULT_FROM_EMAIL
    to = [email or profile.email]

    try:
        site = request.get_host()
    except AttributeError:
        site = 'alazone.kz'

    template_text = get_template('email/to_user/{0}.txt'.format(email_key))
    template_html = get_template('email/to_user/{0}.html'.format(email_key))

    subject = subject

    context = Context({'profile': profile, 'subject': subject, 'site': site})
    context.update(kwargs)
    text_content = template_text.render(context)
    html_content = template_html.render(context)

    message = EmailMultiAlternatives(subject, text_content, from_email, to)
    message.attach_alternative(html_content, "text/html")
    message.send()
    return True


def send_customer_order_email(email, request, order):
    subject = 'Ваш заказ на Alazone'
    email_key = 'order'
    order_kwargs = {'order': order}
    email_user(subject, email_key, email=email, request=request, **order_kwargs)
