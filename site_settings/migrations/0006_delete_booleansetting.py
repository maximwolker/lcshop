# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_settings', '0005_load_seosettings'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BooleanSetting',
        ),
    ]
