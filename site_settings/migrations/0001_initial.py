# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BooleanSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.SlugField(unique=True, max_length=255, verbose_name='\u041a\u043e\u0434')),
                ('value', models.BooleanField(default=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'ordering': ['key'],
                'verbose_name': '\u043b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u0430\u044f',
                'verbose_name_plural': '\u043b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0435 (boolean)',
            },
        ),
        migrations.CreateModel(
            name='SEOSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.SlugField(unique=True, max_length=255, verbose_name='\u041a\u043e\u0434')),
                ('description', models.CharField(max_length=255, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430')),
                ('title', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b (\u0432\u044b\u0448\u0435)', max_length=255, null=True, verbose_name='Title', blank=True)),
                ('meta_desc', models.TextField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_desc', max_length=255, null=True, verbose_name='Meta description (\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435)', blank=True)),
                ('meta_keyw', models.TextField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_keyw', max_length=255, null=True, verbose_name='Meta keywords (\u043a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e)', blank=True)),
            ],
            options={
                'ordering': ['key'],
                'verbose_name': 'SEO-\u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0430',
                'verbose_name_plural': 'SEO-\u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='Setting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.SlugField(unique=True, max_length=255, verbose_name='\u041a\u043e\u0434')),
                ('value', models.TextField(null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'ordering': ['key'],
                'verbose_name': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u0430\u044f \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u0430\u044f',
                'verbose_name_plural': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0435 \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0435',
            },
        ),
        migrations.CreateModel(
            name='VisualSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.SlugField(unique=True, max_length=255, verbose_name='\u041a\u043e\u0434')),
                ('value', ckeditor.fields.RichTextField(null=True, verbose_name='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435', blank=True)),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
            ],
            options={
                'ordering': ['key'],
                'verbose_name': '\u0442\u0435\u043a\u0441\u0442. \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u0430\u044f (wysiwyg)',
                'verbose_name_plural': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0435 \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0435 (wysiwyg)',
            },
        ),
    ]
