# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import Setting, VisualSetting, SEOSetting  # BooleanSetting


def settings(request):
    settings = {key: value for key, value in Setting.objects.values_list('key', 'value')}
    visual_settings = {key: value for key, value in VisualSetting.objects.values_list('key', 'value')}
    # boolean_settings = {key: value for key, value in BooleanSetting.objects.values_list('key', 'value')}
    settings.update(visual_settings)
    # settings.update(boolean_settings)
    seo_settings = {setting.key: setting for setting in SEOSetting.objects.all()}

    return {
        'settings': settings,
        'seo_settings': seo_settings,
    }
