# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Setting, VisualSetting, SEOSetting  # BooleanSetting


def MetatagModelAdmin(cls=None):

    def decorator(cls):
        cls.fieldsets += (
            ('SEO', {
                # 'classes': ('collapse',),
                'classes': ('grp-collapse grp-closed',),
                'fields': ('meta_title', 'meta_description', 'meta_keywords',)
            }),
        )
        cls.search_fields += ['meta_title', 'meta_description', 'meta_keywords', ]
        return cls

    if cls is None:
        return decorator
    else:
        return decorator(cls)


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ('key', 'value', 'description',)
    readonly_fields = ('key',)

    fieldsets = (
        (None, {
            'fields': ('key', 'value', 'description',)
        }),
    )

    def has_add_permission(self, request):
        return None

    def has_delete_permission(self, request, obj=None):
        return None


@admin.register(VisualSetting)
class VisualSettingAdmin(admin.ModelAdmin):
    list_display = ('key', 'description',)
    readonly_fields = ('key',)

    fieldsets = (
        (None, {
            'fields': ('key', 'value', 'description',)
        }),
    )

    def has_add_permission(self, request):
        return None

    def has_delete_permission(self, request, obj=None):
        return None


@admin.register(SEOSetting)
class SEOSettingAdmin(admin.ModelAdmin):
    list_display = ('key', 'description', 'title', 'meta_desc', 'meta_keyw',)
    list_display_links = ('key', 'description',)
    readonly_fields = ('key',)

    fieldsets = (
        (None, {
            'fields': ('key', 'description', 'title', 'meta_desc', 'meta_keyw',)
        }),
    )

    def has_add_permission(self, request):
        return None

    def has_delete_permission(self, request, obj=None):
        return None
