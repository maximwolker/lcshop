# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from ckeditor.fields import RichTextField


class Setting(models.Model):
    key = models.SlugField("Код", max_length=255, unique=True)
    value = models.TextField("Значение", null=True, blank=True)
    description = models.TextField("Описание")

    class Meta:
        ordering = ['key', ]
        verbose_name = "текстовая переменная"
        verbose_name_plural = "текстовые переменные"

    def __unicode__(self):
        return self.description


class VisualSetting(models.Model):
    key = models.SlugField("Код", max_length=255, unique=True)
    value = RichTextField("Значение", null=True, blank=True)
    description = models.TextField("Описание")

    class Meta:
        ordering = ['key', ]
        verbose_name = "текст. переменная (wysiwyg)"
        verbose_name_plural = "текстовые переменные (wysiwyg)"

    def __unicode__(self):
        return self.description


# class BooleanSetting(models.Model):
#     key = models.SlugField("Код", max_length=255, unique=True)
#     value = models.BooleanField("Значение", default=True)
#     description = models.TextField("Описание")
#
#     class Meta:
#         ordering = ['key', ]
#         verbose_name = "логическая переменная"
#         verbose_name_plural = "логические переменные (boolean)"
#
#     def __unicode__(self):
#         return self.description


class SEOSetting(models.Model):
    key = models.SlugField('Код', max_length=255, unique=True)
    description = models.CharField('Страница', max_length=255)
    title = models.CharField(
        'Title',
        max_length=255, null=True, blank=True,
        help_text='Оставьте пустым, чтобы использовать название страницы (выше)',
    )
    meta_desc = models.TextField(
        'Meta description (описание)',
        max_length=255, null=True, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_desc',
    )
    meta_keyw = models.TextField(
        'Meta keywords (ключевые слова через запятую)',
        max_length=255, null=True, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_keyw',
    )

    class Meta:
        ordering = ['key', ]
        verbose_name = 'SEO-настройка'
        verbose_name_plural = 'SEO-настройки'

    def __unicode__(self):
        return self.key

    def get_meta_title(self):
        return self.title if self.title else '{0} — Alazone'.format(self.description)

    def get_meta_desc(self):
        return self.meta_desc if self.meta_desc else SEOSetting.objects.get(key='global').meta_desc

    def get_meta_keyw(self):
        return self.meta_keyw if self.meta_keyw else SEOSetting.objects.get(key='global').meta_keyw


class MetatagModel(models.Model):
    meta_title = models.CharField(
        'Meta title (заголовок страницы)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать поле "Заголовок"',
    )
    meta_description = models.CharField(
        'Meta description (описание страницы)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_desc',
    )
    meta_keywords = models.CharField(
        'Meta keywords (ключевые слова через запятую)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_keyw',
    )

    class Meta:
        abstract = True

    def get_meta_title(self):
        return self.meta_title if self.meta_title else '{0} — Alazone'.format(self.title)

    def get_meta_desc(self):
        return self.meta_description if self.meta_description else SEOSetting.objects.get(key='global').meta_desc

    def get_meta_keyw(self):
        return self.meta_keywords if self.meta_keywords else SEOSetting.objects.get(key='global').meta_keyw
