{{ subject }}

Дата и время поступления: {{ object.datetime|date:"d.m.Y H:i:s" }}

{% if object.profile %}Профиль: {{ object.profile }} (http://{{ site }}/alazone-adm/lk/profile/{{ object.profile.id }}/){% endif %}
Имя: {{ object.name }}
Email: {{ object.email }}
Тема: {{ object.subject }}

Сообщение:
{{ object.message|safe }}

{% if site %}Ссылка на сообщение в админке: http://{{ site }}/alazone-adm/content/contactsmessage/{{ object.id }}/{% endif %}
