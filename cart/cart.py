# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from . import models


CART_ID = 'CART-ID'


class ItemAlreadyExists(Exception):
    pass


class ItemDoesNotExist(Exception):
    pass


class Cart:

    def __init__(self, request):
        cart_id = request.session.get(CART_ID)
        if cart_id:
            try:
                cart = models.Cart.objects.get(id=cart_id, checked_out=False)
            except models.Cart.DoesNotExist:
                cart = self.new(request)
        else:
            cart = self.new(request)
        self.cart = cart

    def new(self, request):
        if request.user.is_authenticated():
            cart = models.Cart(customer=request.user)
        else:
            cart = models.Cart()
        cart.save()
        request.session[CART_ID] = cart.id
        return cart

    def set(self, product, color=None, quantity=1):
        Item = models.CartItem
        if quantity > 0:
            try:
                item = Item.objects.get(
                    cart=self.cart,
                    product_id=product.id,
                    color_id=color.id if color else 0,
                )
                item.quantity = quantity
                if color:
                    item.color_title = color.title
                    item.color_hex = color.hex_value
                    item.color_id = color.id
                # item.price = product.get_price()

            except Item.DoesNotExist:
                item = Item()
                item.cart = self.cart
                item.product = product
                item.quantity = quantity
                item.color_id = color.id if color else 0
                if color:
                    item.color_title = color.title
                    item.color_hex = color.hex_value
                # item.price = product.get_price()

            if product.quantity < item.quantity:
                return False
            else:
                item.save()
                self.cart.save()
                return True
        else:
            return True

    def add(self, product, color=None, quantity=1):
        Item = models.CartItem
        if quantity > 0:
            try:
                item = Item.objects.get(
                    cart=self.cart,
                    product_id=product.id,
                    color_id=color.id if color else 0,
                )
                item.quantity += int(quantity)
                if color:
                    item.color_title = color.title
                    item.color_hex = color.hex_value
                    item.color_id = color.id
                # item.price = product.get_price()

            except Item.DoesNotExist:
                item = Item()
                item.cart = self.cart
                item.product = product
                item.quantity = quantity
                item.color_id = color.id if color else 0
                if color:
                    item.color_title = color.title
                    item.color_hex = color.hex_value
                # item.price = product.get_price()

            if product.quantity < item.quantity:
                return False
            else:
                item.save()
                self.cart.save()
                return True
        else:
            return True

    def remove(self, product, color=None):
        Item = models.CartItem
        try:
            item = Item.objects.get(
                cart=self.cart,
                product_id=product.id,
                color_id=color.id if color else 0,
            )
            item.delete()
            self.cart.save()

        except Item.DoesNotExist:
            raise ItemDoesNotExist

    def count(self):
        return self.cart.count()

    def summary(self):
        return self.cart.summary

    def clear(self):
        for item in self.cart.cartitem_set.all():
            item.delete()
        self.cart.delete()

    def get_basket(self):
        # import ipdb
        # ipdb.set_trace()
        basket = {
            'items': [self.get_item(item) for item in self.cart.cartitem_set.all()],
            'total_products_word': 'товаров',
            'total': self.count(),
            'total_price': str(self.summary()),
        }
        return basket

    def get_item(self, item):
        product = item.product
        cart_item = {
            'color': {
                'hex': item.color_hex,
                'id': str(item.color_id),
                'name': item.color_title,
            },
            'product': {
                'price': str(item.price),
                'brand': '1',
                'id': product.id,
                'image': product.get_cover_sliced_url(),
                'name': product.title
            },
            'position_price': str(item.total_price),
            'quantity': item.quantity
        }
        return cart_item
