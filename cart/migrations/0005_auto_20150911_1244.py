# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0004_cart_phone2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='delivery_type',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', choices=[('0', '\u0421\u0430\u043c\u043e\u0432\u044b\u0432\u043e\u0437'), ('1', '\u041a\u0443\u0440\u044c\u0435\u0440\u0441\u043a\u0430\u044f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0430'), ('2', '\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u043f\u0435\u0440\u0432\u043e\u0433\u043e \u043a\u043b\u0430\u0441\u0441\u0430'), ('3', '\u041f\u043e\u0447\u0442\u0430')]),
        ),
        migrations.AlterField(
            model_name='cart',
            name='email',
            field=models.EmailField(max_length=255, null=True, verbose_name='Email', blank=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='payment_type',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='\u0422\u0438\u043f \u043e\u043f\u043b\u0430\u0442\u044b', choices=[('0', '\u041f\u043b\u0430\u0441\u0442\u0438\u043a\u043e\u0432\u044b\u0435 \u043a\u0430\u0440\u0442\u044b'), ('1', '\u041d\u0430\u043b\u0438\u0447\u043d\u044b\u0435'), ('2', '\u0414\u043e\u043b\u0433 \u043f\u043e \u0434\u0440\u0443\u0436\u0431\u0435')]),
        ),
    ]
