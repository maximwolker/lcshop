# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0005_auto_20150911_1244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='summary',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430, \u0442\u043d\u0433', max_digits=18, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='cartitem',
            name='price',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430, \u0442\u043d\u0433', max_digits=18, decimal_places=2),
        ),
    ]
