# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0003_auto_20150904_0843'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='phone2',
            field=models.CharField(max_length=30, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d \u21162', blank=True),
        ),
    ]
