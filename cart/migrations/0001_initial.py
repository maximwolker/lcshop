# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20150817_1138'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('checked_out', models.BooleanField(default=False, verbose_name='\u041a\u043e\u0440\u0437\u0438\u043d\u0430 \u043e\u0444\u043e\u0440\u043c\u043b\u0435\u043d\u0430')),
                ('checkout_date', models.DateTimeField(verbose_name='\u0414\u0430\u0442\u0430 \u043e\u0444\u043e\u0440\u043c\u043b\u0435\u043d\u0438\u044f')),
                ('first_name', models.CharField(max_length=255, null=True, verbose_name='\u0418\u043c\u044f', blank=True)),
                ('last_name', models.CharField(max_length=255, null=True, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f', blank=True)),
                ('email', models.EmailField(max_length=255, null=True, verbose_name='E-mail', blank=True)),
                ('phone', models.CharField(max_length=30, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True)),
                ('address1', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u21161', blank=True)),
                ('address2', models.CharField(max_length=255, null=True, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u21162', blank=True)),
                ('city', models.CharField(max_length=255, null=True, verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True)),
                ('region', models.CharField(max_length=255, null=True, verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True)),
                ('postal_code', models.CharField(max_length=255, null=True, verbose_name='\u041f\u043e\u0447\u0442\u043e\u0432\u044b\u0439 \u043a\u043e\u0434', blank=True)),
                ('country', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430', blank=True)),
                ('tracking_number', models.CharField(max_length=255, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u043e\u0442\u0441\u043b\u0435\u0436\u0438\u0432\u0430\u043d\u0438\u044f', blank=True)),
                ('delivery_type', models.CharField(blank=True, max_length=15, null=True, verbose_name='\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', choices=[('one', '\u0442\u0438\u043f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438 #1'), ('two', '\u0442\u0438\u043f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438 #2')])),
                ('payment_type', models.CharField(blank=True, max_length=15, null=True, verbose_name='\u0422\u0438\u043f \u043e\u043f\u043b\u0430\u0442\u044b', choices=[('one', '\u0442\u0438\u043f \u043e\u043f\u043b\u0430\u0442\u044b #1'), ('two', '\u0442\u0438\u043f \u043e\u043f\u043b\u0430\u0442\u044b #2')])),
                ('status', models.PositiveSmallIntegerField(default=1, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441', choices=[(1, '\u041d\u043e\u0432\u044b\u0439'), (2, '\u041f\u0440\u0438\u043d\u044f\u0442'), (3, '\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0430\u043d'), (4, '\u041e\u043f\u043b\u0430\u0447\u0435\u043d'), (5, '\u041e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d'), (6, '\u0417\u0430\u0432\u0435\u0440\u0448\u0435\u043d'), (7, '\u041e\u0442\u043c\u0435\u043d\u0435\u043d')])),
                ('summary', models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430, \u0442\u0433', max_digits=18, decimal_places=2)),
                ('customer', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-creation_date',),
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b',
            },
        ),
        migrations.CreateModel(
            name='CartItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.PositiveIntegerField(verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('price', models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430, \u0442\u0433', max_digits=18, decimal_places=2)),
                ('cart', models.ForeignKey(to='cart.Cart')),
                ('product', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='catalog.Product')),
            ],
            options={
                'ordering': ('cart',),
                'verbose_name': '\u0442\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0442\u043e\u0432\u0430\u0440\u044b',
            },
        ),
    ]
