# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0006_auto_20151116_0232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartitem',
            name='color_hex',
            field=models.CharField(max_length=7, verbose_name='\u0426\u0432\u0435\u0442: HEX-\u043f\u0440\u0435\u0434\u0441\u0442\u0430\u0432\u043b\u0435\u043d\u0438\u0435', blank=True),
        ),
        migrations.AlterField(
            model_name='cartitem',
            name='color_id',
            field=models.IntegerField(verbose_name='\u0426\u0432\u0435\u0442: id'),
        ),
        migrations.AlterField(
            model_name='cartitem',
            name='color_title',
            field=models.CharField(max_length=255, verbose_name='\u0426\u0432\u0435\u0442', blank=True),
        ),
    ]
