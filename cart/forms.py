# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from core.utils import validate_password
from lk.models import Profile
from .models import Cart as CartModel


# class CheckoutFormWithoutRegistration(forms.Form):
#     delivery_type = forms.CharField(widget=forms.PasswordInput)
#     payment_type = forms.CharField(widget=forms.PasswordInput)


class CheckoutFormWithoutRegistration(forms.ModelForm):

    class Meta:
        model = CartModel
        fields = ('email', 'first_name', 'last_name', 'phone', 'phone2',
                  'address1', 'address2', 'city', 'region', 'postal_code', 'country',
                  'delivery_type', 'payment_type',)

    def __init__(self, *args, **kwargs):
        super(CheckoutFormWithoutRegistration, self).__init__(*args, **kwargs)

        for _, field in self.fields.iteritems():
            if field.label not in ('Телефон №2', 'Адрес №2'):
                field.required = True

        self.fields['delivery_type'].widget.attrs.update({'class': 'form-control input-lg'})
        self.fields['payment_type'].widget.attrs.update({'class': 'form-control input-lg'})


class CheckoutFormWithRegistration(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(widget=forms.PasswordInput, required=True)

    class Meta:
        model = CartModel
        fields = ('email', 'first_name', 'last_name', 'phone', 'phone2',
                  'address1', 'address2', 'city', 'region', 'postal_code', 'country',
                  'delivery_type', 'payment_type',)

    def __init__(self, *args, **kwargs):
        super(CheckoutFormWithRegistration, self).__init__(*args, **kwargs)

        for _, field in self.fields.iteritems():
            if field.label not in ('Телефон №2', 'Адрес №2'):
                field.required = True

        self.fields['delivery_type'].widget.attrs.update({'class': 'form-control input-lg'})
        self.fields['payment_type'].widget.attrs.update({'class': 'form-control input-lg'})

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        res = validate_password(password1)
        if res != 'ok':
            raise forms.ValidationError(res)
        return password1

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        print password1, password2
        if not password1 == password2:
            raise forms.ValidationError('Пароли не совпадают')
        return password2

    # def clean_password2(self):
    #     password1 = self.cleaned_data.get('password1')
    #     password2 = self.cleaned_data.get('password2')
    #     if not password1 == password2:
    #         raise forms.ValidationError('')
    #     return password2

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if Profile.objects.filter(email=email).count():
            raise forms.ValidationError('Такой email уже занят.')
        return email
