# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .cart import Cart


def cart(request):
    cart = Cart(request)
    basket = Cart(request).cart
    basket_data = cart.get_basket()

    return {
        'basket': basket,
        'basket_data': basket_data,
    }
