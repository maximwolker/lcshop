# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models

from catalog.models import Product


class Cart(models.Model):
    STATUS_CHOICES = (
        (1, 'Новый'),
        (2, 'Принят'),
        (3, 'Обработан'),
        (4, 'Оплачен'),
        (5, 'Отправлен'),
        (6, 'Завершен'),
        (7, 'Отменен'),
    )
    DELIVERY_CHOICES = (
        ('0', 'Самовывоз'),
        ('1', 'Курьерская доставка'),
        ('2', 'Отправление первого класса'),
        ('3', 'Почта'),
    )
    PAYMENT_CHOICES = (
        ('0', 'Пластиковые карты'),
        ('1', 'Наличные'),
        ('2', 'Долг по дружбе'),
    )
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Профиль', blank=True, null=True)

    creation_date = models.DateTimeField('Дата создания', auto_now_add=True)
    checked_out = models.BooleanField('Корзина оформлена', default=False)
    checkout_date = models.DateTimeField('Дата оформления', null=True, blank=True)

    first_name = models.CharField('Имя', max_length=255, null=True, blank=True)
    last_name = models.CharField('Фамилия', max_length=255, null=True, blank=True)
    email = models.EmailField('Email', max_length=255, null=True, blank=True)
    phone = models.CharField('Телефон', max_length=30, null=True, blank=True)
    phone2 = models.CharField('Телефон №2', max_length=30, null=True, blank=True)

    address1 = models.CharField('Адрес №1', max_length=255, null=True, blank=True)
    address2 = models.CharField('Адрес №2', max_length=255, null=True, blank=True)
    city = models.CharField('Город', max_length=255, null=True, blank=True)
    region = models.CharField('Регион', max_length=255, null=True, blank=True)
    postal_code = models.CharField('Почтовый код', max_length=255, null=True, blank=True)
    country = models.CharField('Страна', max_length=255, null=True, blank=True)

    tracking_number = models.CharField('Номер отслеживания', max_length=255, null=True, blank=True)
    delivery_type = models.CharField('Тип доставки', max_length=15, choices=DELIVERY_CHOICES, null=True, blank=True)
    payment_type = models.CharField('Тип оплаты', max_length=15, choices=PAYMENT_CHOICES, null=True, blank=True)

    status = models.PositiveSmallIntegerField('Статус', choices=STATUS_CHOICES, default=1)
    summary = models.DecimalField(max_digits=18, decimal_places=2, verbose_name='Цена, тнг')

    class Meta:
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы'
        ordering = ('-creation_date',)

    def __unicode__(self):
        return unicode(self.creation_date)

    def save(self, *args, **kwargs):
        if not self.checked_out:
            self.summary = self.get_summary()
        return super(Cart, self).save(*args, **kwargs)

    def get_order_url(self):
        return reverse('profile:order', kwargs={'pk': self.id})

    def count(self):
        result = 0
        for item in self.cartitem_set.all():
            result += item.quantity
        return result
    count.allow_tags = True
    count.short_description = 'Количество товара'

    def get_summary(self):
        result = 0
        for item in self.cartitem_set.all():
            result += item.total_price
        return result

    def show_customer(self):
        return self.customer or ''
    show_customer.allow_tags = True
    show_customer.short_description = 'Клиент'

    def get_status(self):
        return self.STATUS_CHOICES[self.status-1][1]

    def get_delivery_type(self):
        return self.DELIVERY_CHOICES[int(self.delivery_type)][1]

    def get_payment_type(self):
        return self.PAYMENT_CHOICES[int(self.payment_type)][1]


class CartItem(models.Model):
    cart = models.ForeignKey(Cart)
    quantity = models.PositiveIntegerField('Количество')
    product = models.ForeignKey(Product, verbose_name='Товар')
    price = models.DecimalField('Цена, тнг', max_digits=18, decimal_places=2)
    color_title = models.CharField('Цвет', max_length=255, blank=True)
    color_hex = models.CharField('Цвет: HEX-представление', max_length=7, blank=True)
    color_id = models.IntegerField('Цвет: id')

    class Meta:
        verbose_name = 'товар'
        verbose_name_plural = 'товары'
        ordering = ('cart', '-id',)

    def __unicode__(self):
        return '%d units of %s' % (self.quantity, self.product.title)

    @property
    def total_price(self):
        return self.quantity * self.price

    def save(self, *args, **kwargs):
        self.price = self.product.get_price()
        return super(CartItem, self).save(*args, **kwargs)
