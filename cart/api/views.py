# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404, JsonResponse
from django.views.generic import View

from catalog.models import Product, ProductColor
from cart.cart import Cart


class CartAjaxView(View):
    """
    Обрабатываем аякс-запросы на изменение корзины: 'set', 'add', 'delete' или 'clear'
    """
    action = ''

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            errors = []
            cart = Cart(request)

            try:
                if self.action == 'clear':
                    cart.clear()

                else:
                    product = Product.objects.filter(id=int(request.POST.get('product_id'))).first()
                    quantity = int(request.POST.get('quantity', 1))
                    color_id = int(request.POST.get('color', 0))
                    color = ProductColor.objects.filter(id=color_id).first() if color_id else product.colors.first()

                    # if product and color:
                    if product:
                        if self.action == 'set':
                            if not cart.set(product, color, quantity):
                                errors = ['Недостаточно товара на складе']

                        elif self.action == 'add':
                            if not cart.add(product, color, quantity):
                                errors = ['Недостаточно товара на складе']

                        elif self.action == 'remove':
                            cart.remove(product, color)

                    else:
                        raise Http404

            except ValueError:
                # print 'value_error'
                raise Http404

            if len(errors):
                data = {'basket': [], 'errors': errors}
            else:
                data = {'basket': cart.get_basket(), 'errors': []}
            return JsonResponse(data)

        else:
            # print 'not_ajax'
            raise Http404
