# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import CartAjaxView


urlpatterns = [
    url(r'^add/$', CartAjaxView.as_view(action='add'), name='add'),
    url(r'^change/$', CartAjaxView.as_view(action='set'), name='change'),
    url(r'^delete/$', CartAjaxView.as_view(action='remove'), name='delete'),
    url(r'^clear/$', CartAjaxView.as_view(action='clear'), name='clear'),
]
