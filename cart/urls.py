# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.views.generic import TemplateView

# from .views import CartView, CheckoutView
from .views import CheckoutView


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="cart/cart.html"), name='home'),
    url(r'^checkout/$', CheckoutView.as_view(), name='checkout'),
    url(r'^checkout/registration/$', CheckoutView.as_view(form_type='with_registration'), name='checkout-registration'),
]
