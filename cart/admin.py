# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Cart, CartItem


class CartItemInline(admin.TabularInline):
    model = CartItem
    extra = 0


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    date_hierarchy = 'creation_date'
    list_display = ('creation_date', 'checkout_date', 'show_customer', 'count', 'summary',)
    list_filter = ('status',)
    fieldsets = (
        (None, {
            'fields': (('creation_date', 'customer',), ('checked_out', 'checkout_date',), 'status',),
        }),
        ('Контакты', {
            'fields': (
                ('first_name', 'last_name',), 'email', ('phone', 'phone2',),
                'address1', 'address2', 'city', 'region', 'postal_code', 'country',
            ),
        }),
        (None, {
            'fields': ('tracking_number', 'delivery_type', 'payment_type',),
        }),
    )
    readonly_fields = ('customer', 'creation_date', 'checked_out', 'checkout_date',)
    inlines = [CartItemInline, ]

    def get_queryset(self, request):
        queryset = super(CartAdmin, self).get_queryset(request)
        return queryset.filter(checked_out=True)
