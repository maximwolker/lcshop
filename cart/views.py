# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.views.generic import UpdateView

from registration.models import RegistrationProfile

from lk.email import send_admin_order_email, send_customer_order_email
from lk.models import Profile
from .cart import Cart
from .forms import CheckoutFormWithoutRegistration, CheckoutFormWithRegistration


class CheckoutView(UpdateView):
    form_type = ''
    template_name = 'cart/checkout.html'
    form_classes = {
        'with_registration': CheckoutFormWithRegistration,
        'default': CheckoutFormWithoutRegistration
    }

    def get(self, request, *args, **kwargs):
        self.form_type = self.form_type or request.GET.get('t')
        if (not self.form_type in ('with_registration', 'without_registration') and self.request.user.is_anonymous()
           or not self.get_object().count()):
            return HttpResponseRedirect(reverse('cart:home'))
        return super(CheckoutView, self).get(request, *args, **kwargs)

    def get_form_class(self):
        return (self.form_classes['with_registration'] if self.form_type == 'with_registration'
                else self.form_classes['default'])
        # return CheckoutFormWithoutRegistration

    def get_success_url(self):
        return self.request.path

    def get_object(self, queryset=None):
        return Cart(self.request).cart

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        cart = form.instance

        if cart.count():
            if self.form_type == 'with_registration':
                profile = Profile.objects.create(
                    email=form.cleaned_data.get('email'),
                    is_active=False,
                    profile_type='human',
                )
                attrs = ('first_name', 'last_name', 'phone', 'phone2', 'address1', 'address2', 'city', 'postal_code')
                         # 'region', 'country',)
                for attr in attrs:
                    setattr(profile, attr, self.request.POST.get(attr))
                profile.set_password(form.cleaned_data.get('password1'))
                profile.save()
                self._create_user(self.request, profile)
                success_message = '<p>Данные о заказе отправлены на ваш email, ' \
                                  'мы будем сообщать вам об изменениях статуса заказа.</p>' \
                                  '<p>Также вам отправлена ссылка для активации профиля.</p>'
                cart.customer = profile

            elif self.request.user.is_anonymous():
                success_message = '<p>Данные о заказе отправлены на ваш email, ' \
                                  'мы будем сообщать вам об изменениях статуса заказа.</p>'
                cart.customer = None

            else:
                success_message = '<p>Данные о заказе отправлены на ваш email, ' \
                                  'мы будем сообщать вам об изменениях статуса заказа.</p>' \
                                  '<p>Вы можете просматривать историю заказов в ' \
                                  '<a href="{0}">личном кабинете</a>.</p>'.format(reverse('profile:orders'))
                # import ipdb; ipdb.set_trace()
                cart.customer = self.request.user

            cart.checked_out = True
            cart.checkout_date = datetime.datetime.now()
            cart.summary = cart.get_summary()
            cart.save()
            super(CheckoutView, self).form_valid(form)

            send_customer_order_email(email=form.cleaned_data.get('email'), request=self.request, order=cart)
            send_admin_order_email(request=self.request, order=cart)

            if self.request.session.get('CART_ID'):
                del self.request.session['CART_ID']

            return JsonResponse({'result': 'ok', 'success_message': success_message})

        else:
            return JsonResponse({'error_message': 'Корзина пуста', 'result': 'error'})

    def _create_user(self, request, new_user_instance):
        """
        registration.backends.default.views.RegistrationView
        """
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        new_user = RegistrationProfile.objects.create_inactive_user(
            new_user=new_user_instance,
            site=site,
            send_email=True,
            request=request,
        )
        return new_user
