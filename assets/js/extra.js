$(function(){
    $('#comment-reply').on('show.bs.modal', function (event) {
        var $button = $(event.relatedTarget),
            $popup = $(this);

        $popup.find('input[name="root"]').val($button.attr('data-comment-id'));
        $popup.find('.root_comment_author').text($button.attr('data-author'));
    })

    $('body').on('submit', '.ajax_form', function(e) {
        e.preventDefault();
        var $form = $(this),
            data = $form.serialize(),
            url = $form.attr('action');

        $form.find($('.input-group')).each(function(){$(this).removeClass('has-error')});
        $form.find($('.input-error')).each(function(){$(this).remove()});
        $form.find('.alert-success').hide();
        $form.find('.alert-danger').hide();
        $form.find('.alert-info').remove();

        $form.addClass('no-pe')
        $form.fadeTo('fast', 0.5)

        if ($form.attr('enctype') == 'multipart/form-data') {
            $.ajax({
                url: url,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(res, textStatus, jqXHR) {
                    var err = res.errors,
                        result = res.result,
                        close_button = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';

                    if (result == 'ok') {
                        if (!$form.hasClass('do_not_reset')) { $form[0].reset(); }
                        if ($form.hasClass('reload_after')) {
                            location.reload();
                        } else if ($form.hasClass('redirect_after')) {
                            location.href = $form.attr('data-redirect-to') || window.location.pathname;
                        } else if ($form.hasClass('comment_form')) {
                            location.href = window.location.pathname + '#comments';
                            location.reload();
                        } else if ($form.hasClass('popup_at_success')) {
                            var $popup = $('#order-success');
                            $popup.find('.modal-body').html(res['success_message']);
                            $popup.modal();
                            setTimeout(function() {location.href = '/'}, 5000);
                        } else {
                            $form.find('.alert-success').show();
                        }
                    } else {
                        err.forEach(function(item, m, list) {
                            if (item['name'] == '__all__') {
                                var $alert_danger = $form.find('.alert-danger');
                                if ($alert_danger.length) {
                                    $alert_danger.html(item.error);
                                    $alert_danger.show();
                                } else {
                                    $form.prepend('<div class="alert alert-danger">' + item.error + '</div>');
                                }
                            } else {
                                var $input_group = $form.find('[name="' + item["name"] + '"]').parent();
                                $input_group.addClass('has-error');
                                if ($form.hasClass('show_field_errors')) {
                                    $input_group.before('<div class="help-block input-error">' + item.error + '</div>');
                                }
                            }
                        });
                        if ((result == 'error') && ($form.hasClass('alert_at_errors'))) {
                            var error_message = res['error_message'];
                            if (error_message) {
                                alert(error_message);
                            } else {
                                alert('Пожалуйста, исправьте ошибки выше.');
                            }
                        }
                        if (result == 'info') {
                            $form.prepend('<div class="alert alert-info alert-dismissable">' + close_button + res["info_message"] + '</div>')
                        }
                    }
                    $form.fadeTo('fast', 1);
                    $form.removeClass('no-pe');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Произошла ошибка');
                    $form.fadeTo('fast', 1);
                    $form.removeClass('no-pe');
                }
            });
        } else {
            $.post(url, data, function(res) {
                var err = res.errors,
                    result = res.result,
                    close_button = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';

                if (result == 'ok') {
                    if (!$form.hasClass('do_not_reset')) { $form[0].reset(); }
                    if ($form.hasClass('reload_after')) {
                        location.reload();
                    } else if ($form.hasClass('redirect_after')) {
                        location.href = $form.attr('data-redirect-to') || window.location.pathname;
                    } else if ($form.hasClass('comment_form')) {
                        location.href = window.location.pathname + '#comments';
                        location.reload();
                    } else if ($form.hasClass('popup_at_success')) {
                        var $popup = $('#order-success');
                        $popup.find('.modal-body').html(res['success_message']);
                        $popup.modal();
                        setTimeout(function() {location.href = '/'}, 5000);
                    } else {
                        $form.find('.alert-success').show();
                    }
                } else {
                    err.forEach(function(item, m, list) {
                        if (item['name'] == '__all__') {
                            var $alert_danger = $form.find('.alert-danger');
                            if ($alert_danger.length) {
                                $alert_danger.html(item.error);
                                $alert_danger.show();
                            } else {
                                $form.prepend('<div class="alert alert-danger">' + item.error + '</div>');
                            }
                        } else {
                            var $input_group = $form.find('[name="' + item["name"] + '"]').parent();
                            $input_group.addClass('has-error');
                            if ($form.hasClass('show_field_errors')) {
                                $input_group.before('<div class="help-block input-error">' + item.error + '</div>');
                            }
                        }
                    });
                    if ((result == 'error') && ($form.hasClass('alert_at_errors'))) {
                        var error_message = res['error_message'];
                        if (error_message) {
                            alert(error_message);
                        } else {
                            alert('Пожалуйста, исправьте ошибки выше.');
                        }
                    }
                    if (result == 'info') {
                        $form.prepend('<div class="alert alert-info alert-dismissable">' + close_button + res["info_message"] + '</div>')
                    }
                }

                $form.fadeTo('fast', 1)
                $form.removeClass('no-pe')
            });
        }
    });

    var $body = $('body');

    $body.on('click', 'a._disabled', function(e) {
        e.preventDefault();
    });

    $body.on('click', '.add_to', function(e) {
        e.preventDefault();
        var $link = $(this),
            url = $link.attr('data-add-url'),
            // data = {product_id: $link.attr('data-id'), csrfmiddlewaretoken: $.cookie('csrftoken')},
            data = {product_id: $link.attr('data-id'), csrfmiddlewaretoken: csrftoken};

        if (!$link.hasClass('disabled')) {
            $.post(url, data, function(res) {
                var result = res.result;
                if (result == 'ok') {
                    $link.addClass('already_added');
                    $link.addClass('no-pe');
                    if ($link.hasClass('add_to_compare')) {
                        $('.compared_count').text(res['count']);
                    }
                } else if (result == 'error') {
                    alert(res['error']);
                }
            });
        } else {
            alert($link.attr('data-disabled-message'));
        }
    });

    $body.on('click', '.remove_from_favorites', function(e) {
        e.preventDefault();
        var $link = $(this),
            url = $link.attr('data-remove-url'),
            // data = {product_id: $link.attr('data-id'), csrfmiddlewaretoken: $.cookie('csrftoken')},
            data = {product_id: $link.attr('data-id'), csrfmiddlewaretoken: csrftoken},
            $parent = $link.parent().parent().parent();

        $parent.slideUp('normal', function() {
            $.post(url, data);
            $parent.remove();
            if ($('.favorite_item').length == 0) {
                $('#favorite_items').hide();
                $('p.empty').fadeIn();
            }
        });
    });

    $body.on('click', '.remove_from_compare', function(e) {
        e.preventDefault();
        var $link = $(this),
            url = $link.attr('data-remove-url'),
            product_id = $link.attr('data-id')
            // data = {product_id: $link.attr('data-id'), csrfmiddlewaretoken: $.cookie('csrftoken')},
            data = {product_id: $link.attr('data-id'), csrfmiddlewaretoken: csrftoken};

        $.post(url, data, function(res) {
            var result = res.result;
            $('.product-' + product_id).each(function(){ $(this).fadeOut(); })
            if (result == 'ok') {
                $('.compared_count').text(res['count']);
            }
        });
    });

    $body.on('click', '.do_nothing', function(e) {
        e.preventDefault();
    });

    $body.on('click', '.like_button', function(e) {
        e.preventDefault();
        var $button = $(this),
            $heart = $button.find('.heart'),
            $count = $button.find('.count'),

            like_url = $button.attr('data-like-url'),
            dislike_url = $button.attr('data-dislike-url'),
            url = like_url,
            data = {post_id: $button.attr('data-id'), csrfmiddlewaretoken: csrftoken};

        if ($heart.hasClass('already_added')) { url = dislike_url }

        $.post(url, data, function(res) {
            var result = res.result;

            if (result == 'ok') {
                if ($heart.hasClass('already_added')) { $heart.removeClass('already_added'); }
                else { $heart.addClass('already_added'); }

                $count.text(res['count']);

            } else if (result == 'error') {
                alert(res['error']);
            }
        });
    });

    $('.support_search_form').on('submit', function(e) {
        e.preventDefault();
        var $form = $(this),
            $input = $form.find('input'),
            data = $form.serialize(),
            url = $form.attr('action'),
            // $res_container = $('.result_container'),
            $res_tab_pane = $('.tab-pane#search'),
            $results = $res_tab_pane.find('.results');

        if (!$input.val()) {
            $res_container.hide();
            $($('#support_tabs a')[0]).tab('show');
            $results.html('');
            location.href = window.location.pathname + '#s';
        } else {
            $('#support_tabs li').each(function(){$(this).removeClass('active')});
            $('#support_tab_content .tab-pane').each(function(){$(this).removeClass('active')});
            $res_tab_pane.addClass('active');
            $res_tab_pane.addClass('no-pe')
            $res_tab_pane.fadeTo('fast', 0.5)
            location.href = window.location.pathname + '#s';

            $.get(url, data, function(res) {
                $results.html(res);
                $res_tab_pane.fadeTo('fast', 1)
                $res_tab_pane.removeClass('no-pe')
            });
        }
    });
});
