$(function(){
    var $filter_form = $('.filter_form');

    function getCleanData($form) {
        var filter = jQuery.grep($form.serializeArray(), function(element) { return element['value'] != ''; })
        return jQuery.param(filter)
    }

    // Фильтруем
    function sendForm(category_changed) {
        var $container = $('.product_list'),
            data = getCleanData($filter_form),
            url = $filter_form.attr('action'),
            title = $(document).attr('title'),
            uri = url + '?' + data;

        history.pushState(data, title, uri);
        history.pathname = uri;

        $container.fadeTo('fast', 0.5)
        $container.addClass('no-pe')

        $.get(url, data, function(res) {
            $container.html(res);
            $container.removeClass('no-pe')
            $container.fadeTo('fast', 1)
            if (category_changed) {
                $('aside .aside_after_categories').remove();
                var $after_categories = $('.product_list').find('.aside_after_categories');
                if ($after_categories.length) {
                    $('aside .aside_categories').after($container.find('.aside_after_categories'));
                }
                if ($('aside .aside_after_categories').length > 1) {
                    $('aside .aside_after_categories').each(function(i, item) { if (i>0) {item.remove()}})
                }

                $price_slider = $('.price-slider')
                if ($price_slider.length) {
                    $price_slider.slider({ tooltip: 'hide' })
                    .on('slide', function(s){ $('.price_range').text(s.value[0] + ' тнг. - ' + s.value[1] + ' тнг.'); })
                    .on('slideStop', function(s){
                        var $hidden_input = $('input[name="price"]');
                        if (s.value[0] == $price_slider.attr('data-slider-min') &&
                            s.value[1] == $price_slider.attr('data-slider-max')) {
                            $hidden_input.val('');
                        }
                        else { $hidden_input.val(s.value[0]+'-'+s.value[1]); }
                        sendForm();
                    })
                }

                $('.filter-color-box').each(function() {
                    var $this = $(this),
                        bgColor = $this.data('bgcolor');

                        $this.css('background-color', bgColor);
                });

            }
            $('.item-hover').on('mouseover', self.itemAnimationIn).on('mouseleave', self.itemAnimationOut);
        });
    }

    $body = $('body');

    // Перехватываем нажатие по ссылке
    // (например, на всяких страницах-фильтрах: кнопки пагинации и сортировки)
    // и обрабатываем это дело на аяксе
    $body.on('click', '.ajax_filter', function(e){
        e.preventDefault();
        var page_href = $(this).attr('href'),
            $container = $('.' + $(this).attr('data-ajax-containter')),
            title = $(document).attr('title');

        $container.fadeTo('fast', 0.5)
        $container.addClass('no-pe');

        history.pushState({}, title, page_href);
        history.pathname = page_href;

        $.get(page_href, function(res) {
            $container.html(res);
            $container.removeClass('no-pe');
            $container.fadeTo('fast', 1)
        });
    });

    // Выбираем цвет
    $body.on('click', '.color_button', function(e){
        e.preventDefault();
        var $button = $(this),
            color = $button.attr('data-color');

        if ($button.hasClass('on')) {
            $button.siblings('input[type="hidden"]').remove();
            $button.removeClass('on');
        } else {
            $button.after('<input type="hidden" name="color" value="' + color + '">')
            $button.addClass('on');
        }
        sendForm();
    });

    // Выбираем атрибуты
    $body.on('change', '.attr_checkbox', function(e){
        e.preventDefault();
        var $checkbox = $(this)
            $checker_span = $checkbox.parent(),
            $children_ul = $checker_span.parent().parent().find('.children_ul');
            $children = $children_ul.find('.attr_checkbox');


        if ($checkbox.hasClass('_category')) {
            // $children.each(function(){
            //     $(this).change();
            // })
            if ($checkbox.is(':checked')) {
                $checker_span.addClass('checked');
                $children.each(function(){
                    $(this).attr('checked', true);
                    $(this).parent().addClass('checked');
                })
            } else {
                $checker_span.removeClass('checked');
                $children.each(function(){
                    $(this).attr('checked', false);
                    $(this).parent().removeClass('checked');
                })
            }
        } else {
            if ($checkbox.is(':checked')) { $checker_span.addClass('checked'); }
            else { $checker_span.removeClass('checked'); }
        }

        if (!$checkbox.hasClass('_category')) {
            sendForm();
        } else {
            var $after_categories = $('aside .aside_after_categories');
            $after_categories.fadeTo('fast', 0.5);
            $after_categories.addClass('no-pe');
            sendForm(true);
        }
    })

    // Выбираем тип сортировки
    $body.on('click', '.order_by_choices', function(e){
        e.preventDefault();
        var $choice = $(this),
            slug = $choice.attr('data-slug'),
            title = $choice.text();

        $('.order_by_title').text(title);
        $filter_form.find('input[name="order_by"]').val(slug)
        sendForm();
    });

    // Слайдер цены
    // $body.on('click', '.ajax_filter', function(e){
    $price_slider = $('.price-slider')
    if ($price_slider.length) {
        $price_slider.slider({ tooltip: 'hide' })
        .on('slide', function(s){
            $('.price_range').text(s.value[0] + ' тнг. - ' + s.value[1] + ' тнг.');
        })
        .on('slideStop', function(s){
            var $hidden_input = $('input[name="price"]');

            if (s.value[0] == $price_slider.attr('data-slider-min') &&
                s.value[1] == $price_slider.attr('data-slider-max')) {
                $hidden_input.val('');
            }
            else { $hidden_input.val(s.value[0]+'-'+s.value[1]); }
            sendForm();
        })
    }

    $('.search_input').on('input', function(e){
        e.preventDefault();
        var $input = $(this),
            data = 'q=' + $input.val();
            url = $filter_form.attr('action'),
            title = $(document).attr('title'),
            uri = url + '?' + data
            $container = $('.product_list'),
            $categories = $('aside .aside_categories'),
            $after_categories = $('aside .aside_after_categories');

        history.pushState(data, title, uri);
        history.pathname = uri;

        $container.fadeTo('fast', 0.5);
        $container.addClass('no-pe');
        $categories.fadeTo('fast', 0.5);
        $categories.addClass('no-pe');

        $.get(url, data, function(res) {
            $container.html(res);
            $categories.html($container.find('.aside_categories').html());
            $categories.removeClass('no-pe');
            $categories.fadeTo('fast', 1);
            $container.removeClass('no-pe');
            $container.fadeTo('fast', 1);
        });
    })
});
