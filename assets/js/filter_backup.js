$(function(){
    var $filter_form = $('.filter_form');

    function getCleanData($form) {
        var filter = jQuery.grep($form.serializeArray(), function(element) { return element['value'] != ''; })
        return jQuery.param(filter)
    }

    // Фильтруем
    function sendForm() {
        var $container = $('.product_list'),
            data = getCleanData($filter_form);
            url = $filter_form.attr('action'),
            title = $(document).attr('title'),
            uri = url + '?' + data;

        history.pushState(data, title, uri);
        history.pathname = uri;

        $container.fadeTo('fast', 0.5)
        $container.addClass('no-pe')

        $.get(url, data, function(res) {
            $container.html(res);
            $container.removeClass('no-pe')
            $container.fadeTo('fast', 1)
        });
    }

    // Перехватываем нажатие по ссылке
    // (например, на всяких страницах-фильтрах: кнопки пагинации и сортировки)
    // и обрабатываем это дело на аяксе
    $('body').on('click', '.ajax_filter', function(e){
        e.preventDefault();
        var page_href = $(this).attr('href'),
            $container = $('.' + $(this).attr('data-ajax-containter')),
            title = $(document).attr('title');

        $container.fadeTo('fast', 0.5)
        $container.addClass('no-pe');

        history.pushState({}, title, page_href);
        history.pathname = page_href;

        $.get(page_href, function(res) {
            $container.html(res);
            $container.removeClass('no-pe');
            $container.fadeTo('fast', 1)
        });
    });

    // Выбираем цвет
    $('.color_button').click(function(e){
        e.preventDefault();
        var $button = $(this),
            color = $button.attr('data-color');

        if ($button.hasClass('on')) {
            $button.siblings('input[type="hidden"]').remove();
            $button.removeClass('on');
        } else {
            $button.after('<input type="hidden" name="color" value="' + color + '">')
            $button.addClass('on');
        }
        sendForm();
    });


    // Выбираем атрибуты
    $('.attr_checkbox').on('change', function(e){
        e.preventDefault();
        var $checkbox = $(this)
            $checker_span = $checkbox.parent();

        if ($checkbox.is(':checked')) { $checker_span.addClass('checked'); }
        else { $checker_span.removeClass('checked'); }
        sendForm();
    })

    // Выбираем тип сортировки
    $filter_form.on('click', '.order_by_choices', function(e){
        e.preventDefault();
        var $choice = $(this),
            slug = $choice.attr('data-slug'),
            title = $choice.text();

        $('.order_by_title').text(title);
        $filter_form.find('input[name="order_by"]').val(slug)
        sendForm();
    });

    // Слайдер цены
    $price_slider = $('.price-slider')
    if ($price_slider.length) {
        $price_slider.slider({ tooltip: 'hide' })
        .on('slide', function(s){
            $('.price_range').text(s.value[0] + ' тнг. - ' + s.value[1] + ' тнг.');
        })
        .on('slideStop', function(s){
            var $hidden_input = $('input[name="price"]');

            if (s.value[0] == $price_slider.attr('data-slider-min') &&
                s.value[1] == $price_slider.attr('data-slider-max')) {
                $hidden_input.val('');
            }
            else { $hidden_input.val(s.value[0]+'-'+s.value[1]); }
            sendForm();
        })
    }

    $('.search_input').on('input', function(e){
        e.preventDefault();
        var $input = $(this),
            data = 'q=' + $input.val();
            url = $filter_form.attr('action'),
            title = $(document).attr('title'),
            uri = url + '?' + data
            $container = $('.product_list'),
            $categories = $('aside .aside_categories'),
            $after_categories = $('aside .aside_after_categories');

        history.pushState(data, title, uri);
        history.pathname = uri;

        $container.fadeTo('fast', 0.5);
        $container.addClass('no-pe');
        $categories.fadeTo('fast', 0.5);
        $categories.addClass('no-pe');

        $.get(url, data, function(res) {
            $container.html(res);
            $categories.html($container.find('.aside_categories').html());
            $categories.removeClass('no-pe');
            $categories.fadeTo('fast', 1);
            $container.removeClass('no-pe');
            $container.fadeTo('fast', 1);
        });
    })
});
