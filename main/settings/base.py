# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


ADMINS = (
    ('Valentin Glinskiy', 'v.valych@gmail.com'),
)

MANAGERS = ADMINS

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'assets'),
)

FIXTURE_DIRS = (os.path.join(BASE_DIR, 'fixtures'), )

SECRET_KEY = 'wju4f4nl&hmra8%nj!$@1iq6-@@623-c92je$1!n1$n&@x60%p&'

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

SITE_ID = 1


# Application definition

INSTALLED_APPS = (
    'core',
    'grappelli',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',

    'admin_honeypot',
    'ckeditor',
    'django_hstore',
    'mptt',
    'pure_pagination',
    'smart_selects',
    'sorl.thumbnail',

    'treenav',
    'site_settings',
    'geo',
    'lk',
    'registration',
    'catalog',
    'blog',
    'cart',
    'support',
    'content',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',

                'django.core.context_processors.request',

                # custom:
                'cart.context_processors.cart',
                'content.context_processors.content',
                'site_settings.context_processors.settings',
                'treenav.context_processors.treenav_active',
            ],
        },
    },
]


AUTH_USER_MODEL = 'lk.Profile'

ROOT_URLCONF = 'main.urls'
WSGI_APPLICATION = 'main.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True
USE_L10N = True
USE_TZ = False

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = ''
SERVER_EMAIL = ''

GRAPPELLI_ADMIN_TITLE = 'Alazone'

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_CONFIGS = {
    'default': {
        'forcePasteAsPlainText': True,
        'language': 'ru',
        'width': '100%',
        'toolbar': [
            {'name': 'document', 'items': ['Source']},
            {'name': 'clipboard', 'groups': ['clipboard', 'undo'],
             'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            '/',
            {'name': 'paragraph', 'groups': ['list', 'blocks'],
             'items': ['NumberedList', 'BulletedList', '-', 'Blockquote']},
            {'name': 'basicstyles', 'items': ['Bold', 'Italic']},
            {'name': 'styles', 'items': ['Format']},
            {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
            {'name': 'insert', 'items': ['Image', 'HorizontalRule']},
        ],
        'format_tags': 'p;h1;h2;h3',
        'removeDialogTabs': 'image:advanced;link:advanced',
        'image_previewText': '&nbsp',
    },
    'simple': {
        'forcePasteAsPlainText': True,
        'language': 'ru',
        'width': '70%',
        'height': '80%',
        'toolbar': [
            {'name': 'document', 'items': ['Source']},
            {'name': 'paragraph', 'groups': ['list', 'blocks'],
             'items': ['NumberedList', 'BulletedList', '-', 'Blockquote']},
            {'name': 'basicstyles', 'items': ['Bold', 'Italic']},
            {'name': 'links', 'items': ['Link']},
        ],
        'removeDialogTabs': 'image:advanced;link:advanced',
        'image_previewText': '&nbsp',
    },
}

# django-registration-redux
REGISTRATION_FORM = 'lk.forms.RegistrationForm'
ACCOUNT_ACTIVATION_DAYS = 7
REGISTRATION_AUTO_LOGIN = True
REGISTRATION_EMAIL_HTML = False
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/auth/login/'
SITE_ID = 1
