# -*- coding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

from lk.auth_views import SellerRegistrationView, AjaxLoginView
from catalog.views import ProductRequestView
from content.views import IndexView, PageView, AboutUsPageView, FeedbackFormView, SubscribeFormView, SearchView
from support.views import SupportView, SupportSearchView, QuestionFormView


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^404/$', TemplateView.as_view(template_name='404.html'), name='404'),
    url(r'^500/$', TemplateView.as_view(template_name='500.html'), name='500'),
    url(r'^about/$', AboutUsPageView.as_view(), name='about'),

    url(r'^support/$', SupportView.as_view(), name='support'),
    url(r'^support/search/$', SupportSearchView.as_view(), name='support-search'),

    # url(r'^auth/',  include('lk.auth.urls', namespace='auth')),
    url(r'^auth/', include('registration.backends.default.urls')),
    url(r'^auth/register/seller/$', SellerRegistrationView.as_view(), name='seller_registration_register'),
    url(r'^auth/login/ajax/$', AjaxLoginView.as_view(), name='auth-ajax-login'),

    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^alazone-adm/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^treenav/', include('treenav.urls')),

    url(r'^profile/',  include('lk.urls', namespace='profile')),
    url(r'^cart/',  include('cart.urls', namespace='cart')),
    url(r'^basket/',  include('cart.api.urls', namespace='basket')),
    url(r'^shop/',  include('catalog.urls', namespace='shop')),
    url(r'^blog/',  include('blog.urls', namespace='blog')),

    # url(r'^forms/', include('feedback.urls', namespace='forms')),
    url(r'^feedback_form/$', FeedbackFormView.as_view(), name='feedback-form'),
    url(r'^subscribe_form/$', SubscribeFormView.as_view(), name='subscribe-form'),
    url(r'^question_form/$', QuestionFormView.as_view(), name='question-form'),
    url(r'^request_form/$', ProductRequestView.as_view(), name='request-form'),

    url(r'^search/$', SearchView.as_view(), name='search'),
    url(r'^(?P<slug>[^/]+)/$', PageView.as_view(), name='page'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
