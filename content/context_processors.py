# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from catalog.models import Category


def content(request):
    root_categories = Category.objects.filter(level=0, show=True)

    compared_products = request.session.get('compared_products', {})
    compared_count = sum([len(arr) for arr in compared_products.values()])
    compared_ids = []
    for arr in compared_products.values():
        compared_ids.extend(arr)

    liked_posts = request.session.get('liked_posts', [])

    return {
        'root_categories': root_categories,
        'compared_products': compared_products,
        'compared_count': compared_count,
        'compared_ids': compared_ids,
        'liked_posts': liked_posts,
    }
