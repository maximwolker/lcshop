# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template

from blog.models import Post
from catalog.models import Product
from content.models import HomepageTestimonial, HomepageBanner, HomepageManufacturer


register = template.Library()


@register.inclusion_tag('include/homepage_testimonials.html')
def homepage_testimonials():
    testimonials = HomepageTestimonial.objects.filter(show=True)
    return {'testimonials': testimonials}


@register.inclusion_tag('include/homepage_banners.html')
def homepage_banners():
    banners = HomepageBanner.active_objects.all().order_by('?')
    return {'banners': banners}


@register.inclusion_tag('include/homepage_manufacturers.html')
def homepage_manufacturers():
    manufacturers = HomepageManufacturer.active_objects.all().order_by('?')
    return {'manufacturers': manufacturers}


@register.inclusion_tag('include/homepage_latest_news.html')
def latest_news():
    posts = Post.objects.posted().filter(at_sidebar=True, category__slug='news')[:5]
    return {'latest_news': posts}


@register.inclusion_tag('include/sponsored_products.html')
def sponsored_products():
    products = Product.objects.filter(show=True, at_sidebar=True).order_by('?')[:5]
    return {'sponsored_products': products}
