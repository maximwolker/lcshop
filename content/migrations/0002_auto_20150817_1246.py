# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banner',
            name='title',
            field=models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
        ),
        migrations.AlterField(
            model_name='contactsmessage',
            name='subject',
            field=models.CharField(max_length=255, verbose_name='\u0422\u0435\u043c\u0430'),
        ),
        migrations.AlterField(
            model_name='manufacturer',
            name='title',
            field=models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True),
        ),
    ]
