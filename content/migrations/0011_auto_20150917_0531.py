# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0010_auto_20150910_1428'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomepageSlide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slide_type', models.PositiveSmallIntegerField(verbose_name='\u0422\u0438\u043f \u0441\u043b\u0430\u0439\u0434\u0430', choices=[(1, '\u0422\u0438\u043f #1 (\u0434\u043e 1 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f)'), (2, '\u0422\u0438\u043f #2 (\u0434\u043e 2 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0439)'), (3, '\u0422\u0438\u043f #3 (\u0434\u043e 4 \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0439)')])),
                ('background', models.ImageField(help_text='\u0411\u0443\u0434\u0435\u0442 \u043e\u0431\u0440\u0435\u0437\u0430\u0442\u044c\u0441\u044f \u0434\u043e 1920x600 px', upload_to='homepage/slider/backgrounds/', verbose_name='\u0424\u043e\u043d \u0441\u043b\u0430\u0439\u0434\u0430')),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('button_link', models.URLField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u043d\u0435 \u043f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043a\u043d\u043e\u043f\u043a\u0443', max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u043a\u043d\u043e\u043f\u043a\u0435', blank=True)),
                ('button_title', models.CharField(default='\u0423\u0437\u043d\u0430\u0442\u044c \u0431\u043e\u043b\u044c\u0448\u0435', help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u043d\u0435 \u043f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043a\u043d\u043e\u043f\u043a\u0443', max_length=127, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043d\u0430 \u043a\u043d\u043e\u043f\u043a\u0435', blank=True)),
                ('button_target_blank', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0441\u0441\u044b\u043b\u043a\u0443 \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435')),
                ('price', models.CharField(max_length=15, verbose_name='\u0426\u0435\u043d\u0430 \u0432 \u0437\u0435\u043b\u0435\u043d\u043e\u043c \u043a\u0440\u0443\u0436\u043a\u0435', blank=True)),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': '\u0441\u043b\u0430\u0439\u0434',
                'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u0441\u043b\u0430\u0439\u0434\u044b',
            },
        ),
        migrations.CreateModel(
            name='HomepageSlideImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('image', models.ImageField(upload_to='homepage/slider/images/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('slide', models.ForeignKey(related_name='images', verbose_name='\u0421\u043b\u0430\u0439\u0434', to='content.HomepageSlide')),
            ],
            options={
                'ordering': ['slide', 'order'],
                'verbose_name': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043d\u0430 \u0441\u043b\u0430\u0439\u0434\u0430\u0445',
            },
        ),
        migrations.AlterField(
            model_name='homepagetestimonial',
            name='text',
            field=models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043e\u0442\u0437\u044b\u0432\u0430'),
        ),
    ]
