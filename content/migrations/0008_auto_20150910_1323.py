# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0007_auto_20150907_0146'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homepagebanner',
            name='clicks',
        ),
        migrations.RemoveField(
            model_name='homepagebanner',
            name='shows',
        ),
        migrations.RemoveField(
            model_name='homepagemanufacturer',
            name='clicks',
        ),
        migrations.RemoveField(
            model_name='homepagemanufacturer',
            name='shows',
        ),
    ]
