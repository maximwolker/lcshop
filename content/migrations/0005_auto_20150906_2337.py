# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0004_contentpage_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='HomepageBanner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('image', models.ImageField(upload_to='homepage/banners/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('target_blank', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435')),
                ('start_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('end_datetime', models.DateTimeField(verbose_name='\u041a\u043e\u043d\u0435\u0446 \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('clicks', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043a\u043b\u0438\u043a\u043e\u0432')),
                ('shows', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u043e\u043a\u0430\u0437\u043e\u0432')),
            ],
            options={
                'ordering': ['-show', 'order', '-end_datetime', '-start_datetime'],
                'verbose_name': '\u0431\u0430\u043d\u043d\u0435\u0440',
                'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u0431\u0430\u043d\u043d\u0435\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='HomepageManufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('image', models.ImageField(upload_to='homepage/manufacturers/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('target_blank', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435')),
                ('start_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('end_datetime', models.DateTimeField(verbose_name='\u041a\u043e\u043d\u0435\u0446 \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('clicks', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043a\u043b\u0438\u043a\u043e\u0432')),
                ('shows', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u043e\u043a\u0430\u0437\u043e\u0432')),
            ],
            options={
                'ordering': ['-show', 'order', '-end_datetime', '-start_datetime'],
                'verbose_name': '\u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a',
                'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='HomepageTestimonial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043e\u0442\u0437\u044b\u0432\u0430')),
                ('text', models.TextField(max_length=255, verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043e\u0442\u0437\u044b\u0432\u0430')),
                ('author', models.CharField(max_length=255, verbose_name='\u0410\u0432\u0442\u043e\u0440')),
                ('image', models.ImageField(upload_to='homepage/testimonials/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.URLField(max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0430\u0432\u0442\u043e\u0440\u0430', blank=True)),
                ('date', models.DateField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': '\u043e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u043e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
        migrations.DeleteModel(
            name='Banner',
        ),
        migrations.DeleteModel(
            name='Manufacturer',
        ),
    ]
