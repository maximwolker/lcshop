# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0015_remove_aboutustestimonial_image'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='aboutuspage',
            options={'verbose_name': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430 "\u041e \u043d\u0430\u0441"', 'verbose_name_plural': '\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430 "\u041e \u043d\u0430\u0441"'},
        ),
        migrations.AddField(
            model_name='aboutuspage',
            name='background_image',
            field=models.ImageField(help_text='\u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e, \u0441\u0438\u043d\u0438\u0439 \u0444\u043e\u043d', upload_to='about/bg/', null=True, verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430 \u0431\u043b\u043e\u043a\u0430 \u0441 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u043e\u043c', blank=True),
        ),
        migrations.AddField(
            model_name='aboutuspage',
            name='testimonials_bg_image',
            field=models.ImageField(help_text='\u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e, \u0436\u0435\u043b\u0442\u044b\u0439 \u0444\u043e\u043d', upload_to='about/bg/', null=True, verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430 \u0431\u043b\u043e\u043a\u0430 \u0441 \u043e\u0442\u0437\u044b\u0432\u0430\u043c\u0438', blank=True),
        ),
    ]
