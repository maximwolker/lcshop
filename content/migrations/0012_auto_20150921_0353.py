# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0011_auto_20150917_0531'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='homepageslide',
            options={'ordering': ['order', 'id'], 'verbose_name': '\u0441\u043b\u0430\u0439\u0434', 'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u0441\u043b\u0430\u0439\u0434\u044b'},
        ),
        migrations.AlterModelOptions(
            name='homepageslideimage',
            options={'ordering': ['slide', 'order', 'id'], 'verbose_name': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f \u043d\u0430 \u0441\u043b\u0430\u0439\u0434\u0430\u0445'},
        ),
        migrations.AddField(
            model_name='homepageslide',
            name='description_color',
            field=models.CharField(default='#4f5150', help_text='\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: "#80bfff", "rgb(49, 151, 116)", "red"', max_length=15, verbose_name='\u0426\u0432\u0435\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f'),
        ),
        migrations.AddField(
            model_name='homepageslide',
            name='title_color',
            field=models.CharField(default='#4f5150', help_text='\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: "#80bfff", "rgb(49, 151, 116)", "red"', max_length=15, verbose_name='\u0426\u0432\u0435\u0442 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u0430'),
        ),
    ]
