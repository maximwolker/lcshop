# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0013_auto_20150921_0356'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUsManufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('image', models.ImageField(help_text='\u0411\u0443\u0434\u0435\u0442 \u043e\u0431\u0440\u0435\u0437\u0430\u0442\u044c\u0441\u044f \u0434\u043e 170x100 px', upload_to='about/manufacturers/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.URLField(max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('target_blank', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435')),
                ('start_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('end_datetime', models.DateTimeField(verbose_name='\u041a\u043e\u043d\u0435\u0446 \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
            ],
            options={
                'ordering': ['-show', 'order', '-end_datetime', '-start_datetime'],
                'verbose_name': '\u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a',
                'verbose_name_plural': '\u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a\u0438',
            },
        ),
        migrations.CreateModel(
            name='AboutUsPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meta_title', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043b\u0435 "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a"', max_length=255, verbose_name='Meta title (\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_description', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_desc', max_length=255, verbose_name='Meta description (\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_keywords', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_keyw', max_length=255, verbose_name='Meta keywords (\u043a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e)', blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('description', models.TextField(null=True, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 (\u043f\u043e\u0434 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u043e\u043c)', blank=True)),
            ],
            options={
                'verbose_name': '\u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430 "\u041e \u043d\u0430\u0441"',
                'verbose_name_plural': '\u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430 "\u041e \u043d\u0430\u0441"',
            },
        ),
        migrations.CreateModel(
            name='AboutUsTestimonial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u043e\u0442\u0437\u044b\u0432\u0430')),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043e\u0442\u0437\u044b\u0432\u0430')),
                ('author', models.CharField(max_length=255, verbose_name='\u0410\u0432\u0442\u043e\u0440')),
                ('image', models.ImageField(upload_to='about/testimonials/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.URLField(max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430 \u043d\u0430 \u0430\u0432\u0442\u043e\u0440\u0430', blank=True)),
                ('date', models.DateField(default=django.utils.timezone.now, help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0440\u044f\u0434\u043e\u043c \u0441 \u0438\u043c\u0435\u043d\u0435\u043c \u0430\u0432\u0442\u043e\u0440\u0430', verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('page', models.ForeignKey(related_name='testimonials_all', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430', to='content.AboutUsPage')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': '\u043e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u043e\u0442\u0437\u044b\u0432\u044b',
            },
        ),
        migrations.CreateModel(
            name='AboutUsTextBlock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('text', ckeditor.fields.RichTextField(verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('page', models.ForeignKey(related_name='text_blocks', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430', to='content.AboutUsPage')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0439 \u0431\u043b\u043e\u043a',
                'verbose_name_plural': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0435 \u0431\u043b\u043e\u043a\u0438',
            },
        ),
        migrations.AddField(
            model_name='aboutusmanufacturer',
            name='page',
            field=models.ForeignKey(related_name='manufacturers_all', verbose_name='\u0421\u0442\u0440\u0430\u043d\u0438\u0446\u0430', to='content.AboutUsPage'),
        ),
    ]
