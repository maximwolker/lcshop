# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0012_auto_20150921_0353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepageslide',
            name='description_color',
            field=models.CharField(default='#4f5150', help_text='\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: "#80bfff", "rgb(49, 151, 116)", "red"', max_length=31, verbose_name='\u0426\u0432\u0435\u0442 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='homepageslide',
            name='title_color',
            field=models.CharField(default='#4f5150', help_text='\u041d\u0430\u043f\u0440\u0438\u043c\u0435\u0440: "#80bfff", "rgb(49, 151, 116)", "red"', max_length=31, verbose_name='\u0426\u0432\u0435\u0442 \u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043a\u0430'),
        ),
    ]
