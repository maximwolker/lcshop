# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0008_auto_20150910_1323'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepagebanner',
            name='image',
            field=models.ImageField(help_text='\u0411\u0443\u0434\u0435\u0442 \u043e\u0431\u0440\u0435\u0437\u0430\u0442\u044c\u0441\u044f \u0434\u043e 270x400 px', upload_to='homepage/banners/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='homepagebanner',
            name='target_blank',
            field=models.BooleanField(default=True, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435'),
        ),
        migrations.AlterField(
            model_name='homepagemanufacturer',
            name='image',
            field=models.ImageField(help_text='\u0411\u0443\u0434\u0435\u0442 \u043e\u0431\u0440\u0435\u0437\u0430\u0442\u044c\u0441\u044f \u0434\u043e 170x100 px', upload_to='homepage/manufacturers/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
        ),
        migrations.AlterField(
            model_name='homepagemanufacturer',
            name='target_blank',
            field=models.BooleanField(default=True, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435'),
        ),
    ]
