# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0005_auto_20150906_2337'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='homepagebanner',
            options={'ordering': ['-show', 'order', '-end_datetime', '-start_datetime'], 'verbose_name': '\u0431\u0430\u043d\u043d\u0435\u0440', 'verbose_name_plural': '\u0433\u043b\u0430\u0432\u043d\u0430\u044f: \u0431\u0430\u043d\u043d\u0435\u0440\u044b \u0441\u043f\u0440\u0430\u0432\u0430'},
        ),
        migrations.AlterField(
            model_name='homepagetestimonial',
            name='date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f'),
        ),
    ]
