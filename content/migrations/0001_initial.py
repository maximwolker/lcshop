# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('image', models.ImageField(upload_to='banners/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('location', models.CharField(default='left', max_length=10, verbose_name='\u0420\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u0435', choices=[('right', '\u0441\u043f\u0440\u0430\u0432\u0430'), ('bottom', '\u0441\u043d\u0438\u0437\u0443'), ('elsewhere', '\u0433\u0434\u0435-\u0442\u043e \u0435\u0449\u0451')])),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('target_blank', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435')),
                ('start_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('end_datetime', models.DateTimeField(verbose_name='\u041a\u043e\u043d\u0435\u0446 \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('clicks', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043a\u043b\u0438\u043a\u043e\u0432')),
                ('shows', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u043e\u043a\u0430\u0437\u043e\u0432')),
            ],
            options={
                'ordering': ['-show', 'location', 'order', '-end_datetime', '-start_datetime'],
                'verbose_name': '\u0431\u0430\u043d\u043d\u0435\u0440',
                'verbose_name_plural': '\u0431\u0430\u043d\u043d\u0435\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='ContactsMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('email', models.EmailField(max_length=255, verbose_name='Email')),
                ('subject', models.TextField(max_length=255, verbose_name='\u0422\u0435\u043c\u0430')),
                ('message', models.TextField(verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f \u0441\u043e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='ContentPage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meta_title', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043b\u0435 "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a"', max_length=255, verbose_name='Meta title (\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_description', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_desc', max_length=255, verbose_name='Meta description (\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_keywords', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_keyw', max_length=255, verbose_name='Meta keywords (\u043a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e)', blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slug', models.SlugField(unique=True, max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0432 url')),
                ('text', ckeditor.fields.RichTextField(verbose_name='\u0422\u0435\u043a\u0441\u0442')),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': '\u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u043a\u043e\u043d\u0442\u0435\u043d\u0442\u043d\u044b\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435 \u0438 \u0432 \u0442\u0435\u0433\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('image', models.ImageField(upload_to='manufacturers/', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('url', models.CharField(max_length=255, null=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True)),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('target_blank', models.BooleanField(default=False, verbose_name='\u041e\u0442\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0432 \u043d\u043e\u0432\u043e\u043c \u043e\u043a\u043d\u0435')),
                ('start_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('end_datetime', models.DateTimeField(verbose_name='\u041a\u043e\u043d\u0435\u0446 \u0440\u0430\u0437\u043c\u0435\u0449\u0435\u043d\u0438\u044f')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('clicks', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043a\u043b\u0438\u043a\u043e\u0432')),
                ('shows', models.IntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043f\u043e\u043a\u0430\u0437\u043e\u0432')),
            ],
            options={
                'ordering': ['-show', 'order', '-end_datetime', '-start_datetime'],
                'verbose_name': '\u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a',
                'verbose_name_plural': '\u043f\u043e\u0441\u0442\u0430\u0432\u0449\u0438\u043a\u0438 \u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0435 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439',
            },
        ),
    ]
