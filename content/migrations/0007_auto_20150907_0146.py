# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0006_auto_20150907_0145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepagebanner',
            name='url',
            field=models.URLField(default='', max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='homepagemanufacturer',
            name='url',
            field=models.URLField(default='', max_length=255, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=False,
        ),
    ]
