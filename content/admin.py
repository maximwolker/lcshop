# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# from mptt.admin import MPTTModelAdmin
from registration.models import RegistrationProfile

from site_settings.admin import MetatagModelAdmin
from .models import (ContentPage, AboutUsPage, AboutUsTextBlock, AboutUsTestimonial, AboutUsManufacturer,
                     HomepageBanner, HomepageManufacturer, HomepageTestimonial,
                     HomepageSlide, HomepageSlideImage, ContactsMessage)
from .utils import IsPublishedNow


@admin.register(ContentPage)
@MetatagModelAdmin
class ContentPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug',)
    list_editable = ('slug',)
    fieldsets = (
        (None, {
            'fields': (('title', 'slug',),  'description', 'text',)
        }),
    )
    search_fields = ['title', 'description', 'slug', 'text', ]
    prepopulated_fields = {"slug": ("title",)}


class AboutUsTextBlockInline(admin.StackedInline):
    model = AboutUsTextBlock
    extra = 1


class AboutUsTestimonialInline(admin.StackedInline):
    model = AboutUsTestimonial
    extra = 0


class AboutUsManufacturerInline(admin.StackedInline):
    model = AboutUsManufacturer
    extra = 0


@admin.register(AboutUsPage)
@MetatagModelAdmin
class AboutUsPageAdmin(admin.ModelAdmin):
    list_display = ('show_title',)
    fieldsets = (
        (None, {
            'fields': ('title',  'description', ('background_image', 'testimonials_bg_image',),)
        }),
    )
    search_fields = ['title', 'description', ]
    inlines = [AboutUsTextBlockInline, AboutUsTestimonialInline, AboutUsManufacturerInline, ]

    def has_add_permission(self, request):
        return None

    def has_delete_permission(self, request, obj=None):
        return None


@admin.register(HomepageBanner)
class HomepageBannerAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'image', 'url', 'show', 'order',
                    'start_datetime', 'end_datetime',)
    list_display_links = ('id', 'title',)
    list_filter = ('show', IsPublishedNow)
    list_editable = ('order',)

    fieldsets = (
        (None, {
            'fields': ('title', ('image', 'url',), 'target_blank', 'show', 'order',
                       'start_datetime', 'end_datetime',)
        }),
    )


@admin.register(HomepageManufacturer)
class HomepageManufacturerAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'image', 'url', 'show', 'order',
                    'start_datetime', 'end_datetime',)
    list_display_links = ('id', 'title',)
    list_filter = ('show', IsPublishedNow)
    list_editable = ('order',)

    fieldsets = (
        (None, {
            'fields': ('title', ('image', 'url',), 'target_blank', 'show', 'order',
                       'start_datetime', 'end_datetime',)
        }),
    )


@admin.register(HomepageTestimonial)
class HomepageTestimonialAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'url', 'date', 'order', 'show',)
    list_filter = ('show',)
    search_fields = ['title', 'text', 'author', 'url', ]


class SliderImageInline(admin.TabularInline):
    model = HomepageSlideImage
    fields = ('title', 'image', 'order',)
    extra = 4


@admin.register(HomepageSlide)
class HomepageSlideAdmin(admin.ModelAdmin):
    list_display = ('title', 'slide_type', 'show_price', 'order', 'show',)
    list_filter = ('slide_type', 'show',)
    list_editable = ('order',)
    inlines = [SliderImageInline, ]


@admin.register(ContactsMessage)
class ContactsMessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'name', 'email', 'subject',)
    list_display_links = ('id', 'datetime',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), 'profile',)
        }),
        ('Данные из формы', {
            'fields': ('name', 'email', 'subject', 'message',)
        }),
    )
    search_fields = ['email', 'name', 'subject', 'message', ]
    readonly_fields = ('id', 'datetime', 'profile', 'name', 'email', 'subject', 'message',)


admin.site.unregister(RegistrationProfile)
