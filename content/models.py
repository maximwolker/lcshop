# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.utils import timezone

from ckeditor.fields import RichTextField

from core.utils import get_formatted_dt
from lk.models import Profile
from site_settings.models import MetatagModel


class ContentPage(MetatagModel):
    title = models.CharField('Заголовок', max_length=255)
    description = models.TextField('Описание (под заголовком)', blank=True, null=True)
    slug = models.SlugField('Адрес в url', max_length=255, unique=True)
    text = RichTextField('Текст')

    class Meta:
        ordering = ['title', ]
        verbose_name = 'страница'
        verbose_name_plural = 'контентные страницы'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('page', kwargs={'slug': self.slug})


class ActiveObjectManager(models.Manager):

    def get_queryset(self):
        return super(ActiveObjectManager, self).get_queryset().filter(
            Q(start_datetime__lte=get_formatted_dt())
            & Q(end_datetime__gte=get_formatted_dt())
            & Q(show=True)
        )


class AboutUsPage(MetatagModel):
    title = models.CharField('Заголовок', max_length=255)
    description = models.TextField('Описание (под заголовком)', blank=True, null=True)
    background_image = models.ImageField('Обложка блока с заголовком', upload_to='about/bg/', blank=True, null=True,
                                         help_text='по умолчанию, синий фон')
    testimonials_bg_image = models.ImageField('Обложка блока с отзывами', upload_to='about/bg/', blank=True, null=True,
                                              help_text='по умолчанию, желтый фон')

    class Meta:
        verbose_name = 'Страница "О нас"'
        verbose_name_plural = 'Страница "О нас"'

    def __unicode__(self):
        return 'Страница "О нас"'

    def show_title(self):
        return self.__unicode__()
    show_title.allow_tags = True
    show_title.short_description = ''

    @property
    def testimonials(self):
        return self.testimonials_all.filter(show=True)

    @property
    def manufacturers(self):
        return AboutUsManufacturer.active_objects.filter(page=self)

    def get_absolute_url(self):
        return reverse('about')


class AboutUsTextBlock(models.Model):
    page = models.ForeignKey(AboutUsPage, verbose_name='Страница', related_name='text_blocks')
    title = models.CharField('Заголовок', max_length=255)
    text = RichTextField('Текст')
    order = models.IntegerField('Порядок', default=10)

    class Meta:
        ordering = ['order', ]
        verbose_name = 'текстовый блок'
        verbose_name_plural = 'текстовые блоки'

    def __unicode__(self):
        return self.title


class AboutUsTestimonial(models.Model):
    page = models.ForeignKey(AboutUsPage, verbose_name='Страница', related_name='testimonials_all')
    title = models.CharField('Заголовок отзыва', max_length=255)
    text = models.TextField('Текст отзыва')

    author = models.CharField('Автор', max_length=255)
    url = models.URLField('Ссылка на автора', max_length=255, blank=True)
    date = models.DateField('Время добавления', default=timezone.now, help_text='Отображается рядом с именем автора')

    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField("Показывать на сайте", default=True)

    class Meta:
        ordering = ['order', ]
        verbose_name = 'отзыв'
        verbose_name_plural = 'отзывы'

    def __unicode__(self):
        return self.title


class AboutUsManufacturer(models.Model):
    page = models.ForeignKey(AboutUsPage, verbose_name='Страница', related_name='manufacturers_all')
    title = models.CharField('Заголовок', max_length=255, help_text='Для показа в админке и в теге alt', blank=True)
    image = models.ImageField('Изображение', upload_to='about/manufacturers/',
                              help_text='Будет обрезаться до 170x100 px')
    url = models.URLField('Ссылка', max_length=255, blank=True)

    show = models.BooleanField("Показывать на сайте", default=True)
    target_blank = models.BooleanField("Открывать в новом окне", default=True)
    start_datetime = models.DateTimeField('Начало размещения', default=timezone.now)
    end_datetime = models.DateTimeField('Конец размещения')

    order = models.IntegerField('Порядок', default=10)

    objects = models.Manager()
    active_objects = ActiveObjectManager()

    class Meta:
        ordering = ['-show', 'order', '-end_datetime', '-start_datetime', ]
        verbose_name = "поставщик"
        verbose_name_plural = "поставщики"

    def __unicode__(self):
        return self.title or '#{0}'.format(self.id)


class HomepageBanner(models.Model):
    title = models.CharField('Заголовок', max_length=255, help_text='Для показа в админке и в теге alt', blank=True)
    image = models.ImageField('Изображение', upload_to='homepage/banners/',
                              help_text='Будет обрезаться до 270x400 px')
    url = models.URLField('Ссылка', max_length=255, blank=True)

    show = models.BooleanField("Показывать на сайте", default=True)
    target_blank = models.BooleanField("Открывать в новом окне", default=True)
    start_datetime = models.DateTimeField('Начало размещения', default=timezone.now)
    end_datetime = models.DateTimeField('Конец размещения')

    order = models.IntegerField('Порядок', default=10)

    objects = models.Manager()
    active_objects = ActiveObjectManager()

    class Meta:
        ordering = ['-show', 'order', '-end_datetime', '-start_datetime', ]
        verbose_name = "баннер"
        verbose_name_plural = "главная: баннеры справа"

    def __unicode__(self):
        return self.title or self.id


class HomepageManufacturer(models.Model):
    title = models.CharField('Заголовок', max_length=255, help_text='Для показа в админке и в теге alt', blank=True)
    image = models.ImageField('Изображение', upload_to='homepage/manufacturers/',
                              help_text='Будет обрезаться до 170x100 px')
    url = models.URLField('Ссылка', max_length=255, blank=True)

    show = models.BooleanField("Показывать на сайте", default=True)
    target_blank = models.BooleanField("Открывать в новом окне", default=True)
    start_datetime = models.DateTimeField('Начало размещения', default=timezone.now)
    end_datetime = models.DateTimeField('Конец размещения')

    order = models.IntegerField('Порядок', default=10)

    objects = models.Manager()
    active_objects = ActiveObjectManager()

    class Meta:
        ordering = ['-show', 'order', '-end_datetime', '-start_datetime', ]
        verbose_name = "поставщик"
        verbose_name_plural = "главная: поставщики"

    def __unicode__(self):
        return self.title or '#{0}'.format(self.id)


class HomepageTestimonial(models.Model):
    title = models.CharField('Заголовок отзыва', max_length=255)
    text = models.TextField('Текст отзыва')

    author = models.CharField('Автор', max_length=255)
    image = models.ImageField('Изображение', upload_to='homepage/testimonials/')
    url = models.URLField('Ссылка на автора', max_length=255, blank=True)
    date = models.DateField('Время добавления', default=timezone.now, help_text='Отображается рядом с именем автора')

    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField("Показывать на сайте", default=True)

    class Meta:
        ordering = ['order', ]
        verbose_name = 'отзыв'
        verbose_name_plural = 'главная: отзывы'

    def __unicode__(self):
        return self.title


class HomepageSlide(models.Model):
    SLIDE_TYPES = (
        (1, 'Тип #1 (до 1 изображения)'),
        (2, 'Тип #2 (до 2 изображений)'),
        (3, 'Тип #3 (до 4 изображений)'),
    )
    slide_type = models.PositiveSmallIntegerField('Тип слайда', choices=SLIDE_TYPES)
    background = models.ImageField('Фон слайда', upload_to='homepage/slider/backgrounds/',
                                   help_text='Будет обрезаться до 1920x600 px')

    title = models.CharField('Заголовок', max_length=255)
    title_color = models.CharField('Цвет заголовка', max_length=31, default='#4f5150',
                                   help_text='Например: "#80bfff", "rgb(49, 151, 116)", "red"')
    description = models.TextField('Описание', blank=True)
    description_color = models.CharField('Цвет описания', max_length=31, default='#4f5150',
                                         help_text='Например: "#80bfff", "rgb(49, 151, 116)", "red"')

    button_link = models.URLField('Ссылка на кнопке', max_length=255, blank=True,
                                  help_text='Оставьте пустым, чтобы не показывать кнопку')
    button_title = models.CharField('Текст на кнопке', max_length=127, blank=True, default='Узнать больше',
                                    help_text='Оставьте пустым, чтобы не показывать кнопку')
    button_target_blank = models.BooleanField("Открывать ссылку в новом окне", default=True)

    price = models.CharField('Цена в зеленом кружке', max_length=15, blank=True)
    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField("Показывать на сайте", default=True)

    class Meta:
        ordering = ['order', 'id', ]
        verbose_name = 'слайд'
        verbose_name_plural = 'главная: слайды'

    def __unicode__(self):
        return self.title

    def show_price(self):
        return self.price or ''
    show_price.allow_tags = True
    show_price.short_description = 'Цена в кружке'


class HomepageSlideImage(models.Model):
    slide = models.ForeignKey(HomepageSlide, verbose_name='Слайд', related_name='images')
    title = models.CharField('Заголовок', max_length=255, help_text='Для показа в админке и в теге alt', blank=True)
    image = models.ImageField('Изображение', upload_to='homepage/slider/images/')

    order = models.IntegerField('Порядок', default=10)

    class Meta:
        ordering = ['slide', 'order', 'id', ]
        verbose_name = 'изображение'
        verbose_name_plural = 'главная: изображения на слайдах'

    def __unicode__(self):
        return self.title or '#{0}'.format(self.id)

    def show_title(self):
        return self.title or '{0} (#{1})'.format(self.slide.title, self.id)


class ContactsMessage(models.Model):
    profile = models.ForeignKey(Profile, verbose_name='Пользователь', null=True, blank=True)
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)

    name = models.CharField('Имя', max_length=255)
    email = models.EmailField('Email', max_length=255)
    subject = models.CharField('Тема', max_length=255)
    message = models.TextField('Сообщение')

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'сообщение'
        verbose_name_plural = 'сообщения со страницы контактов'

    def __unicode__(self):
        return '{0} ({1})'.format(self.email, self.datetime)
