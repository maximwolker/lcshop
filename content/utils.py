# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.contrib.admin.filters import SimpleListFilter
from django.db.models import Q


class IsPublishedNow(SimpleListFilter):
    """
    Дополнительный фильтр для баннеров в админке
    """
    title = 'Размещены сейчас'
    parameter_name = 'published'

    def lookups(self, request, model_admin):
        return (
            ('yes', 'Да'),
            ('no', 'Нет'),
        )

    def queryset(self, request, queryset):
        dt = datetime.now()
        formatted_dt = '{0}-{1}-{2} {3}:{4}'.format(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second)

        if self.value() == 'yes':
            return queryset.filter(
                Q(start_datetime__lte=formatted_dt)
                & Q(end_datetime__gte=formatted_dt)
                & Q(show=True)
            )

        if self.value() == 'no':
            return queryset.exclude(
                Q(start_datetime__lte=formatted_dt)
                & Q(end_datetime__gte=formatted_dt)
                & Q(show=True)
            )
