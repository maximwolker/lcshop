# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from .models import ContactsMessage


class FeedbackForm(forms.ModelForm):

    class Meta:
        model = ContactsMessage
        fields = ('profile', 'name', 'email', 'subject', 'message',)
