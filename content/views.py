# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView, DetailView, CreateView, View, ListView

from pure_pagination.mixins import PaginationMixin

from catalog.models import Category, Product, ProductColor
from lk.email import send_feedback_email
from lk.models import NewsletterSubscription
from .forms import FeedbackForm
from .models import ContentPage, AboutUsPage, HomepageSlide


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        last_products = self.request.session.get('last_products', [])
        l = last_products
        homepage_products = []

        for product_id in last_products:
            try:
                # import ipdb; ipdb.set_trace()
                product = Product.objects.get(id=product_id)
                homepage_products.append(product)
            except Product.DoesNotExist:
                l.pop(l.index(product_id))
                self.request.session['last_products'] = l

        if not len(homepage_products):
            homepage_products = Product.objects.filter(show=True, at_homepage_catalog=True)[:6]

        categories = [
            ('Все', homepage_products),
        ]
        for category in Category.objects.filter(show=True, level=0):
            products = category.products.filter(at_homepage_catalog=True)[:6]
            if products.count():
                categories.append((category.title, products))

        context = {
            'categories': categories,
            'products_on_sale': Product.objects.filter(show=True, at_homepage_slider=True)[:10],
            'slider': HomepageSlide.objects.filter(show=True),
        }
        context.update(super(IndexView, self).get_context_data(**kwargs))
        return context


class PageView(DetailView):
    template_name = "page.html"
    model = ContentPage
    context_object_name = 'page'


class AboutUsPageView(TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        context = super(AboutUsPageView, self).get_context_data(**kwargs)
        context['page'] = AboutUsPage.objects.first()
        return context


class FeedbackFormView(CreateView):
    form_class = FeedbackForm
    # email_function = None

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        super(FeedbackFormView, self).form_valid(form)
        send_feedback_email(self.request, form.instance)
        data = {
            'errors': [],
            'result': 'ok',
        }
        return JsonResponse(data)


class SubscribeFormView(View):

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        subscription, created = NewsletterSubscription.objects.get_or_create(email=email)

        if created:
            if not request.user.is_anonymous():
                subscription.profile = request.user
                subscription.save()
            data = {
                'errors': [],
                'result': 'ok',
            }

        else:
            data = {
                'errors': [],
                'result': 'info',
                'info_message': 'Этот email уже внесен в базу рассылки.'
            }
        return JsonResponse(data)


class SearchView(PaginationMixin, ListView):
    TEMPLATES = {
        'default': "search.html",
        'ajax': 'ajax/search_product_list.html',
    }
    model = Product
    context_object_name = 'products'
    paginate_by = 15
    f = {}
    order_by = 'price'
    PRICE_QUERY = 'COALESCE(new_price, price)'
    ORDER_BY_CHOICES = {
        'price': '0 Цене (по возрастанию)',
        '-price': '0 Цене (по убыванию)',
        'title': '1 Имени (по возрастанию)',
        '-title': '1 Имени (по убыванию)',
        '-update_dt': '2 Дате',
    }

    def get(self, request, *args, **kwargs):
        self.q = request.GET.get('q')
        return super(SearchView, self).get(request, *args, **kwargs)

    def get_template_names(self):
        if self.request.is_ajax():
            return self.TEMPLATES['ajax']
        return self.TEMPLATES['default']

    def get_price_minimax(self, queryset):
        prices = queryset.values_list('p', flat=True)
        max_price = max(prices) if len(prices) else 1
        return {'min': int(min(prices)) if len(prices) > 1 else 0, 'max': int(max_price) if int(max_price) == max_price else int(max(prices)) + 1}

    def get_filter(self):
        f = {}
        if self.colors:
            f['colors__hex_value__in'] = self.colors

        unicode_attrs = {k: v for k, v in self.attrs_filter.iteritems() if isinstance(v, unicode)}
        if unicode_attrs:
            f['attrs__contains'] = unicode_attrs

        self.f = f
        return f

    def get_color_set(self):
        colors = set(ProductColor.objects.filter(
            product__category_id__in=self.f['category_ids'], product__show=True
        ).values_list('hex_value', flat=True))
        return [color.split('#')[-1] for color in colors]

    def get_attrs(self):
        categories = [Category.objects.get(id=c_id) for c_id in self.f['category_ids']]
        attrs = []; attr_names = set([])
        for category in categories:
            for field in category.attrs:
                if not (field['name'].endswith('-new') or field['name'] in attr_names):
                    attrs.append(field)
                    attr_names.add(field['name'])
        return attrs

    def get_attrs_with_values(self, queryset):
        products = queryset
        attrs = []
        for attr in self.get_attrs():
            a_name = attr['name']
            attr_dict = {
                'attr': attr,
                'values': ['Да', 'Нет'] if attr['class'] == 'BooleanField' else sorted(list(set(
                    [value for value in products.extra(
                        select={a_name: "catalog_product.attrs -> '{0}'".format(a_name)}
                    ).values_list(a_name, flat=True) if value]
                ))),
            }
            attrs.append(attr_dict)
        return attrs

    def get_queryset(self):
        GET = self.request.GET

        order_by = GET.get('order_by', self.order_by)
        if order_by in self.ORDER_BY_CHOICES.keys():
            self.order_by = order_by

        queryset = Product.objects.filter(show=True).extra(select={'p': self.PRICE_QUERY})

        if self.q:
            queryset = queryset.filter((Q(title__icontains=self.q) | Q(description__icontains=self.q)))

        self.sidebar_category_ids = sorted(list(set(list(queryset.values_list('category_id', flat=True)))))

        self.sidebar_category_counts = {}
        for cat_id in self.sidebar_category_ids:
            self.sidebar_category_counts[cat_id] = queryset.filter(category_id__in=Category.objects.get(id=cat_id).me_with_children).count()

        # root_categories = Category.objects.filter(show=True, level=0)
        # self.sidebar_categories = [self._get_sidebar_category_dict(queryset, self.sidebar_category_ids, cat.id)
        #                            for cat in root_categories]

        self.f = {}
        self.f['category_ids'] = [int(c) for c in self.request.GET.getlist('c')]

        if len(self.f['category_ids']):
            queryset = queryset.filter(category_id__in=self.f['category_ids'])

            self.colors = ['#{0}'.format(color) for color in GET.getlist('color')]
            self.f['attrs_filter'] = {key.split('e_')[-1]: (GET.getlist(key) if len(GET.getlist(key)) != 1 else GET.get(key))
                                      for key in GET.keys() if key.startswith('e_')}
            self.attrs_filter = self.f['attrs_filter']

            self.f['price_minimax'] = self.get_price_minimax(queryset)
            p = GET.get('price', '').split('-')
            price = {'min': int(p[0]), 'max': int(p[1])} if len(p) > 1 else {}
            self.f['price'] = price or self.f['price_minimax']
            self.price = self.f['price']
            self.f['filter_by_price'] = True if price else False

            self.f['color_set'] = self.get_color_set()

            self.f['attrs_with_values'] = self.get_attrs_with_values(queryset)

            if self.colors or self.f['attrs_filter'] or price:
                queryset = queryset.filter(**self.get_filter()).distinct()
                if price:
                    queryset = queryset.extra(
                        where=['{0} >= %s'.format(self.PRICE_QUERY)], params=[self.price['min']]
                    ).extra(
                        where=['{0} <= %s'.format(self.PRICE_QUERY)], params=[self.price['max']]
                    )

                array_attrs = {k: v for k, v in self.attrs_filter.iteritems() if isinstance(v, list)}
                for k, values in array_attrs.iteritems():
                    s = ', '.join(["'{0}=>{1}'::hstore".format(k, value) for value in values])
                    queryset = queryset.extra(where=['"catalog_product"."attrs"::hstore @> ANY(ARRAY[%s])' % s])

        order_by = 'p' if self.order_by == 'price' else '-p' if self.order_by == '-price' else self.order_by
        queryset = queryset.order_by(order_by)
        return queryset

    def get_context_data(self, **kwargs):
        context = {
            'sidebar_category_ids': self.sidebar_category_ids,
            'sidebar_category_counts': self.sidebar_category_counts,
            'f': self.f,
            # 'attrs_filter': self.attrs_filter,
            # 'price_minimax': self.price_minimax,
            # 'price': self.price,
            # 'filter_by_price': self.filter_by_price,
            'order_by': {
                'value': self.order_by,
                'choices': [(k, v[2:]) for k, v in sorted(self.ORDER_BY_CHOICES.items(), key=lambda t: t[1])],
                'title': self.ORDER_BY_CHOICES.get(self.order_by)[2:],
            },
            'ajax_container': 'product_list',
        }
        context.update(super(SearchView, self).get_context_data(**kwargs))
        return context
