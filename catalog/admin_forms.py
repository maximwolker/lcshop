# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin
from django.forms import widgets
from django.utils.html import format_html
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe

from .models import Product


class DifferentAddChangeAdminMixin(admin.ModelAdmin):
    """
    django.contrib.auth.admin.UserAdmin
    """
    IS_POPUP_VAR = '_popup'

    def response_add(self, request, obj, post_url_continue=None):
        if '_addanother' not in request.POST and self.IS_POPUP_VAR not in request.POST:
            request.POST['_continue'] = 1
        return super(DifferentAddChangeAdminMixin, self).response_add(request, obj,
                                                                      post_url_continue)

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super(DifferentAddChangeAdminMixin, self).get_fieldsets(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super(DifferentAddChangeAdminMixin, self).get_form(request, obj, **defaults)


class DisabledSelectWidget(widgets.Select):
    """
    Вешаем disabled на все опшны, кроме выбранного
    """

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                selected_choices.remove(option_value)
        else:
            selected_html = ' disabled'
        return format_html('<option value="{0}"{1}>{2}</option>',
                           option_value,
                           selected_html,
                           force_text(option_label))


class ProductAddForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = ('title', 'category', 'seller', 'price',)


class ProductChangeForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'category': DisabledSelectWidget(),
        }
