# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView, CreateView

from pure_pagination.mixins import PaginationMixin

from lk.models import Profile
from lk.email import send_new_product_email, send_request_email
from .forms import AddProductForm, ReviewForm, ProductRequestForm
from .models import Category, Product, ProductImage, ProductColor


class CatalogHomeView(TemplateView):
    template_name = "catalog/home.html"

    def get_context_data(self, **kwargs):
        categories = [('Все', '', Product.objects.filter(show=True)[:6]), ]
        for category in Category.objects.filter(show=True, level=0):
            products = category.products.filter(show=True)[:6]
            if products.count():
                categories.append((category.title, category.get_absolute_url(), products))

        context = {'categories': categories}
        context.update(super(CatalogHomeView, self).get_context_data(**kwargs))
        return context


class CategoryView(PaginationMixin, ListView):
    TEMPLATES = {
        'default': "catalog/category.html",
        'ajax': 'catalog/ajax/product_list.html',
    }
    model = Product
    context_object_name = 'products'
    paginate_by = 15
    f = {}
    order_by = '-update_dt'
    PRICE_QUERY = 'COALESCE(new_price, price)'
    ORDER_BY_CHOICES = {
        '-update_dt': '0 Дате',
        'price': '1 Цене (по возрастанию)',
        '-price': '1 Цене (по убыванию)',
        'title': '2 Имени (по возрастанию)',
        '-title': '2 Имени (по убыванию)',
    }

    def get_template_names(self):
        if self.request.is_ajax():
            return self.TEMPLATES['ajax']
        return self.TEMPLATES['default']

    def get_category(self):
        return get_object_or_404(Category, slug=self.kwargs['slug'], show=True)

    def get_price_minimax(self, queryset):
        prices = queryset.values_list('p', flat=True)
        max_price = max(prices) if len(prices) else 1
        return {'min': int(min(prices)) if len(prices) > 1 else 0, 'max': int(max_price) if int(max_price) == max_price else int(max(prices)) + 1}

    def get_filter(self):
        f = {}
        if self.colors:
            f['colors__hex_value__in'] = self.colors

        unicode_attrs = {k: v for k, v in self.attrs_filter.iteritems() if isinstance(v, unicode)}
        if unicode_attrs:
            f['attrs__contains'] = unicode_attrs

        self.f = f
        return f

    def get_queryset(self):
        self.category = self.get_category()
        queryset = self.category.products.extra(select={'p': self.PRICE_QUERY})

        GET = self.request.GET

        order_by = GET.get('order_by', self.order_by)
        if order_by in self.ORDER_BY_CHOICES.keys():
            self.order_by = order_by

        self.colors = ['#{0}'.format(color) for color in GET.getlist('color')]
        self.attrs_filter = {key.split('e_')[-1]: (GET.getlist(key) if len(GET.getlist(key)) != 1 else GET.get(key))
                             for key in GET.keys() if key.startswith('e_')}

        self.price_minimax = self.get_price_minimax(queryset)
        p = GET.get('price', '').split('-')
        price = {'min': int(p[0]), 'max': int(p[1])} if len(p) > 1 else {}
        self.price = price or self.price_minimax
        self.filter_by_price = True if price else False

        if self.colors or self.attrs_filter or price:
            queryset = queryset.filter(**self.get_filter()).distinct()
            if price:
                queryset = queryset.extra(
                    where=['{0} >= %s'.format(self.PRICE_QUERY)], params=[self.price['min']]
                ).extra(
                    where=['{0} <= %s'.format(self.PRICE_QUERY)], params=[self.price['max']]
                )

            array_attrs = {k: v for k, v in self.attrs_filter.iteritems() if isinstance(v, list)}
            for k, values in array_attrs.iteritems():
                s = ', '.join(["'{0}=>{1}'::hstore".format(k, value) for value in values])
                queryset = queryset.extra(where=['"catalog_product"."attrs"::hstore @> ANY(ARRAY[%s])' % s])

        order_by = 'p' if self.order_by == 'price' else '-p' if self.order_by == '-price' else self.order_by
        queryset = queryset.order_by(order_by)
        return queryset

    def get_context_data(self, **kwargs):
        context = {
            'category': self.category,
            'f': self.f,
            'attrs_filter': self.attrs_filter,
            'price_minimax': self.price_minimax,
            'price': self.price,
            'filter_by_price': self.filter_by_price,
            'order_by': {
                'value': self.order_by,
                'choices': [(k, v[2:]) for k, v in sorted(self.ORDER_BY_CHOICES.items(), key=lambda t: t[1])],
                'title': self.ORDER_BY_CHOICES.get(self.order_by)[2:],
            },
            'ajax_container': 'product_list',
        }
        context.update(super(CategoryView, self).get_context_data(**kwargs))
        return context


class SellerView(PaginationMixin, ListView):
    TEMPLATES = {
        'default': "catalog/seller.html",
        'ajax': 'catalog/ajax/product_list.html',
    }
    model = Product
    context_object_name = 'products'
    paginate_by = 15
    # paginate_by = 3
    order_by = '-update_dt'
    PRICE_QUERY = 'COALESCE(new_price, price)'
    ORDER_BY_CHOICES = {
        '-update_dt': '0 Дате',
        'price': '1 Цене (по возрастанию)',
        '-price': '1 Цене (по убыванию)',
        'title': '2 Имени (по возрастанию)',
        '-title': '2 Имени (по убыванию)',
    }

    def get_seller(self):
        return get_object_or_404(Profile, profile_type='company', id=int(self.kwargs['seller_id']), is_active=True, is_blocked=False)

    def get_category(self):
        if self.kwargs.get('category_slug'):
            return get_object_or_404(Category, slug=self.kwargs['category_slug'], show=True)
        return None

    def get_template_names(self):
        if self.request.is_ajax():
            return self.TEMPLATES['ajax']
        return self.TEMPLATES['default']

    def get_queryset(self):
        self.seller = self.get_seller()
        self.category = self.get_category()
        queryset = self.seller.shown_products.extra(select={'p': self.PRICE_QUERY})
        if self.category:
            queryset = queryset.filter(category__in=self.category.me_with_children)

        GET = self.request.GET

        order_by = GET.get('order_by', self.order_by)
        if order_by in self.ORDER_BY_CHOICES.keys():
            self.order_by = order_by

        order_by = 'p' if self.order_by == 'price' else '-p' if self.order_by == '-price' else self.order_by
        queryset = queryset.order_by(order_by)
        return queryset

    def get_context_data(self, **kwargs):
        category_set = []
        for category in Category.objects.filter(level=0):
            count = self.seller.shown_products.filter(category__in=category.me_with_children).count()
            if count:
                category_set.append((category, count))

        context = {
            'seller': self.seller,
            'category': self.category,
            'category_set': category_set,
            'order_by': {
                'value': self.order_by,
                'choices': [(k, v[2:]) for k, v in sorted(self.ORDER_BY_CHOICES.items(), key=lambda t: t[1])],
                'title': self.ORDER_BY_CHOICES.get(self.order_by)[2:],
            },
            'ajax_container': 'product_list',
        }
        context.update(super(SellerView, self).get_context_data(**kwargs))
        return context


class GetCategoryAttrs(DetailView):
    template_name = "catalog/include/category_fields.html"
    model = Category
    context_object_name = 'category'


class ProductView(DetailView):
    template_name = "catalog/product.html"
    model = Product
    context_object_name = 'product'

    def get_object(self, queryset=None):
        product = super(ProductView, self).get_object(queryset)
        if product.removed or not product.show:
            raise Http404

        last_products = self.request.session.get('last_products', [])
        try:
            last_products.pop(last_products.index(product.id))
        except ValueError:
            pass
        finally:
            last_products.insert(0, product.id)
            if len(last_products) > 6:
                last_products.pop()
        self.request.session['last_products'] = last_products

        return product


class AddReviewView(CreateView):
    form_class = ReviewForm

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        form.instance.profile = self.request.user if not self.request.user.is_anonymous() else None
        super(AddReviewView, self).form_valid(form)
        return JsonResponse({'result': 'ok'})


class CompareProductsView(TemplateView):
    template_name = "catalog/compare.html"

    def get_context_data(self, **kwargs):
        compared_products = self.request.session.get('compared_products', {})
        compared_categories = []
        categories_ids = sorted(compared_products.keys())

        for category_id in categories_ids:
            try:
                category = Category.objects.get(id=int(category_id))
                products = []
                for product_id in compared_products[category_id]:
                    try:
                        product = Product.objects.get(id=product_id)
                        products.append(product)
                    except Product.DoesNotExist:
                        pass

                if len(products) == 2:
                    products.append(None)
                elif len(products) == 1:
                    products.extend([None, None])
                if len(products):
                    compared_categories.append((category, products))

            except (Category.DoesNotExist, ValueError) as e:
                pass

        context = {
            'compared_categories': compared_categories,
        }
        context.update(super(CompareProductsView, self).get_context_data(**kwargs))
        return context


class AddProductMixin(object):

    def get(self, request, *args, **kwargs):
        profile = request.user
        if profile.is_anonymous() or not profile.is_seller:
            return HttpResponseRedirect(reverse('index'))
        return super(AddProductMixin, self).get(request, *args, **kwargs)


class AddProductView(AddProductMixin, CreateView):
    model = Product
    form_class = AddProductForm
    template_name = 'catalog/wizard/add_product.html'

    def get_form(self, form_class=None):
        """
        https://jacobian.org/writing/dynamic-form-generation/
        """
        category_id = self.request.POST.get('category')
        extra_fields = Category.objects.get(id=category_id).attrs if category_id else []
        # extra_fields = Category.objects.get(id=category_id).get_attrs() if category_id else []
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.request.user, extra_fields, **self.get_form_kwargs())

    def form_valid(self, form):
        super(AddProductView, self).form_valid(form)
        return HttpResponseRedirect(reverse('shop:add-images', kwargs={'pk': form.instance.pk}))


class AddImagesView(AddProductMixin, TemplateView):
    template_name = 'catalog/wizard/add_images.html'
    errors = {}

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        context = {
            'product': self.get_product,
            'errors': self.errors,
        }
        context.update(super(AddImagesView, self).get_context_data(**kwargs))
        return context

    def post(self, request, *args, **kwargs):
        product = self.get_product()
        images = []
        errors = {}

        for i in xrange(1, 8):
            var_name = 'image%s' % i
            if var_name in request.FILES:
                image = request.FILES[var_name]
                if image.content_type not in ['image/png', 'image/jpeg', 'image/gif']:
                    errors[var_name] = 'Неверный формат файла'
                else:
                    images.append(image)

        if len(images) and not len(errors):
            for image in product.images.all():
                image.delete()
            for image in images:
                ProductImage.objects.create(product=product, image=image)

            send_new_product_email(request, product)
            product.admin_emailed_at_new = True
            product.save()
            return HttpResponseRedirect(reverse('shop:add-colors', args=[product.id]))

        if not len(errors):
            errors['image1'] = 'Нужно загрузить хотя бы одну картинку'

        self.errors = errors
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class AddColorsView(AddProductMixin, TemplateView):
    template_name = 'catalog/wizard/add_colors.html'

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        context = {
            'product': self.get_product,
        }
        context.update(super(AddColorsView, self).get_context_data(**kwargs))
        return context

    def post(self, request, *args, **kwargs):
        product = self.get_product()
        colors = []

        form_data = dict(request.POST)
        i = 0
        while 'color_name[%s]' % i in form_data and 'color[%s]' % i in form_data:
            if not form_data['color_name[%s]' % i][0]:
                continue
            if not form_data['color[%s]' % i][0]:
                continue
            colors.append({'name': form_data['color_name[%s]' % i][0], 'hex': form_data['color[%s]' % i][0]})
            del form_data['color_name[%s]' % i]
            del form_data['color[%s]' % i]
            i += 1

        if len(colors):
            for color in colors:
                ProductColor.objects.create(product=product, title=color['name'], hex_value=color['hex'])

        redirect_to = 'shop:add-related' if self.request.user.product_set.count() > 1 else 'shop:add-complete'
        return HttpResponseRedirect(reverse(redirect_to, args=[product.id]))


class AddRelatedProductsView(AddProductMixin, TemplateView):
    template_name = 'catalog/wizard/add_related.html'

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        context = {
            'product': self.get_product,
        }
        context.update(super(AddRelatedProductsView, self).get_context_data(**kwargs))
        return context

    def post(self, request, *args, **kwargs):
        seller_id = request.user.id
        product = self.get_product()
        product.clear_related_from_seller()

        related = request.POST.getlist('related')
        for p_id in related:
            p = Product.objects.filter(id=p_id, seller_id=seller_id).first()
            if p:
                product.related.add(p)
        return HttpResponseRedirect(reverse('shop:add-complete', args=[product.id]))


class AddCompleteView(AddProductMixin, TemplateView):
    template_name = 'catalog/wizard/complete.html'

    def get_product(self):
        return get_object_or_404(Product, pk=self.kwargs['pk'], seller=self.request.user)

    def get_context_data(self, **kwargs):
        context = {
            'product': self.get_product,
        }
        context.update(super(AddCompleteView, self).get_context_data(**kwargs))
        return context


class ProductRequestView(CreateView):
    form_class = ProductRequestForm

    def get_success_url(self):
        return self.request.path

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)

    def form_valid(self, form):
        super(ProductRequestView, self).form_valid(form)
        send_request_email(self.request, form.instance)
        data = {
            'errors': [],
            'result': 'ok',
        }
        return JsonResponse(data)
