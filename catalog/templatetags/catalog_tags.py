# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template

from catalog.models import Category


register = template.Library()


@register.filter
def get_field(form, key):
    return form[key]


@register.filter
def get_field_value(form, key):
    return form.cleaned_data.get(key)


@register.filter
def get_value(product, key):
    return product.attrs.get(key, '')


@register.simple_tag
def get_attr_value(product, key, attr_class):
    value = product.attrs.get(key, '')
    if attr_class == 'BooleanField':
        return '<span style="color: #7bae23;">Да</span>' if (value == 'True' or value == True) else '<span style="color: #e82c0c;">Нет</span>' if (value == 'False' or value == False) else value or ''
    return value or ''


@register.assignment_tag
def get_boolean_value(product, key):
    value = product.attrs.get(key, '')
    return True if value == 'True' else False if value == 'False' else value


@register.filter
def get_blank_from_new(category, new_key):
    old_field = category.objects.attrs[new_key.replace('-new')]
    return old_field['kwargs'].get('blank')

@register.filter
def remove_shit(verbose_name):
    return verbose_name.split(' (новое значение)')[0]


# @register.filter
# def get_new_value(product, key):
#     return product.attrs.get(key + '-new', '')


@register.filter
def get_field_errors(form, key):
    return form.errors.get(key)


@register.filter
def endswith(name, end):
    return name.endswith(end)


@register.simple_tag
def get_input_type(field_class):
    return {
        'CharField': 'text',
        'IntegerField': 'number',
        'FloatField': 'text',
        'DateField': 'text',
        'BooleanField': 'checkbox',
    }.get(field_class, 'text')


@register.assignment_tag
def get_category(category_id):
    return Category.objects.get(id=category_id)


@register.assignment_tag
def is_value_in_filter(attrs_filter, key, key_class, value):
    if key_class == 'BooleanField':
        value = 'True' if value == 'Да' else 'False' if value == 'Нет' else value
    # import ipdb
    # ipdb.set_trace()
    return True if attrs_filter.get(key) and value in attrs_filter[key] else False


@register.assignment_tag
def get_count(d, cat_id):
    return d.get(cat_id, 0)
