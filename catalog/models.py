# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.core.urlresolvers import reverse
from django.db import models

from django_pgjson.fields import JsonField
from django_hstore import hstore
from mptt.models import MPTTModel, TreeForeignKey

from lk.models import Profile
from site_settings.models import Setting, SEOSetting
from .utils import slugify


class Category(MPTTModel):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('В url', unique=True, max_length=255)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', verbose_name='Родитель')

    cover = models.ImageField('Обложка', null=True, blank=True, upload_to='catalog/categories/')
    order = models.IntegerField('Порядок', default=10)
    description = models.TextField('Краткое описание', blank=True, null=True)

    show = models.BooleanField('Показывать на сайте', default=True)
    use_home4 = models.BooleanField('Использовать шаблон Home4', default=False)

    # Сериализованные поля в фильтре
    attrs = JsonField(null=True, blank=True, editable=False)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = 'catalog'
        ordering = ['order', 'id', ]
        verbose_name = 'категория'
        verbose_name_plural = 'категории товаров'

    def get_absolute_url(self):
        return reverse('shop:category', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        self.update_json_attrs()
        return super(Category, self).save(*args, **kwargs)

    def update_json_attrs(self):
        attrs = []

        for attr in self.attribute_set.all():
            attr_dict = {
                'name': attr.slug,
                'class': attr.attr_type,
                'kwargs': {
                    'verbose_name': attr.title,
                }
            }
            new_attr_dict = {
                'name': attr.slug + '-new',
                'class': attr.attr_type,
                'kwargs': {
                    'verbose_name': attr.title + ' (новое значение)',
                }
            }
            if not attr.required:
                attr_dict['kwargs']['blank'] = True
            new_attr_dict['kwargs']['blank'] = True

            if attr.attr_type == 'CharField':
                attr_dict['kwargs']['max_length'] = 255
                new_attr_dict['kwargs']['max_length'] = 255
            attrs.append(attr_dict)
            attrs.append(new_attr_dict)

        self.attrs = attrs

    meta_title = models.CharField(
        'Meta title (заголовок страницы)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать поле "Заголовок"',
    )
    meta_description = models.CharField(
        'Meta description (описание страницы)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_desc',
    )
    meta_keywords = models.CharField(
        'Meta keywords (ключевые слова через запятую)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_keyw',
    )

    def get_meta_title(self):
        return self.meta_title if self.meta_title else '{0} — Каталог — Alazone'.format(self.title)

    def get_meta_desc(self):
        return self.meta_description if self.meta_description else SEOSetting.objects.get(key='global').meta_desc

    def get_meta_keyw(self):
        return self.meta_keywords if self.meta_keywords else SEOSetting.objects.get(key='global').meta_keyw

    @property
    def shown_children(self):
        return self.children.filter(show=True)

    @property
    def me_with_children(self):
        categories = [self.id]
        categories.extend(self.get_descendants().values_list('id', flat=True))
        return categories

    @property
    def color_set(self):
        colors = set(ProductColor.objects.filter(
            product__category_id__in=self.me_with_children, product__show=True
        ).values_list('hex_value', flat=True))
        return [color.split('#')[-1] for color in colors]

    @property
    def products(self):
        categories = [self.id]
        categories.extend(self.get_descendants().values_list('id', flat=True))
        return Product.objects.filter(category_id__in=self.me_with_children, show=True)

    def get_attrs(self):
        return [field for field in self.attrs if not field['name'].endswith('-new')]

    def get_new_attrs(self):
        return [field for field in self.attrs if field['name'].endswith('-new')]

    def get_attrs_with_values(self):
        products = self.products
        attrs = []
        for attr in self.get_attrs():
            a_name = attr['name']
            attr_dict = {
                'attr': attr,
                'values': ['Да', 'Нет'] if attr['class'] == 'BooleanField' else sorted(list(set(
                    [value for value in products.extra(
                        select={a_name: "catalog_product.attrs -> '{0}'".format(a_name)}
                    ).values_list(a_name, flat=True) if value]
                ))),
            }
            attrs.append(attr_dict)
        return attrs


class Attribute(models.Model):
    TYPE_CHOICES = (
        ('CharField', 'строка'),
        ('IntegerField', 'целое число'),
        ('FloatField', 'число с плавающей запятой'),
        ('DateField', 'дата'),
        ('BooleanField', 'логическая переменная ("да/нет")'),
    )
    category = models.ForeignKey(Category, verbose_name='Категория')
    title = models.CharField('Имя поля', max_length=255)
    slug = models.CharField(max_length=255, editable=False)
    attr_type = models.CharField('Тип поля', max_length=255, choices=TYPE_CHOICES)
    required = models.BooleanField('Обязательное поле?', default=False)
    # multiple_choices = models.BooleanField('Выбор из определенных вариантов', default=False)
    # choices = models.TextField('Варианты', null=True, blank=True,
    #                            help_text='Каждый вариант на новой строке; не забудьте проставить галочку сверху')
    # multiple_filter_choices = models.BooleanField('Фильтрация по нескольким', default=False)
    order = models.IntegerField('Порядок', default=10)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = 'catalog'
        ordering = ['order', 'id', ]
        unique_together = ('category', 'title',)
        unique_together = ('category', 'slug',)
        verbose_name = 'поле'
        verbose_name_plural = 'поля'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        save = super(Attribute, self).save(*args, **kwargs)
        self.category.update_json_attrs()
        return save


class ProductManager(models.Manager):

    def get_queryset(self):
        return super(ProductManager, self).get_queryset().filter(removed=False)


class Product(models.Model):
    DELIVERY_CHOICES = (
        ('regions', 'по регионам'),
        ('abroad', 'зарубеж'),
    )
    CONDITION_CHOICES = (
        ('new', 'новый'),
        ('used', 'б/у'),
    )
    category = models.ForeignKey(Category, verbose_name=u'Категория')
    seller = models.ForeignKey(Profile, verbose_name='Продавец', limit_choices_to={'profile_type': 'company'})

    # старые поля (отображаются на сайте)
    title = models.CharField('Название', max_length=255)
    price = models.DecimalField('Цена, тнг', max_digits=18, decimal_places=2, blank=True, null=True)
    new_price = models.DecimalField('Цена после скидки, тнг', max_digits=18, decimal_places=2, blank=True, null=True)
    quantity = models.SmallIntegerField('Количество', default=0)
    delivery_type = models.CharField('Тип доставки', max_length=15, choices=DELIVERY_CHOICES, default='regions')
    condition = models.CharField('Состояние товара', max_length=5, choices=CONDITION_CHOICES, default='new')
    description = models.TextField('Краткое описание', null=True, blank=True)

    # новые поля (не отображаются до подтверждения модератором)
    title_new = models.CharField('Название (новое значение)', max_length=255)
    price_new = models.DecimalField('Цена, тнг (новое значение)',
                                    max_digits=18, decimal_places=2, blank=True, null=True)
    new_price_new = models.DecimalField('Цена после скидки, тнг (новое значение)',
                                        max_digits=18, decimal_places=2, blank=True, null=True)
    quantity_new = models.SmallIntegerField('Количество (новое значение)', default=0)
    delivery_type_new = models.CharField('Тип доставки (новое значение)', max_length=15,
                                         choices=DELIVERY_CHOICES, default='regions')
    condition_new = models.CharField('Состояние товара (новое значение)', max_length=5,
                                     choices=CONDITION_CHOICES, default='new')
    description_new = models.TextField('Краткое описание (новое значение)', null=True, blank=True)

    attrs = hstore.DictionaryField(null=True, blank=True)  # db_index=True?

    show = models.BooleanField('Показывать на сайте', default=False,
                               help_text='Проставьте галку, чтобы опубликовать товар')
    updated = models.BooleanField('Товар был изменен', default=False, help_text='и ждет проверки')
    apply_changes = models.BooleanField('Товар проверен', default=True,
                                        help_text='Проставьте галку, чтобы применить последние изменения')
    removed = models.BooleanField('Удалить товар', default=False, help_text='Проставьте галку, чтобы удалить товар')

    at_homepage_catalog = models.BooleanField('Вынести на главную (в товары по категориям)', default=True)
    at_homepage_slider = models.BooleanField('Вынести на главную (в слайдер о новых/популярных товарах)', default=False)
    at_sidebar = models.BooleanField('Вынести в сайдбар (в продвигаемые товары)', default=False)
    related = models.ManyToManyField('self', verbose_name='Товары в блоке "с этим товаром также покупают"',
                                     related_name='in_related', blank=True)

    add_dt = models.DateTimeField('Время добавления', null=True, blank=True, auto_now_add=True, editable=False)
    publish_dt = models.DateTimeField('Время первой публикации', null=True, blank=True, editable=False)
    update_dt = models.DateTimeField('Время последнего обновления', null=True, blank=True, editable=False)

    admin_emailed_at_new = models.BooleanField(editable=False, default=True)

    objects = ProductManager()
    all_objects = models.Manager()
    # objects = hstore.Manager?

    class Meta:
        app_label = 'catalog'
        # ordering = ['-id']
        ordering = ['-show', '-update_dt', '-id']
        verbose_name = u'товар'
        verbose_name_plural = u'товары'

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.id:
            p = Product.objects.get(id=self.id)
            old = {
                'show': p.show,
                'updated': p.updated,
                'apply_changes': p.apply_changes,
                'publish_dt': p.publish_dt,
                # 'update_dt': p.update_dt,
            }

            # если публикуем:
            if self.show and not old['show']:
                if not old['publish_dt']:
                    now = datetime.datetime.now()
                    self.publish_dt = self.update_dt = now
                    # отправляем письмо юзеру (?)

            # если апдейтнулся:
            if self.updated and not old['updated']:
                self.apply_changes = False
                # отправляем письмо админу
                pass

            # если подтвердили апдейт:
            if self.apply_changes and not old['apply_changes']:
                self.update_dt = datetime.datetime.now()
                self.updated = False
                # отправляем письмо юзеру (?)

            if self.apply_changes:
                self.new_to_old()

            self.check_old()
            save = super(Product, self).save(*args, **kwargs)

        else:
            self.old_to_new()
            save = super(Product, self).save(*args, **kwargs)
        return save

    FIELDS_TO_UPDATE = ['title', 'price', 'new_price', 'quantity', 'delivery_type', 'condition', 'description', ]

    def check_old(self):
        for field in [attr['name'] for attr in self.category.get_attrs()]:
            # import ipdb; ipdb.set_trace()
            if self.attrs.get(field) is None:
                self.attrs[field] = self.attrs.get('{0}-new'.format(field))
        return "done; don't forget to save() the object"


    def new_to_old(self):
        """
        Сохраняем изменения у полей
        (юзать при проставленном self.apply_changes)
        """
        for field in self.FIELDS_TO_UPDATE:
            setattr(self, field, getattr(self, '{0}_new'.format(field)))
        # for field in self.attrs.keys():
        #     if not field.endswith('-new'):
        #         self.attrs[field] = self.attrs.get('{0}-new'.format(field))
        for field in [attr['name'] for attr in self.category.get_attrs()]:
            self.attrs[field] = self.attrs.get('{0}-new'.format(field))
        return "done; don't forget to save() the object"

    def old_to_new(self):
        """
        Копируем старые значения полей в новые
        (юзать при создании нового товара)
        """
        for field in self.FIELDS_TO_UPDATE:
            setattr(self, '{0}_new'.format(field), getattr(self, field))
        # for field in self.attrs.keys():
        #     if not field.endswith('-new'):
        #         self.attrs['{0}-new'.format(field)] = self.attrs.get(field)
        for field in [attr['name'] for attr in self.category.get_attrs()]:
            self.attrs['{0}-new'.format(field)] = self.attrs.get(field)
        return "done; don't forget to save() the object"

    def get_absolute_url(self):
        return reverse('shop:product', kwargs={'pk': self.pk})

    def get_profile_product_url(self):
        return reverse('profile:product', kwargs={'pk': self.id})

    def get_price(self):
        return self.new_price if self.new_price else self.price

    meta_title = models.CharField(
        'Meta title (заголовок страницы)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать поле "Заголовок"',
    )
    meta_description = models.CharField(
        'Meta description (описание страницы)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_desc',
    )
    meta_keywords = models.CharField(
        'Meta keywords (ключевые слова через запятую)',
        max_length=255, blank=True,
        help_text='Оставьте пустым, чтобы использовать глобальный meta_keyw',
    )

    def get_meta_title(self):
        return self.meta_title if self.meta_title else '{0} — Каталог — Alazone'.format(self.title)

    def get_meta_desc(self):
        return self.meta_description if self.meta_description else SEOSetting.objects.get(key='global').meta_desc

    def get_meta_keyw(self):
        return self.meta_keywords if self.meta_keywords else SEOSetting.objects.get(key='global').meta_keyw

    @property
    def reviews(self):
        return self.productreview_set.filter(show=True)

    @property
    def reviews_average(self):
        reviews = self.reviews.values_list('rating', flat=True)
        return int(round(sum(reviews)/float(len(reviews)) * 20)) if len(reviews) else 0

    def show_price(self):
        return int(self.price) if int(self.price) == self.price else self.price

    def show_new_price(self):
        return int(self.new_price) if int(self.new_price) == self.new_price else self.new_price

    def admin_show_new_price(self):
        return self.new_price or ''
    admin_show_new_price.allow_tags = True
    admin_show_new_price.short_description = 'Цена после скидки, тнг'

    def get_cover(self):
        # image = self.images.first()
        # return image.image if image else None
        return self.images.first()

    def get_backing(self):
        return self.images.all()[1] if self.images.count() > 1 else self.get_cover()

    def get_cover_sliced_url(self):
        return self.get_cover().image.url.replace('/media/', '') if self.get_cover() else ''

    def get_attr_value(self, attr_name, attr_class):
        try:
            value = self.attrs[attr_name]
            if attr_class == 'BooleanField':
                value = 'Да' if (value == 'True' or True) else 'Нет' if (value == 'False' or False) else value
        except KeyError:
            value = ''
        return value

    def show_attrs(self):
        attrs = [
            '<li><span>{0}:</span>{1}</li>'.format(attr['kwargs']['verbose_name'], self.get_attr_value(attr['name'], attr['class']))
            for attr in self.category.attrs if self.get_attr_value(attr['name'], attr['class']) and not attr['name'].endswith('-new')
        ]
        return attrs

    @property
    def is_new(self):
        """
        Определяем, является ли товар новым
        """
        try:
            days = int(Setting.objects.get(key='days_new').value)
        except (ValueError, Setting.DoesNotExist) as e:
            days = 7
        now = datetime.datetime.now()
        delta = datetime.timedelta(days=days)
        is_new = self.publish_dt > (now - delta) if self.publish_dt else False
        return is_new

    def get_discount(self):
        """
        Получаем скидку в процентах
        """
        return (int(round(100 - float(self.new_price/self.price*100)))
                if self.new_price and self.new_price < self.price else 0)

    @property
    def remove(self):
        """
        Обеспечиваем обратную совместимость
        """
        return self.removed

    def seller_another_products(self):
        return [p for p in self.seller.shown_products.order_by('?')[:10] if p != self]

    def seller_all_another_products(self):
        return [p for p in self.seller.product_set.order_by('-id') if p != self]

    def get_also_purchased(self):
        return [p for p in self.related.filter(show=True, removed=False).order_by('?')[:10] if p != self]

    def clear_related_from_seller(self):
        seller_id = self.seller_id
        for p_id, p_seller_id in self.related.values_list('id', 'seller_id'):
            if p_seller_id == seller_id:
                self.related.remove(p_id)
        return True


class ProductImage(models.Model):
    product = models.ForeignKey(Product, verbose_name='Товар', related_name='images')
    title = models.CharField('Заголовок', max_length=255, help_text='Для показа в атрибуте alt', blank=True)
    image = models.ImageField('Картинка', upload_to='catalog/products/')
    order = models.IntegerField('Порядок', default=10)

    class Meta:
        app_label = 'catalog'
        ordering = ['order', 'id', ]
        verbose_name = "изображение"
        verbose_name_plural = "изображения"

    def __unicode__(self):
        return self.title if self.title else '#{0} — {1}'.format(self.id, self.product.title)


class ProductColor(models.Model):
    product = models.ForeignKey(Product, verbose_name='Товар', related_name='colors')
    title = models.CharField('Название', max_length=255)
    hex_value = models.CharField('HEX-представление', max_length=7)
    order = models.IntegerField('Порядок', default=10)

    class Meta:
        app_label = 'catalog'
        ordering = ['order', 'id', ]
        verbose_name = "цвет"
        verbose_name_plural = "цвета"

    def __unicode__(self):
        return self.title


class ProductReview(models.Model):
    RATING_CHOICES = (
        (1, '★☆☆☆☆'),
        (2, '★★☆☆☆'),
        (3, '★★★☆☆'),
        (4, '★★★★☆'),
        (5, '★★★★★'),
    )
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)
    product = models.ForeignKey(Product, verbose_name='Товар')
    profile = models.ForeignKey(Profile, verbose_name='Профиль', null=True, blank=True)

    name = models.CharField('Имя', max_length=255)

    rating = models.PositiveSmallIntegerField('Оценка', choices=RATING_CHOICES)
    text = models.TextField('Текст отзыва')

    show = models.BooleanField('Опубликован?', default=True)

    class Meta:
        app_label = 'catalog'
        ordering = ['-datetime', ]
        verbose_name = 'отзыв'
        verbose_name_plural = 'отзывы к товарам'

    def __unicode__(self):
        return 'Отзыв от {0} ({1})'.format(self.name, self.datetime)

    def show_product(self):
        return self.product.title
    show_product.allow_tags = True
    show_product.short_description = 'Товар'

    @property
    def rating_percents(self):
        return self.rating * 20


class ProductSearchRequest(models.Model):
    profile = models.ForeignKey(Profile, verbose_name='Профиль', null=True, blank=True, related_name='product_requests')
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)

    request = models.CharField('Искомый товар', max_length=255)
    name = models.CharField('Имя', max_length=255)
    email = models.EmailField('Email', max_length=255)
    phone = models.CharField('Телефон', max_length=255)

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы поиска товара'

    def __unicode__(self):
        return '{0} ({1})'.format(self.name, self.datetime)
