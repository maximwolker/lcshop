# -*- coding: utf-8 -*-
from django.conf.urls import url
# from django.views.generic import RedirectView

from .views import (CatalogHomeView, CategoryView, SellerView, GetCategoryAttrs,
                    ProductView, AddReviewView, CompareProductsView,
                    AddProductView, AddImagesView, AddColorsView, AddRelatedProductsView, AddCompleteView,)


urlpatterns = [
    url(r'^$', CatalogHomeView.as_view(), name='home'),
    url(r'^category/(?P<slug>[^/]+)/$', CategoryView.as_view(), name='category'),
    url(r'^category/(?P<pk>\d+)/attrs/$', GetCategoryAttrs.as_view(), name='get-category-attrs'),

    url(r'^seller/(?P<seller_id>\d+)/$', SellerView.as_view(), name='seller'),
    url(r'^seller/(?P<seller_id>\d+)/(?P<category_slug>[^/]+)/$', SellerView.as_view(), name='seller-category'),

    url(r'^product/(?P<pk>\d+)/$', ProductView.as_view(), name='product'),
    url(r'^add_review/$', AddReviewView.as_view(), name='add-review'),

    url(r'^compare/$', CompareProductsView.as_view(), name='compare'),

    url(r'^add/$', AddProductView.as_view(), name='add'),
    url(r'^add/(?P<pk>\d+)/images/$', AddImagesView.as_view(), name='add-images'),
    url(r'^add/(?P<pk>\d+)/colors/$', AddColorsView.as_view(), name='add-colors'),
    url(r'^add/(?P<pk>\d+)/related/$', AddRelatedProductsView.as_view(), name='add-related'),
    url(r'^add/(?P<pk>\d+)/complete/$', AddCompleteView.as_view(), name='add-complete'),
]
