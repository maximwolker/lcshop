# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_auto_20150907_1332'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='add_dt',
            field=models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f', null=True),
        ),
        migrations.AddField(
            model_name='product',
            name='apply_changes',
            field=models.BooleanField(default=True, help_text='\u041f\u0440\u043e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u0433\u0430\u043b\u043a\u0443, \u0447\u0442\u043e\u0431\u044b \u043f\u0440\u0438\u043c\u0435\u043d\u0438\u0442\u044c \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0438\u0435 \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f', verbose_name='\u0422\u043e\u0432\u0430\u0440 \u043f\u0440\u043e\u0432\u0435\u0440\u0435\u043d'),
        ),
        migrations.AddField(
            model_name='product',
            name='publish_dt',
            field=models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u0435\u0440\u0432\u043e\u0439 \u043f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438', null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='update_dt',
            field=models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0433\u043e \u043e\u0431\u043d\u043e\u0432\u043b\u0435\u043d\u0438\u044f', null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='updated',
            field=models.BooleanField(default=False, help_text='\u0438 \u0436\u0434\u0435\u0442 \u043f\u0440\u043e\u0432\u0435\u0440\u043a\u0438', verbose_name='\u0422\u043e\u0432\u0430\u0440 \u0431\u044b\u043b \u0438\u0437\u043c\u0435\u043d\u0435\u043d'),
        ),
        migrations.AlterField(
            model_name='product',
            name='remove',
            field=models.BooleanField(default=False, help_text='\u041f\u0440\u043e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u0433\u0430\u043b\u043a\u0443, \u0447\u0442\u043e\u0431\u044b \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0442\u043e\u0432\u0430\u0440', verbose_name='\u0423\u0434\u0430\u043b\u0438\u0442\u044c \u0442\u043e\u0432\u0430\u0440'),
        ),
        migrations.AlterField(
            model_name='product',
            name='show',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435'),
        ),
    ]
