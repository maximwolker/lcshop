# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0013_auto_20150909_1123'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='remove',
            new_name='removed',
        ),
    ]
