# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0011_auto_20150908_1343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productreview',
            name='rating',
            field=models.PositiveSmallIntegerField(verbose_name='\u041e\u0446\u0435\u043d\u043a\u0430', choices=[(1, '\u2605\u2606\u2606\u2606\u2606'), (2, '\u2605\u2605\u2606\u2606\u2606'), (3, '\u2605\u2605\u2605\u2606\u2606'), (4, '\u2605\u2605\u2605\u2605\u2606'), (5, '\u2605\u2605\u2605\u2605\u2605')]),
        ),
    ]
