# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0009_auto_20150908_0707'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='attrs_new',
        ),
        migrations.RemoveField(
            model_name='product',
            name='attrs_new',
        ),
    ]
