# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0020_product_also_purchased'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='also_purchased',
        ),
        migrations.AddField(
            model_name='product',
            name='related',
            field=models.ManyToManyField(related_name='related_rel_+', verbose_name='\u0422\u043e\u0432\u0430\u0440\u044b \u0432 \u0431\u043b\u043e\u043a\u0435 "\u0441 \u044d\u0442\u0438\u043c \u0442\u043e\u0432\u0430\u0440\u043e\u043c \u0442\u0430\u043a\u0436\u0435 \u043f\u043e\u043a\u0443\u043f\u0430\u044e\u0442"', to='catalog.Product', blank=True),
        ),
    ]
