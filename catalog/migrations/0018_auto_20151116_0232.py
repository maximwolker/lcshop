# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0017_productsearchrequest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='new_price',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430 \u043f\u043e\u0441\u043b\u0435 \u0441\u043a\u0438\u0434\u043a\u0438, \u0442\u043d\u0433', max_digits=18, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='new_price_new',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430 \u043f\u043e\u0441\u043b\u0435 \u0441\u043a\u0438\u0434\u043a\u0438, \u0442\u043d\u0433 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', max_digits=18, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430, \u0442\u043d\u0433', max_digits=18, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='price_new',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430, \u0442\u043d\u0433 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', max_digits=18, decimal_places=2, blank=True),
        ),
    ]
