# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0014_auto_20150909_1403'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='admin_emailed_at_new',
            field=models.BooleanField(default=True, editable=False),
        ),
    ]
