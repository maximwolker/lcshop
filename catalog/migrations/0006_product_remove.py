# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0005_auto_20150825_1152'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='remove',
            field=models.BooleanField(default=False, verbose_name='\u0423\u0434\u0430\u043b\u0438\u0442\u044c \u0442\u043e\u0432\u0430\u0440'),
        ),
    ]
