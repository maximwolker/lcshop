# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_pgjson.fields
import django_hstore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_auto_20150908_0614'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='attrs_new',
            field=django_pgjson.fields.JsonField(null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='attrs_new',
            field=django_hstore.fields.DictionaryField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='condition_new',
            field=models.CharField(default='new', max_length=5, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', choices=[('new', '\u043d\u043e\u0432\u044b\u0439'), ('used', '\u0431/\u0443')]),
        ),
        migrations.AddField(
            model_name='product',
            name='delivery_type_new',
            field=models.CharField(default='regions', max_length=15, verbose_name='\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', choices=[('regions', '\u043f\u043e \u0440\u0435\u0433\u0438\u043e\u043d\u0430\u043c'), ('abroad', '\u0437\u0430\u0440\u0443\u0431\u0435\u0436')]),
        ),
        migrations.AddField(
            model_name='product',
            name='description_new',
            field=models.TextField(null=True, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='new_price_new',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430 \u043f\u043e\u0441\u043b\u0435 \u0441\u043a\u0438\u0434\u043a\u0438, \u0442\u0433 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', max_digits=18, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='price_new',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430, \u0442\u0433 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)', max_digits=18, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='product',
            name='quantity_new',
            field=models.SmallIntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)'),
        ),
        migrations.AddField(
            model_name='product',
            name='title_new',
            field=models.CharField(default='-', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 (\u043d\u043e\u0432\u043e\u0435 \u0437\u043d\u0430\u0447\u0435\u043d\u0438\u0435)'),
            preserve_default=False,
        ),
    ]
