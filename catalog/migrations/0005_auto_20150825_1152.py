# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0004_auto_20150825_0503'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productcolor',
            options={'ordering': ['order', 'id'], 'verbose_name': '\u0446\u0432\u0435\u0442', 'verbose_name_plural': '\u0446\u0432\u0435\u0442\u0430'},
        ),
        migrations.AlterModelOptions(
            name='productimage',
            options={'ordering': ['order', 'id'], 'verbose_name': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'},
        ),
    ]
