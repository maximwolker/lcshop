# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_pgjson.fields
import mptt.fields
from django.conf import settings
import django_hstore.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f \u043f\u043e\u043b\u044f')),
                ('slug', models.CharField(max_length=255, editable=False)),
                ('attr_type', models.CharField(max_length=255, verbose_name='\u0422\u0438\u043f \u043f\u043e\u043b\u044f', choices=[('CharField', '\u0441\u0442\u0440\u043e\u043a\u0430'), ('IntegerField', '\u0446\u0435\u043b\u043e\u0435 \u0447\u0438\u0441\u043b\u043e'), ('FloatField', '\u0447\u0438\u0441\u043b\u043e \u0441 \u043f\u043b\u0430\u0432\u0430\u044e\u0449\u0435\u0439 \u0437\u0430\u043f\u044f\u0442\u043e\u0439'), ('DateField', '\u0434\u0430\u0442\u0430'), ('BooleanField', '\u043b\u043e\u0433\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u043f\u0435\u0440\u0435\u043c\u0435\u043d\u043d\u0430\u044f ("\u0434\u0430/\u043d\u0435\u0442")')])),
                ('required', models.BooleanField(default=False, verbose_name='\u041e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u043d\u043e\u0435 \u043f\u043e\u043b\u0435?')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
            ],
            options={
                'ordering': ['order', 'id'],
                'verbose_name': '\u043f\u043e\u043b\u0435',
                'verbose_name_plural': '\u043f\u043e\u043b\u044f',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slug', models.SlugField(unique=True, max_length=255, verbose_name='\u0412 url')),
                ('cover', models.ImageField(upload_to='catalog/categories/', null=True, verbose_name='\u041e\u0431\u043b\u043e\u0436\u043a\u0430', blank=True)),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('description', models.TextField(null=True, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('use_home4', models.BooleanField(default=False, verbose_name='\u0418\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0448\u0430\u0431\u043b\u043e\u043d Home4')),
                ('json_attrs', django_pgjson.fields.JsonField(null=True, editable=False, blank=True)),
                ('meta_title', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043b\u0435 "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a"', max_length=255, verbose_name='Meta title (\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_description', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_desc', max_length=255, verbose_name='Meta description (\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_keywords', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_keyw', max_length=255, verbose_name='Meta keywords (\u043a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e)', blank=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c', blank=True, to='catalog.Category', null=True)),
            ],
            options={
                'ordering': ['order', 'id'],
                'verbose_name': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f',
                'verbose_name_plural': '\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438 \u0442\u043e\u0432\u0430\u0440\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('meta_title', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u043b\u0435 "\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a"', max_length=255, verbose_name='Meta title (\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_description', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_desc', max_length=255, verbose_name='Meta description (\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b)', blank=True)),
                ('meta_keywords', models.CharField(help_text='\u041e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u043f\u0443\u0441\u0442\u044b\u043c, \u0447\u0442\u043e\u0431\u044b \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0433\u043b\u043e\u0431\u0430\u043b\u044c\u043d\u044b\u0439 meta_keyw', max_length=255, verbose_name='Meta keywords (\u043a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0441\u043b\u043e\u0432\u0430 \u0447\u0435\u0440\u0435\u0437 \u0437\u0430\u043f\u044f\u0442\u0443\u044e)', blank=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('price', models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430, \u0442\u0433', max_digits=18, decimal_places=2)),
                ('new_price', models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430 \u043f\u043e\u0441\u043b\u0435 \u0441\u043a\u0438\u0434\u043a\u0438, \u0442\u0433', max_digits=18, decimal_places=2, blank=True)),
                ('quantity', models.SmallIntegerField(default=0, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('delivery_type', models.CharField(default='regions', max_length=15, verbose_name='\u0422\u0438\u043f \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', choices=[('regions', '\u043f\u043e \u0440\u0435\u0433\u0438\u043e\u043d\u0430\u043c'), ('abroad', '\u0437\u0430\u0440\u0443\u0431\u0435\u0436')])),
                ('condition', models.CharField(default='new', max_length=5, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435 \u0442\u043e\u0432\u0430\u0440\u0430', choices=[('new', '\u043d\u043e\u0432\u044b\u0439'), ('used', '\u0431/\u0443')])),
                ('description', models.TextField(null=True, verbose_name='\u041a\u0440\u0430\u0442\u043a\u043e\u0435 \u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('show', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
                ('at_homepage_catalog', models.BooleanField(default=True, verbose_name='\u0412\u044b\u043d\u0435\u0441\u0442\u0438 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u0443\u044e (\u0432 \u0442\u043e\u0432\u0430\u0440\u044b \u043f\u043e \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f\u043c)')),
                ('at_homepage_slider', models.BooleanField(default=False, verbose_name='\u0412\u044b\u043d\u0435\u0441\u0442\u0438 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u0443\u044e (\u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440 \u043e \u0440\u0430\u0441\u043f\u0440\u043e\u0434\u0430\u0436\u0430\u0445)')),
                ('attrs', django_hstore.fields.DictionaryField(null=True, blank=True)),
                ('category', models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category')),
                ('seller', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0434\u0430\u0432\u0435\u0446', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-id'],
                'verbose_name': '\u0442\u043e\u0432\u0430\u0440',
                'verbose_name_plural': '\u0442\u043e\u0432\u0430\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='ProductColor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('hex_value', models.CharField(max_length=7, verbose_name='HEX-\u043f\u0440\u0435\u0434\u0441\u0442\u0430\u0432\u043b\u0435\u043d\u0438\u0435')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('product', models.ForeignKey(related_name='colors', verbose_name='\u0422\u043e\u0432\u0430\u0440', to='catalog.Product')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': '\u0446\u0432\u0435\u0442',
                'verbose_name_plural': '\u0446\u0432\u0435\u0442\u0430',
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text='\u0414\u043b\u044f \u043f\u043e\u043a\u0430\u0437\u0430 \u0432 \u0430\u0442\u0440\u0438\u0431\u0443\u0442\u0435 alt', max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a', blank=True)),
                ('picture', models.ImageField(upload_to='catalog/products/', verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430')),
                ('order', models.IntegerField(default=10, verbose_name='\u041f\u043e\u0440\u044f\u0434\u043e\u043a')),
                ('product', models.ForeignKey(related_name='images', verbose_name='\u0422\u043e\u0432\u0430\u0440', to='catalog.Product')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='ProductReview',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('rating', models.PositiveSmallIntegerField(verbose_name='\u041e\u0446\u0435\u043d\u043a\u0430', choices=[(1, '\u2605\u2606\u2606\u2606\u2606'), (2, '\u2605\u2605\u2606\u2606\u2606'), (3, '\u2605\u2605\u2605\u2606\u2606'), (4, '\u2605\u2605\u2605\u2605\u2606'), (5, '\u2605\u2605\u2605\u2605\u2605')])),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043e\u0442\u0437\u044b\u0432\u0430')),
                ('show', models.BooleanField(default=True, verbose_name='\u041e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u043d?')),
                ('product', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='catalog.Product')),
                ('profile', models.ForeignKey(verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u043e\u0442\u0437\u044b\u0432',
                'verbose_name_plural': '\u043e\u0442\u0437\u044b\u0432\u044b \u043a \u0442\u043e\u0432\u0430\u0440\u0430\u043c',
            },
        ),
        migrations.AddField(
            model_name='attribute',
            name='category',
            field=models.ForeignKey(verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f', to='catalog.Category'),
        ),
        migrations.AlterUniqueTogether(
            name='attribute',
            unique_together=set([('category', 'slug')]),
        ),
    ]
