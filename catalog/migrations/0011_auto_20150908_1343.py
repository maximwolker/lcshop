# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0010_auto_20150908_0720'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['-update_dt', '-id'], 'verbose_name': '\u0442\u043e\u0432\u0430\u0440', 'verbose_name_plural': '\u0442\u043e\u0432\u0430\u0440\u044b'},
        ),
        migrations.AlterField(
            model_name='product',
            name='show',
            field=models.BooleanField(default=False, help_text='\u041f\u0440\u043e\u0441\u0442\u0430\u0432\u044c\u0442\u0435 \u0433\u0430\u043b\u043a\u0443, \u0447\u0442\u043e\u0431\u044b \u043e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c \u0442\u043e\u0432\u0430\u0440', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435'),
        ),
        migrations.AlterField(
            model_name='productreview',
            name='rating',
            field=models.PositiveSmallIntegerField(verbose_name='\u041e\u0446\u0435\u043d\u043a\u0430', choices=[(1, '?????'), (2, '?????'), (3, '?????'), (4, '?????'), (5, '?????')]),
        ),
    ]
