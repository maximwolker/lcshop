# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0012_auto_20150908_1559'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='at_homepage_slider',
            field=models.BooleanField(default=False, verbose_name='\u0412\u044b\u043d\u0435\u0441\u0442\u0438 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u0443\u044e (\u0432 \u0441\u043b\u0430\u0439\u0434\u0435\u0440 \u043e \u043d\u043e\u0432\u044b\u0445/\u043f\u043e\u043f\u0443\u043b\u044f\u0440\u043d\u044b\u0445 \u0442\u043e\u0432\u0430\u0440\u0430\u0445)'),
        ),
    ]
