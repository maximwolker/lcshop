# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20150823_2322'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productimage',
            old_name='picture',
            new_name='image',
        ),
    ]
