# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(null=True, verbose_name='\u0426\u0435\u043d\u0430, \u0442\u0433', max_digits=18, decimal_places=2, blank=True),
        ),
    ]
