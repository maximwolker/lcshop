# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0018_auto_20151116_0232'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='at_sidebar',
            field=models.BooleanField(default=False, verbose_name='\u0412\u044b\u043d\u0435\u0441\u0442\u0438 \u0432 \u0441\u0430\u0439\u0434\u0431\u0430\u0440 (\u0432 \u043f\u0440\u043e\u0434\u0432\u0438\u0433\u0430\u0435\u043c\u044b\u0435 \u0442\u043e\u0432\u0430\u0440\u044b)'),
        ),
    ]
