# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_auto_20150817_1138'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='json_attrs',
            new_name='attrs',
        ),
    ]
