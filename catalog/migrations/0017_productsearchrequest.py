# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('catalog', '0016_auto_20151001_0516'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductSearchRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u043f\u043e\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f')),
                ('request', models.CharField(max_length=255, verbose_name='\u0418\u0441\u043a\u043e\u043c\u044b\u0439 \u0442\u043e\u0432\u0430\u0440')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
                ('email', models.EmailField(max_length=255, verbose_name='Email')),
                ('phone', models.CharField(max_length=255, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('profile', models.ForeignKey(related_name='product_requests', verbose_name='\u041f\u0440\u043e\u0444\u0438\u043b\u044c', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-datetime'],
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b \u043f\u043e\u0438\u0441\u043a\u0430 \u0442\u043e\u0432\u0430\u0440\u0430',
            },
        ),
    ]
