# -*- coding: utf-8 -*-
from unidecode import unidecode

from django.utils.text import slugify as _slugify


def slugify(string):
    return _slugify(unicode(unidecode(string)))
