# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import copy

from django.contrib import admin

from mptt.admin import MPTTModelAdmin

from .admin_forms import DifferentAddChangeAdminMixin, ProductAddForm, ProductChangeForm
from .models import Category, Attribute, Product, ProductImage, ProductColor, ProductReview, ProductSearchRequest


class AttributeInline(admin.TabularInline):
    fields = ('title', 'attr_type', 'required', 'order',)
    template = "admin/catalog/attribute_tabular.html"
    model = Attribute
    extra = 0


@admin.register(Category)
class CategoryAdmin(MPTTModelAdmin):
    list_display = ('title', 'slug', 'parent', 'order', 'show', 'use_home4',)
    list_editable = ('slug', 'order',)
    list_filter = ('show', 'use_home4',)

    fieldsets = (
        (None, {
            'fields': (('title', 'slug'), 'parent', 'cover', 'order', ('show', 'use_home4',), 'description',)
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('meta_title', 'meta_description', 'meta_keywords',)
        }),
    )
    search_fields = ['title', 'slug', ]
    inlines = [AttributeInline, ]
    prepopulated_fields = {"slug": ("title",)}

    # class Media:
    #     js = (
    #         'grappelli/jquery/jquery-1.9.1.min.js',
    #         'admin/js/catalog/override-child-attributes.js',
    #     )

    def save_model(self, request, obj, form, change):
        """
        http://pytalk.ru/forum/django/24634/
        """
        obj.save()
        self.savedObj = obj

    def save_formset(self, request, form, formset, change):
        """
        http://pytalk.ru/forum/django/24634/
        """
        formset.save()
        self.savedObj.save()


class ProductImageInline(admin.TabularInline):
    fields = ('title', 'image', 'order',)
    model = ProductImage
    extra = 1


class ProductColorInline(admin.TabularInline):
    fields = ('title', 'hex_value',)
    model = ProductColor
    extra = 1


class ProductReviewInline(admin.TabularInline):
    model = ProductReview
    fields = ('datetime', 'name', 'rating', 'show',)
    readonly_fields = ('datetime', 'name', 'rating',)
    extra = 0

    def has_add_permission(self, request):
        return None


@admin.register(Product)
class ProductAdmin(DifferentAddChangeAdminMixin):
    list_display = ('id', 'title', 'category', 'seller', 'price', 'admin_show_new_price',
                    'show', 'updated', 'apply_changes',)
    list_display_links = ('id', 'title',)
    list_filter = ('category', 'seller', 'show', 'updated', 'apply_changes',
                   'at_homepage_catalog', 'at_homepage_slider', 'at_sidebar',)
    list_per_page = 200
    ordering = ('-id',)

    add_form = ProductAddForm
    form = ProductChangeForm
    add_fieldsets = ((None, {'classes': ('wide',), 'fields': ('title', 'category', 'seller', 'price',)}),)
    fieldsets = (
        (None, {
            'fields': (('title', 'title_new',), 'category', 'seller',)
        }),
        ('Общие поля', {
            'fields': (
                ('price', 'price_new',),
                ('new_price', 'new_price_new',),
                ('quantity', 'quantity_new',),
                ('delivery_type', 'delivery_type_new',),
                ('condition', 'condition_new',),
                ('description', 'description_new',),
            )
        }),
        ('Дополнительные поля', {
            'fields': ('',)
        }),
        ('Параметры отображения', {
            # 'classes': ('grp-collapse grp-closed',),
            'fields': ('show', 'at_homepage_catalog', 'at_homepage_slider', 'at_sidebar', 'related',
                       'updated', 'apply_changes', 'removed',),
        }),
        (None, {
            'fields': ('add_dt', 'publish_dt', 'update_dt',),
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('meta_title', 'meta_description', 'meta_keywords',)
        }),
    )
    inlines = [ProductImageInline, ProductColorInline, ProductReviewInline, ]
    search_fields = ['title', 'description', ]
    readonly_fields = ['updated', 'add_dt', 'publish_dt', 'update_dt', ]
    readonly_fields += ['title', 'price', 'new_price', 'quantity', 'delivery_type', 'condition', 'description', ]
    filter_horizontal = ['related', ]

    def get_fieldsets(self, request, obj=None):
        fieldsets = copy.deepcopy(super(ProductAdmin, self).get_fieldsets(request, obj))
        if obj and obj.category:
            attrs_field = Product._meta.get_field('attrs')
            attrs_field.reload_schema(obj.category.attrs)
            fieldsets[2][1]['fields'] = tuple(
                (field['name'], '{0}-new'.format(field['name'])) for field in obj.category.get_attrs()
            )
        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return ()
        readonly_fields = copy.deepcopy(super(ProductAdmin, self).get_readonly_fields(request, obj))
        return readonly_fields + [field['name'] for field in obj.category.get_attrs()]
        # return super(ProductAdmin, self).get_readonly_fields(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        if obj and obj.category:
            field = Product._meta.get_field('attrs')
            field.reload_schema(obj.category.attrs)
        return super(ProductAdmin, self).get_form(request, obj, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(ProductReview)
class ProductReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'datetime', 'show_product', 'rating', 'show',)
    list_display_links = ('id', 'name',)
    list_filter = ('rating', 'show',)
    list_per_page = 200

    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), ('product', 'profile'), 'show',)
        }),
        ('Данные из формы', {
            'fields': ('name', 'rating', 'text',)
        }),
    )
    search_fields = ['name', 'product__title', 'text', ]
    readonly_fields = ('id', 'datetime',)


@admin.register(ProductSearchRequest)
class ProductSearchRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'request', 'email', 'name', 'phone',)
    list_display_links = ('id', 'datetime',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), 'profile',)
        }),
        ('Данные из формы', {
            'fields': ('request', 'name', 'phone', 'email',)
        }),
    )
    search_fields = ['request', 'name', 'phone', 'email', ]
    readonly_fields = ('id', 'datetime', 'profile', 'request', 'name', 'phone', 'email',)
    # readonly_fields = ('id', 'datetime',)
