# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from .models import Product, ProductReview, ProductSearchRequest


class ProductRequestForm(forms.ModelForm):

    class Meta:
        model = ProductSearchRequest
        fields = ('profile', 'request', 'name', 'email', 'phone',)


class AddProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = ['category', 'title', 'price', 'new_price', 'quantity', 'delivery_type', 'condition', 'description', 'meta_title', 'meta_description', 'meta_keywords',]

    def __init__(self, profile, extra_fields, *args, **kwargs):
        """
        https://jacobian.org/writing/dynamic-form-generation/
        """
        super(AddProductForm, self).__init__(*args, **kwargs)
        self.profile = profile

        for field in extra_fields:
            # print field['name']
            extra_field = forms.__getattribute__(field['class'])(label=field['kwargs']['verbose_name'])
            if field['kwargs'].get('blank'):
                extra_field.required = False
            if field['kwargs'].get('max_length'):
                extra_field.max_length = field['kwargs'].get('max_length')
            self.fields[field['name']] = extra_field

        for _, field in self.fields.iteritems():
            field.widget.attrs.update({'class': 'form-control input-lg'})

    def clean(self):
        category = self.cleaned_data.get('category')
        attrs_field = Product._meta.get_field('attrs')
        attrs_field.reload_schema(category.attrs)
        # attrs_field.reload_schema(category.get_attrs())
        return super(AddProductForm, self).clean()

    def save(self, commit=True):
        instance = super(AddProductForm, self).save(commit=False)
        for field in instance.category.attrs:
            setattr(instance, field['name'], self.cleaned_data.get(field['name']))

        instance.seller = self.profile
        instance.show = False
        instance.at_homepage_catalog = False
        instance.at_homepage_slider = False
        instance.admin_emailed_at_new = False
        if commit:
            instance.save()
        return instance


class EditProductForm(AddProductForm):

    def __init__(self, profile, extra_fields, is_product_shown, old_category, old_attrs, *args, **kwargs):
        """
        https://jacobian.org/writing/dynamic-form-generation/
        """
        super(EditProductForm, self).__init__(profile, extra_fields, *args, **kwargs)
        self.is_product_shown = is_product_shown
        self.old_category = old_category
        self.old_attrs = old_attrs
        self.fields['category'].widget.attrs.update({'readonly': 'readonly', 'class': 'no-pe form-control input-lg'})

    class Meta:
        model = Product
        fields = ['category', 'article', 'title_new', 'price_new', 'new_price_new', 'quantity_new',
                  'delivery_type_new', 'condition_new', 'description_new', 'meta_title', 'meta_description', 'meta_keywords']

    def save(self, commit=True):
        instance = super(EditProductForm, self).save(commit=False)
        old_product = Product.objects.get(id=instance.id)

        instance.attrs = self.old_attrs
        for field in instance.category.get_new_attrs():
            instance.attrs[field['name']] = self.cleaned_data.get(field['name'])

        if instance.category != self.old_category:
            instance.show = False
            category_changed = True
        else:
            instance.show = self.is_product_shown
            category_changed = False

        changed_fields = []
        for key, field in self.fields.iteritems():
            new_value = self.cleaned_data.get(key)
            old_value = getattr(old_product, key)
            if new_value != old_value:
                changed_fields.append(field.label.split(' (новое значение)')[0])

        was_product_updated_before = instance.updated
        if len(changed_fields):
            instance.updated = True

        if commit:
            instance.save()

        return instance, category_changed, was_product_updated_before, changed_fields


class ReviewForm(forms.ModelForm):

    class Meta:
        model = ProductReview
        fields = ['product', 'profile', 'name', 'rating', 'text', ]
