# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class GEOConfig(AppConfig):
    name = 'geo'
    verbose_name = "Страны и регионы"
