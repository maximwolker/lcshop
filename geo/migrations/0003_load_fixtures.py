# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def load_fixture(apps, schema_editor):
    from django.core.management import call_command
    call_command('loaddata', 'countries_20150909.json')
    call_command('loaddata', 'regions_20150909.json')


def empty_migration(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0002_auto_20150908_1559'),
    ]

    operations = [
    ]

    operations = [
        migrations.RunPython(load_fixture, reverse_code=empty_migration),
    ]
