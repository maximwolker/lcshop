# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('geo', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='country',
            name='show',
        ),
        migrations.RemoveField(
            model_name='region',
            name='show',
        ),
    ]
