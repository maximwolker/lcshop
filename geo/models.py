# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    title = models.CharField('Название', max_length=255)
    order = models.IntegerField('Порядок', default=10)

    class Meta:
        ordering = ['order', 'title', ]
        verbose_name = _('страна')
        verbose_name_plural = _('страны')

    def __unicode__(self):
        return self.title

    @property
    def regions(self):
        return self.region_set.filter(show=True)


class Region(models.Model):
    country = models.ForeignKey(Country, verbose_name='Страна')
    title = models.CharField('Название', max_length=255)
    order = models.IntegerField('Порядок', default=10)

    class Meta:
        ordering = ['country', 'order', 'id', ]
        verbose_name = 'регион'
        verbose_name_plural = 'регионы'

    def __unicode__(self):
        return self.title
