# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Country, Region


class RegionInline(admin.TabularInline):
    model = Region
    fields = ('title', 'order',)
    extra = 3


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('title', 'order',)
    list_editable = ('order',)
    inlines = [RegionInline, ]


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('title', 'country', 'order',)
    list_filter = ('country',)
    list_editable = ('order',)
