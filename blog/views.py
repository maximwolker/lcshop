# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404, JsonResponse
from django.views.generic import DetailView, ListView, CreateView, View
from django.shortcuts import get_object_or_404

from pure_pagination.mixins import PaginationMixin

from .forms import CommentForm
from .models import Category, Tag, Post


class HomeView(PaginationMixin, ListView):
    template_name = "blog/home.html"
    model = Post
    context_object_name = 'post_list'
    paginate_by = 4

    def get(self, request, *args, **kwargs):
        tag = request.GET.get('tag')
        self.tag = get_object_or_404(Tag, slug=tag) if tag else ''
        return super(HomeView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Post.objects.posted()
        return (queryset if not self.tag
                else queryset.filter(tags=self.tag))

    def get_context_data(self, **kwargs):
        context = {
            'tag': self.tag,
        }
        context.update(super(HomeView, self).get_context_data(**kwargs))
        return context


class CategoryView(PaginationMixin, ListView):
    template_name = "blog/category.html"
    model = Post
    context_object_name = 'post_list'
    paginate_by = 4

    def get(self, request, *args, **kwargs):
        tag = request.GET.get('tag')
        self.tag = get_object_or_404(Tag, slug=tag) if tag else ''
        return super(CategoryView, self).get(request, *args, **kwargs)

    def get_category(self):
        self.category = get_object_or_404(Category, slug=self.kwargs['slug'])
        return self.category

    def get_queryset(self):
        queryset = self.get_category().posts
        return (queryset if not self.tag
                else queryset.filter(tags=self.tag))

    def get_context_data(self, **kwargs):
        context = {
            'category': self.category,
            'tag': self.tag,
        }
        context.update(super(CategoryView, self).get_context_data(**kwargs))
        return context


class PostView(DetailView):
    template_name = "blog/post.html"
    model = Post
    context_object_name = 'post'

    def get(self, request, *args, **kwargs):
        post = self.get_object()
        post.views += 1
        post.save()
        return super(PostView, self).get(request, *args, **kwargs)

    def get_object(self, queryset=None):
        self.post = super(PostView, self).get_object(queryset)
        if self.kwargs.get('category') != self.post.category.slug:
            raise Http404
        return self.post

    def get_context_data(self, **kwargs):
        context = {
            'category': self.post.category,
        }
        context.update(super(PostView, self).get_context_data(**kwargs))
        return context


class AddCommentView(CreateView):
    form_class = CommentForm

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        super(AddCommentView, self).form_valid(form)
        data = {
            'errors': [],
            'result': 'ok',
            'comment_id': form.instance.id,
        }
        return JsonResponse(data)

    def form_invalid(self, form):
        errors = []
        for k in form.errors:
            errors.append({'name': k, 'error': form.errors[k][0]})
        data = {
            'errors': errors,
            'result': 'error',
        }
        return JsonResponse(data)


class AddLikeView(View):

    def post(self, request, *args, **kwargs):
        try:
            post_id = request.POST['post_id']
            post = Post.objects.get(id=post_id)
            liked_posts = request.session.get('liked_posts', [])

            if not post_id in liked_posts:
                post.likes += 1
                post.save()
                liked_posts.append(post_id)
                request.session['liked_posts'] = liked_posts

            return JsonResponse({'result': 'ok', 'count': post.likes})

        except (ValueError, KeyError, Post.DoesNotExist) as e:
            return JsonResponse({'result': 'ok', 'count': post.likes})


class RemoveLikeView(View):

    def post(self, request, *args, **kwargs):
        try:
            post_id = request.POST['post_id']
            post = Post.objects.get(id=post_id)
            liked_posts = request.session.get('liked_posts', [])

            if post_id in liked_posts:
                post.likes -= 1
                post.save()
                liked_posts.pop(liked_posts.index(post_id))
                request.session['liked_posts'] = liked_posts

            return JsonResponse({'result': 'ok', 'count': post.likes})

        except (ValueError, KeyError, Post.DoesNotExist) as e:
            return JsonResponse({'result': 'ok', 'count': post.likes})
