# -*- coding: utf-8 -*-
from django.conf.urls import url

from .views import HomeView, CategoryView, PostView, AddCommentView, AddLikeView, RemoveLikeView


urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),

    url(r'^add_comment/$', AddCommentView.as_view(), name='add-comment'),
    url(r'^like/add/$', AddLikeView.as_view(), name='add-like'),
    url(r'^like/remove/$', RemoveLikeView.as_view(), name='remove-like'),

    url(r'^(?P<slug>[^/]+)/$', CategoryView.as_view(), name='category'),
    url(r'^(?P<category>[^/]+)/(?P<pk>\d+)/$', PostView.as_view(), name='post'),
]
