# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.contrib import admin

from site_settings.admin import MetatagModelAdmin
from .models import Category, Tag, Post, Testimonial, Comment


@MetatagModelAdmin
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'order', 'show',)
    list_editable = ('order',)
    list_filter = ('show',)
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'order', 'show',)
        }),
    )
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ['title', 'slug', ]


class TagAdminForm(forms.ModelForm):

    class Meta:
        model = Tag
        fields = '__all__'

    def clean_title(self):
        title = self.cleaned_data.get('title')
        if title.count(' '):
            raise forms.ValidationError('Заголовок не должен содержать пробелов.')
        return title


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug',)
    form = TagAdminForm
    prepopulated_fields = {"slug": ("title",)}
    search_fields = ['title', 'slug', ]


@MetatagModelAdmin
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'datetime', 'category', 'tag_list', 'show', 'at_sidebar', 'views', 'likes',)
    list_filter = ('category', 'show', 'at_sidebar', 'tags',)

    fieldsets = (
        (None, {
            'fields': (('title', 'datetime',), ('show', 'at_sidebar',), 'category', 'tags',
                       'cover', 'description', 'text', ('views', 'likes',),)
        }),
    )
    filter_horizontal = ('tags',)
    readonly_fields = ('views',)
    search_fields = ['title', 'description', 'text', ]


@admin.register(Testimonial)
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'url', 'date', 'order', 'show',)
    list_filter = ('show',)
    search_fields = ['title', 'text', 'author', 'url', ]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'post', 'show_root', 'author', 'email', 'subject', 'show',)
    list_display_links = ('id', 'datetime',)
    list_filter = ('post', 'show',)
    list_per_page = 200
    fieldsets = (
        (None, {
            'fields': (('datetime', 'id',), ('post', 'root',), 'profile',)
        }),
        ('Данные из формы', {
            'fields': ('author', 'email', 'subject', 'comment',)
        }),
    )
    search_fields = ['author', 'email', 'subject', 'comment', ]
    # readonly_fields = ('id', 'datetime', 'profile', 'author', 'email', 'subject', 'comment',)
    readonly_fields = ('id', 'datetime',)
