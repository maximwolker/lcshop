# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20150904_1135'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testimonial',
            name='date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f'),
        ),
    ]
