# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0011_auto_20150910_1428'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testimonial',
            name='text',
            field=models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043e\u0442\u0437\u044b\u0432\u0430'),
        ),
    ]
