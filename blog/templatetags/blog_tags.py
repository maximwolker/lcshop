# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template

from blog.models import Category, Tag, Post, Testimonial


register = template.Library()


@register.inclusion_tag('blog/include/blog_categories.html')
def blog_categories():
    categories = [category for category in Category.objects.filter(show=True) if category.posts.count()]
    return {'categories': categories}


@register.inclusion_tag('blog/include/recent_posts.html')
def recent_posts():
    posts = Post.objects.posted().filter(at_sidebar=True)[:5]
    return {'recent_posts': posts}


@register.inclusion_tag('blog/include/blog_testimonials.html')
def blog_testimonials():
    testimonials = Testimonial.objects.filter(show=True)
    return {'testimonials': testimonials}


@register.inclusion_tag('blog/include/tag_cloud.html')
def tag_cloud(category_id=None):
    if category_id:
        category = Category.objects.get(id=category_id)
        tags = [tag for tag in Tag.objects.all().order_by('title')
                if Post.objects.posted().filter(tags=tag, category_id=category_id).count()]
    else:
        category = None
        tags = [tag for tag in Tag.objects.all().order_by('title') if Post.objects.posted().filter(tags=tag).count()]
    return {'tag_cloud': tags, 'category': category}
