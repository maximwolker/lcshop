# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone

from ckeditor.fields import RichTextField

from core.utils import get_formatted_dt
from lk.models import Profile
from site_settings.models import MetatagModel


class Category(MetatagModel):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('В url', max_length=255, unique=True)
    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField("Показывать в правой колонке", default=True)

    class Meta:
        ordering = ['order', ]
        verbose_name = 'категория'
        verbose_name_plural = 'категории'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:category', kwargs={'slug': self.slug})

    def get_meta_title(self):
        return self.meta_title if self.meta_title else '{0} — Блог — Alazone'.format(self.title)

    @property
    def posts(self):
        return Post.objects.posted().filter(category_id=self.id)


class Tag(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('В url', max_length=255, unique=True)

    class Meta:
        ordering = ['title', ]
        verbose_name = 'тег'
        verbose_name_plural = 'теги к публикациям'

    def __unicode__(self):
        return self.title


class PostManager(models.Manager):

    def posted(self):
        return self.filter(datetime__lte=get_formatted_dt(), show=True)


class Post(MetatagModel):
    category = models.ForeignKey(Category, verbose_name='Категория')
    title = models.CharField('Заголовок', max_length=255)
    cover = models.ImageField('Обложка', upload_to='blog/covers/')
    description = RichTextField('Краткое описание', config_name='simple', null=True, blank=True)
    text = RichTextField('Текст', null=True, blank=True)

    show = models.BooleanField('Показывать на сайте', default=True)
    at_sidebar = models.BooleanField('Вынести в правую колонку', default=True)
    datetime = models.DateTimeField('Время публикации', default=timezone.now)

    tags = models.ManyToManyField(Tag, verbose_name='Теги', blank=True)
    views = models.IntegerField('Количество просмотров', default=0)
    likes = models.IntegerField('Количество лайков', default=0)

    objects = PostManager()

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'публикация'
        verbose_name_plural = 'публикации'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post', kwargs={'category': self.category.slug,'pk': self.pk})

    def get_meta_title(self):
        return (self.meta_title if self.meta_title
                else '{0} — {1}'.format(self.title, self.category.get_meta_title()))

    def get_month(self):
        MONTHS = {
            1: 'Янв', 2: 'Фев', 3: 'Мар', 4: 'Апр', 5: 'Мая', 6: 'Июня',
            7: 'Июля', 8: 'Авг', 9: 'Сен', 10: 'Окт', 11: 'Ноя', 12: 'Фев',
        }
        return MONTHS[self.datetime.month]

    def tag_list(self):
        return ', '.join([tag.title for tag in self.tags.all()])
    tag_list.allow_tags = True
    tag_list.short_description = 'Теги'

    @property
    def comments(self):
        return self.comment_set.filter(show=True)

    @property
    def root_comments(self):
        return self.comments.filter(root=None).order_by('datetime')


class Testimonial(models.Model):
    title = models.CharField('Заголовок отзыва', max_length=255)
    text = models.TextField('Текст отзыва')

    author = models.CharField('Автор', max_length=255)
    image = models.ImageField('Изображение', upload_to='blog/testimonials/')
    url = models.URLField('Ссылка на автора', max_length=255, blank=True)
    date = models.DateField('Время добавления', default=timezone.now, help_text='Отображается рядом с именем автора')

    order = models.IntegerField('Порядок', default=10)
    show = models.BooleanField("Показывать на сайте", default=True)

    class Meta:
        ordering = ['order', ]
        verbose_name = 'отзыв'
        verbose_name_plural = 'отзывы'

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(Post, verbose_name='Публикация')
    root = models.ForeignKey('self', verbose_name='Родительский комментарий', null=True, blank=True,
                             related_name='children_set')

    profile = models.ForeignKey(Profile, verbose_name='Пользователь', null=True, blank=True)
    datetime = models.DateTimeField('Дата и время поступления', auto_now_add=True)

    author = models.CharField('Автор', max_length=255)
    email = models.EmailField('Email', max_length=255)
    subject = models.CharField('Тема', max_length=255, blank=True)
    comment = models.TextField('Комментарий')

    show = models.BooleanField("Показывать на сайте", default=True)

    class Meta:
        ordering = ['-datetime', ]
        verbose_name = 'комментарий'
        verbose_name_plural = 'комментарии к публикациям'

    def __unicode__(self):
        return '({0}) {1}'.format(self.datetime.strftime('%d.%m.%Y %H:%M'), self.subject)

    @property
    def children(self):
        return self.children_set.filter(show=True).order_by('datetime')

    def show_root(self):
        return self.root or ''
    show_root.allow_tags = True
    show_root.short_description = 'Родительский комментарий'
